// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [C,L] = sudoku_candidates(X) 
//   Returns candidates for a sudoku X.
//
// Calling Sequence
//   [C,L] = sudoku_candidates(X)
//
// Parameters
// X: a 9-by-9 matrix of doubles, with 0 for unknown entries.
// C: a 9-by-9-by-9 hypermatrix of booleans, the candidates for each cell.
// L: the number of candidates for each cell
//
// Description
//   Returns candidates for a sudoku X.
//
// Examples
// X = [
// 0 0 0   0 0 0   0 0 0
// 0 0 0   0 0 0   5 3 0
// 9 0 0   0 3 7   0 0 0
// 0 0 2   3 0 1   0 9 6
// 0 4 7   6 9 0   1 8 0
// 0 0 6   7 8 5   0 0 0
// 0 5 0   0 2 0   9 0 3
// 0 3 0   9 0 0   6 0 8
// 8 0 0   0 1 0   4 7 0
// ]
// [C,L] = sudoku_candidates(X) 
//
// X = [
// 0 0 0   0 0 0   0 0 0
// 9 0 0   0 0 4   0 3 6
// 0 0 0   0 7 2   0 0 9
// 3 0 0   0 0 0   0 0 5
// 0 1 4   0 0 0   8 0 0
// 0 2 5   0 1 0   0 0 0
// 0 0 6   1 0 0   0 0 3
// 0 0 0   0 0 6   0 4 0
// 0 0 2   4 8 9   0 0 0
// ]
// [C,L] = sudoku_candidates(X) 
//
// X = [
//     1    2    0    0    3    0    0    4    0  
//     6    0    0    0    0    0    0    0    3  
//     3    0    4    0    0    0    5    0    0  
//     2    0    0    8    0    6    0    0    0  
//     8    0    0    0    1    0    0    0    6  
//     0    0    0    7    0    5    0    0    0  
//     0    0    7    0    0    0    6    0    0  
//     4    0    0    0    0    0    0    0    8  
//     0    3    0    0    4    0    0    2    0  
// ]
// [C,L] = sudoku_candidates(X) 
//
// X = [
// 0 2 0   0 3 0   0 4 0
// 6 0 0   0 0 0   0 0 3
// 0 0 4   0 0 0   5 0 0
// 0 0 0   8 0 6   0 0 0
// 8 0 0   0 1 0   0 0 6
// 0 0 0   7 0 5   0 0 0
// 0 0 7   0 0 0   6 0 0
// 4 0 0   0 0 0   0 0 8
// 0 3 0   0 4 0   0 2 0
// ]
// [C,L] = sudoku_candidates(X) 
// 
//
// Authors
//   Cleve Moler, 2009
//   Michael Baudin, Scilab port and comments, 2010-2011

    [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_candidates" , rhs , 1 )
  apifun_checklhs ( "sudoku_candidates" , lhs , 2 )
  //
  // Check type
  apifun_checktype ( "sudoku_candidates" , X , "X" , 1 , "constant" )
  //
  // Check size
  apifun_checkdims ( "sudoku_candidates" , X , "X" , 1 , [9 9] )
  //
  C(1:9,1:9,1:9) = %t
  [irows,icols] = find(X>0)
  nf = size(irows,"*")
  for k = 1 : nf
    r = irows(k); c = icols(k)
    d = X(r,c)
    C(r,:,d) = %f;
    C(:,c,d) = %f;
    C(tri(r),tri(c),d) = %f;
    C(r,c,:) = %f;
  end
  L = zeros(9,9)
  for r = 1 : 9
    for c = 1 : 9
      L(r,c) = size(find(C(r,c,:)),"*")
    end
  end
endfunction


// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction


