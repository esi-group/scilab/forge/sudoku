// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function X = sudoku_latinsquare ( varargin )
  //   Create Latin Squares of order n.
  //
  // Calling Sequence
  //   X = sudoku_latinsquare ( n )
  //   X = sudoku_latinsquare ( n , randomized )
  //
  // Parameters
  // n: a positive integer 1,2, ...
  // X: a n x n matrix with integers from 1, 2, ..., n
  // randomized: a boolean. If true, rows and columns are permuted. (default = %t)
  //
  //
  // Description
  //   Generates a Latin Square of order n. In each row 
  //   or column of the matrix, all integers from 1 to n 
  //   are present only once.
  //
  // Examples
  // X = sudoku_latinsquare ( 4 )
  // X = sudoku_latinsquare ( 4 , %f )
  //
  // Authors
  //   Michael Baudin, 2010-2011

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_latinsquare" , rhs , 1:2 )
  apifun_checklhs ( "sudoku_latinsquare" , lhs , 1 )
  //
  n = varargin(1)
  randomized = apifun_argindefault ( varargin , 2 , %t )
  //
  // Check type
  apifun_checktype ( "sudoku_latinsquare" , n , "n" , 1 , "constant" )
  apifun_checktype ( "sudoku_latinsquare" , randomized , "randomized" , 2 , "boolean" )
  //
  // Check size
  apifun_checkscalar ( "sudoku_latinsquare" , n , "n" , 1  )
  apifun_checkscalar ( "sudoku_latinsquare" , randomized , "randomized" , 2 )
  //
  X = zeros(n,n)
  for i = 1 : n
    X(i,:) = modulo((1:n)+(i-2),n) + 1
  end
  if ( randomized ) then
    r = grand(1,"prm",(1:n)')'
    X(:,r) = X(:,:)
    r = grand(1,"prm",(1:n)')
    X(r,:) = X(:,:)
  end
endfunction

