// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function X = sudoku_readsdm ( filename , k )
  //   Read a sudoku in a .sdm file.
  //
  // Calling Sequence
  //   X = sudoku_readsdm ( filename , k )
  //
  // Parameters
  // filename: a string representing a file
  // k: the index of the sudoku in the collection
  // X: a 9-by-9 matrix, with 0 for unknown entries
  //
  // Description
  //   Read a sudoku in a .sdm and return the matrix X associated,
  //   where 0 represent an unknown cell.
  //   Lines begining with # are considered as comments and are ignored.
  //
  // Examples
  //    path = sudoku_getpath();
  //    filename = fullfile(path,"tests","unit_tests","mycollection.sdm");
  //    X = sudoku_readsdm ( filename , 3 )
  //
  // See also
  //   sudoku_readsdk
  //   sudoku_readsdm
  //   sudoku_readsdmnb
  //   sudoku_readgivens
  //   sudoku_readss
  //   sudoku_writesdk
  //   sudoku_writegivens
  //   sudoku_writess
  //
  // Authors
  //   Michael Baudin, 2010-2011
  //
  // Bibliography
  //   http://www.sudocue.net/fileformats.php

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_readsdm" , rhs , 2 )
  apifun_checklhs ( "sudoku_readsdm" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_readsdm" , filename , "filename" , 1 , "string" )
  apifun_checktype ( "sudoku_readsdm" , k , "k" , 2 , "constant" )
  //
  // Check size
  apifun_checkscalar ( "sudoku_readsdm" , filename , "filename" , 1 )
  apifun_checkscalar ( "sudoku_readsdm" , k , "k" , 2 )
  //
  if ( fileinfo(filename)==[] ) then
    errmsg = msprintf(gettext("%s: The file %s does not exist."), "sudoku_readsdm", filename);
    error(errmsg)
  end
  if ( k < 0 ) then
    errmsg = msprintf(gettext("%s: The given sudoku index k=%d is negative."), "sudoku_readsdm", k );
    error(errmsg)
  end

  X = zeros(9,9)
  buffer = read(filename,-1,1,"(A)")
  nblines = size(buffer,"r")
  nb = 0
  for i = 1 : nblines
    row = stripblanks ( buffer ( i ))
    if ( row <> "" & part(row,1) <> "#" ) then
      nb = nb + 1
      if ( nb == k ) then
        X = sudoku_readgivens ( row )
      end
    end
  end
endfunction


