// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function islatin = sudoku_islatin ( X )
  //   Check if a matrix is a Latin Square.
  //
  // Calling Sequence
  //   islatin = sudoku_islatin ( X )
  //
  // Parameters
  // X: a n-by-n matrix of doubles, with integers from 1, 2, ..., n
  // islatin: a 1-by-1 matrix of booleans. If true, each integer appear only once in each row and each column
  //
  // Description
  //   Returns %t if the given square matrix is a Latin Square
  //
  // Examples
  // X = sudoku_latinsquare ( 4 )
  // islatin = sudoku_islatin ( X )
  //
  // Authors
  //   Michael Baudin, 2010-2011
  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_islatin" , rhs , 1 )
  apifun_checklhs ( "sudoku_islatin" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_islatin" , X , "X" , 1 , "constant" )
  //
  // Check size
  // A non-square X is non a latin square: do not produce an error.
  //

  n = size(X,"r")
  ncols = size(X,"c")
  if ( ncols <> n ) then
    islatin = %f
    return
  end
  // Check entries
  wrong = find( (X < 1) | (X > n) )
  if ( wrong ) then
    islatin = %f
    return
  end
  s = sum(1:n)
  // Check rows 
  rows = sum(X,"r")
  wrong = find(rows <> s)
  if ( wrong ) then
    islatin = %f
    return
  end
  // Check columns
  cols = sum(X,"c")
  wrong = find(cols <> s)
  if ( wrong ) then
    islatin = %f
    return
  end
  islatin = %t
endfunction

