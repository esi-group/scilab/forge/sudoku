// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ C , L ] = sudoku_candidateremove ( C , L , kind , iv , jv , v )
  //   Remove a candidate.
  //
  // Calling Sequence
  //   C = sudoku_candidateremove ( C , kind , iv , jv , v )
  //
  // Parameters
  // C: a 9-by-9-by-9 hypermatrix of booleans, the candidates
  // L: a 9-by-9 matrix of doubles, the number candidates
  // kind: set to 1 for row, 2 for column and 3 for block, 4 for cell
  // iv: the row of the value. If kind = 1 or 4, iv in 1:9. If kind = 3, iv in [1 4 7].
  // jv: the column of the value. If kind = 2 or 4, jv in 1:9. If kind = 3, jv in [1 4 7].
  // v: the value to remove
  //
  // Description
  //   Removes the value v from the candidates in C.
  //   Removes v from the row, column or block identified by
  //   ix and jx. 
  //   If kind = 1, we remove v from the row iv.
  //   If kind = 2, we remove v from the column jv.
  //   If kind = 3, we remove v from the block (iv,jv).
  //   If kind = 4, we remove v from the cell (iv,jv).
  //
  // Examples
  //
  // X = [
  // 0 6 0   0 9 0   0 0 0
  // 0 0 3   1 0 0   0 9 0
  // 1 9 0   0 2 6   3 0 0
  // ..
  // 8 0 0   5 0 0   4 1 0
  // 0 0 0   0 6 0   0 0 0
  // 0 5 1   0 0 3   0 0 9
  // ..
  // 0 0 9   6 3 0   0 2 7
  // 0 2 0   0 0 4   5 0 0
  // 0 0 0   0 1 0   0 4 0
  // ];
  // [C,L] = sudoku_candidates ( X )
  // // Remove the 1 from the cell (7,7) : it is a row locked candidate of type 1
  // [ C , L ] = sudoku_candidateremove ( C , L , 4 , 2 , 2 , 7 ) 
  //
  // See also
  //   sudoku_candidateremove
  //   sudoku_candidates
  //   sudoku_candidatescell
  //   sudoku_confirmcell
  //
  // Authors
  // Michael Baudin, 2010

    [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_candidateremove" , rhs , 6 )
  apifun_checklhs ( "sudoku_candidateremove" , lhs , 2 )
  //
  // Check type
  apifun_checktype ( "sudoku_candidateremove" , C , "C" , 1 , "hypermat" )
  apifun_checktype ( "sudoku_candidateremove" , L , "L" , 2 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , kind , "kind" , 3 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , iv , "iv" , 4 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , jv , "jv" , 5 , "constant" )
  apifun_checktype ( "sudoku_candidateremove" , v , "v" , 6 , "constant" )
  //
  // Check size
  apifun_checkdims ( "sudoku_candidateremove" , C , "C" , 1 , [9 9 9] )
  apifun_checkdims ( "sudoku_candidateremove" , L , "L" , 2 , [9 9] )
  apifun_checkdims ( "sudoku_candidateremove" , kind , "kind" , 3 , [1 1] )
  if ( or(kind == [1 3 4]) ) then
    apifun_checkdims ( "sudoku_candidateremove" , iv , "iv" , 4 , [1 1] )
  else
    apifun_checkdims ( "sudoku_candidateremove" , iv , "iv" , 4 , [0 0] )
  end
  if ( or(kind == [2 3 4]) ) then
    apifun_checkdims ( "sudoku_candidateremove" , jv , "jv" , 5 , [1 1] )
  else
    apifun_checkdims ( "sudoku_candidateremove" , jv , "jv" , 5 , [0 0] )
  end
  apifun_checkdims ( "sudoku_candidateremove" , v , "v" , 6 , [1 1] )
  //
  if ( kind == 1 ) then
    // Remove v from row iv
    if ( iv == [] ) then
      error ( mprintf("%s: Empty iv is not expected when kind = 1.","sudoku_candidateremove"))
    end
    C(iv,:,v) = %f
    for j = 1 : 9
      L(iv,j) = size(find(C(iv,j,:)),"*")
    end
  elseif ( kind == 2 ) then
    // Remove v from column jv
    if ( jv == [] ) then
      error ( mprintf("%s: Empty jv is not expected when kind = 2.","sudoku_candidateremove"))
    end
    C(:,jv,v) = %f
    for i = 1 : 9
      L(i,jv) = size(find(C(i,jv,:)),"*")
    end
  elseif ( kind == 3 ) then
    // Remove v from block (iv,jv)
    if ( iv == [] ) then
      error ( mprintf("%s: Empty iv is not expected when kind = 3.","sudoku_candidateremove"))
    end
    if ( jv == [] ) then
      error ( mprintf("%s: Empty jv is not expected when kind = 3.","sudoku_candidateremove"))
    end
    C(tri(iv),tri(jv),v) = %f
    for i = tri(iv)
    for j = tri(jv)
      L(i,j) = size(find(C(i,j,:)),"*")
    end
    end
  else
    // Remove v from cell (iv,jv)
    if ( iv == [] ) then
      error ( mprintf("%s: Empty iv is not expected when kind = 4.","sudoku_candidateremove"))
    end
    if ( jv == [] ) then
      error ( mprintf("%s: Empty jv is not expected when kind = 4.","sudoku_candidateremove"))
    end
    C(iv,jv,v) = %f
    L(iv,jv) = size(find(C(iv,jv,:)),"*")
  end
endfunction
//
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction
