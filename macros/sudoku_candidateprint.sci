// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function str = sudoku_candidateprint ( X , C , L , d )
  //   Find and print a candidate in the sudoku.
  //
  // Calling Sequence
  //   str = sudoku_candidateprint ( X , C , L , d )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // d: the digit to find in the sudoku
  // rows: the rows where the candidate is found
  // cols: the columns where the candidate is found
  //
  // Description
  //   Search for the candidate d in all the nonfixed cells of the sudoku X.
  //   The search is performed by sudoku_candidatefind.
  //   Then print the cells where the candidate d appears.
  //
  // Examples
  // X = [
  // 4 0 0   3 9 0   0 0 2
  // 2 6 0   0 5 8   3 9 0
  // 5 9 3   6 0 0   1 8 0
  // ..
  // 1 0 0   8 6 0   0 0 9
  // 6 0 5   9 0 0   2 0 0 
  // 0 3 9   2 4 5   0 1 6
  // ..
  // 0 5 6   0 0 9   0 2 0
  // 0 1 4   7 0 0   9 0 5
  // 9 0 0   5 3 0   0 0 0
  // ];
  // [C,L] = sudoku_candidates(X);
  // str = sudoku_candidateprint ( X , C , L , 2 ); 
  //
  // Authors
  //   Michael Baudin, 2010
  //
  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_candidateprint" , rhs , 4 )
  apifun_checklhs ( "sudoku_candidateprint" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_candidateprint" , X , "X" , 1 , "constant" )
  apifun_checktype ( "sudoku_candidateprint" , C , "C" , 2 , "hypermat" )
  apifun_checktype ( "sudoku_candidateprint" , L , "L" , 3 , "constant" )
  apifun_checktype ( "sudoku_candidateprint" , d , "d" , 4 , "constant" )
  //
  // Check size
  apifun_checkdims ( "sudoku_candidateprint" , X , "X" , 1 , [9 9] )
  apifun_checkdims ( "sudoku_candidateprint" , C , "C" , 2 , [9 9 9] )
  apifun_checkdims ( "sudoku_candidateprint" , L , "L" , 3 , [9 9] )
  apifun_checkdims ( "sudoku_candidateprint" , d , "d" , 4 , [1 1] )
  //
  [rows,cols] = sudoku_candidatefind ( X , C , L , d )
  //
  // Create a matrix of strings
  len = size(rows,"*")
  S(1:9,1:9) = "";
  for k = 1 : len
    S(rows(k),cols(k)) = string(k)
  end
  //
  // Compute the maximum number of digits in a cell
  lmax = 0
  for k = 1 : len
    lmax = max ( [length(S(rows(k),cols(k))) lmax ] )
  end
  // Add a blank at the end
  lmax = lmax + 1
  //
  // Update the matrix of string and pad with blanks up to the max
  for i = 1 : 9
    for j = 1 : 9
      lS = length(S(i,j))
      if ( lS < lmax ) then
        S(i,j) = S(i,j) + blanks(lmax - lS)
      end
    end
  end
  //
  // Create a matrix of rows suitable for human reading
  str = []
  k = 0
  for i = 1 : 9
    k = k + 1
    str(k) = ""
    for j = 1 : 9
      str(k) = str(k) + S(i,j)
      if ( modulo(j,3) == 0 & j < 9 ) then
        str(k) = str(k) + "|"
      end
    end
    if ( modulo(i,3) == 0 & i < 9 ) then
      k = k + 1
      str(k) = ""
      for km = 1 : lmax*9 + 3
        str(k) = str(k) + "-"
      end
    end
  end
  //
  // Print the rows
  for r = 1 : size(str,"r")
    mprintf("%s\n",str(r))
  end
endfunction
