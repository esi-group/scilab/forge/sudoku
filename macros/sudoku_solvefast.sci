// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [S, totalsol, score, depth] = sudoku_solvefast( X )
  //   Solves Sudoku with fast library.
  //
  // Calling Sequence
  //   [S, totalsol, score, depth] = sudoku_solvefast( X )
  //
  // Parameters
  // X: a 9-by-9 matrix of doubles, with 0 for unknown entries
  // totalsol: a 1-by-1 matrix of doubles, the total number of solutions for this puzzle
  // score: a 1-by-1 matrix of doubles, the score of the first solution, positive
  // depth: a 1-by-1 matrix of doubles, the maximum depth of the recursive calls for the first puzzle. depth is greater than 1.
  // S: a 9-by-9 matrix of doubles, the first solution. If there is no solution, then S is the empty matrix.
  //
  // Description
  // Uses the compiled C source code by Bill DuPree.
  //
  // This is a program that solves Su Doku (aka Sudoku, Number Place, etc.) puzzles   
  // primarily using deductive logic. 
  // It will only resort to trial-and-error and      
  // backtracking approaches upon exhausting all of its deductive moves. See the C    
  // source code for more detailed information. 
  //
  // The following describes the score:
  // <itemizedlist>
  // <listitem><para> Trivial 	: 80 points or less </para></listitem>
  // <listitem><para> Easy 	: 81 - 125 points </para></listitem>
  // <listitem><para> Medium 	: 126 - 225 points </para></listitem>
  // <listitem><para> Hard 	: 226 - 350 points </para></listitem>
  // <listitem><para> Very Hard 	: 351 - 760 points </para></listitem>
  // <listitem><para> Diabolical 	: 761 and up </para></listitem>
  // </itemizedlist>
  //
  // If the sudoku cannot be solved, then S == [], totalsol == 0 and 
  // score and depth have no meaning.
  //
  // More details on the algorithm
  //
  // The puzzle algorithm initially assumes every unsolved cell can assume every possible value. 
  // It then uses the placement of the givens to refine the choices available to each cell. 
  // This is the markup phase.
  //
  // After markup completes, the algorithm then looks for singleton cells with values that, 
  // due to constraints imposed by the row, column, or 3x3 box, may only assume one possible value. 
  // Once these cells are assigned values, the algorithm returns to the markup phase to apply 
  // these changes to the remaining candidate solutions. 
  // The markup/singleton phases alternate until either no more changes occur, 
  // or the puzzle is solved. 
  // I call the markup/singleton elimination loop the "Simple Solver" because 
  // in a majority of cases it solves the puzzle.
  //
  // If the simple solver portion of the algorithm doesn't produce a solution, 
  // then more advanced deductive rules are applied. 
  // I've implemented two additional rules as part of the deductive puzzle solver. 
  // The first is "naked/hidden" subset elimination wherein a row/column/box is 
  // scanned for X number of cells with X number of matching candidate solutions. 
  // If such subsets are found in the row, column, or box, then the candidates values 
  // from the subset may be eliminated from all other unsolved cells within the row, 
  // column, or box, respectively.
  //
  // The second advanced deductive rule scans boxes looking for candidate values that 
  // exclusively align themselves along rows or columns within the boxes, aka chutes. 
  // If candidate values are found aligning within a set of N chutes aligned within N boxes, 
  // then those candidates may be eliminated from aligned chutes in boxes outside of the set of N boxes.
  //
  // Note that each of the advanced deductive rules calls all preceeding rules, in order, 
  // if that advanced rule has effected a change in puzzle markup.
  //
  // Finally, if no solution is found after iteratively applying all deductive rules, 
  // then we begin trial-and-error using recursion for backtracking. 
  // A working copy is created from our puzzle, and using this copy the first cell 
  // with the smallest number of candidate solutions is chosen. 
  // One of the solutions values is assigned to that cell, and the solver algorithm 
  // is called using this working copy as its starting point. 
  // Eventually, either a solution, or an impasse is reached.
  //
  // If we reach an impasse, the recursion unwinds and the next trial solution is attempted. 
  // If a solution is found (at any point) the values for the solution are added to a list. 
  // Again, so long as we are examining all possibilities, the recursion unwinds so that 
  // the next trial may be attempted. 
  // It is in this manner that we enumerate puzzles with multiple solutions.
  //
  // Note that it is certainly possible to add to the list of applied deductive rules. 
  // The techniques known as "X-Wing" and "Swordfish" come to mind. 
  // On the other hand, adding these additional rules will, in all likelihood, slow 
  // the solver down by adding to the computational burden while producing very few results.                                 
  //
  // Examples
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 0 0 0   0 0 0   5 3 0
  // 9 0 0   0 3 7   0 0 0
  // 0 0 2   3 0 1   0 9 6
  // 0 4 7   6 9 0   1 8 0
  // 0 0 6   7 8 5   0 0 0
  // 0 5 0   0 2 0   9 0 3
  // 0 3 0   9 0 0   6 0 8
  // 8 0 0   0 1 0   4 7 0
  // ]
  // [S, totalsol, score, depth] = sudoku_solvefast( X )
  //
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 9 0 0   0 0 4   0 3 6
  // 0 0 0   0 7 2   0 0 9
  // 3 0 0   0 0 0   0 0 5
  // 0 1 4   0 0 0   8 0 0
  // 0 2 5   0 1 0   0 0 0
  // 0 0 6   1 0 0   0 0 3
  // 0 0 0   0 0 6   0 4 0
  // 0 0 2   4 8 9   0 0 0
  // ]
  // [S, totalsol, score, depth] = sudoku_solvefast( X )
  //
  // // An impossible sudoku
  // X = [
  // 1 2 0    0 3 0    0 4 0  
  // 6 0 0    0 0 0    0 0 3  
  // 3 0 4    0 0 0    5 0 0  
  // 2 0 0    8 0 6    0 0 0  
  // 8 0 0    0 1 0    0 0 6  
  // 0 0 0    7 0 5    0 0 0  
  // 0 0 7    0 0 0    6 0 0  
  // 4 0 0    0 0 0    0 0 8  
  // 0 3 0    0 4 0    0 2 0  
  // ]
  // [S, totalsol, score, depth] = sudoku_solvefast( X )
  //
  // // A difficult sudoku
  // X = [
  // 0 2 0   0 3 0   0 4 0
  // 6 0 0   0 0 0   0 0 3
  // 0 0 4   0 0 0   5 0 0
  // 0 0 0   8 0 6   0 0 0
  // 8 0 0   0 1 0   0 0 6
  // 0 0 0   7 0 5   0 0 0
  // 0 0 7   0 0 0   6 0 0
  // 4 0 0   0 0 0   0 0 8
  // 0 3 0   0 4 0   0 2 0
  // ]
  // [S, totalsol, score, depth] = sudoku_solvefast( X )
  //
  // // A sudoku without solution
  // X = [
  // 8 0 0   0 0 0   0 0 0 
  // 8 0 0   3 0 5   0 0 2 
  // 0 0 6   0 0 0   9 0 0 
  // 0 4 0   5 0 6   0 8 0 
  // 7 0 1   0 0 0   4 0 9 
  // 0 0 0   9 0 1   0 0 0 
  // 9 7 0   0 6 0   0 3 5 
  // 0 0 3   0 0 0   1 0 0 
  // 0 0 4   0 2 0   7 0 0 
  // ]
  // [S, totalsol, score, depth] = sudoku_solvefast( X )
  //
  // // An extremely difficult sudoku
  // X = [
  // 1 0 0   0 0 7   0 9 0 
  // 0 3 0   0 2 0   0 0 8 
  // 0 0 9   6 0 0   5 0 0 
  // 0 0 5   3 0 0   9 0 0 
  // 0 1 0   0 8 0   0 0 2 
  // 6 0 0   0 0 4   0 0 0 
  // 3 0 0   0 0 0   0 1 0 
  // 0 4 0   0 0 0   0 0 7 
  // 0 0 7   0 0 0   3 0 0 
  // ]
  // [S, totalsol, score, depth] = sudoku_solvefast( X )
  //
  // // A sudoku with 155 different solutions
  // X = [
  // 0 0 0   0 0 7   0 9 0 
  // 0 3 0   0 2 0   0 0 8 
  // 0 0 9   6 0 0   5 0 0 
  // 0 0 5   3 0 0   9 0 0 
  // 0 1 0   0 8 0   0 0 2 
  // 6 0 0   0 0 4   0 0 0 
  // 3 0 0   0 0 0   0 1 0 
  // 0 4 0   0 0 0   0 0 7 
  // 0 0 7   0 0 0   3 0 0 
  // ]
  // [S, totalsol, score, depth] = sudoku_solvefast( X )
  //
  // Authors
  // Bill DuPree, 2006-2008
  // Michael Baudin, 2011
  //
  // Bibliography
  //   A Su Doku Solver in C, Bill DuPree, 2008
  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_solvefast" , rhs , 1 )
  apifun_checklhs ( "sudoku_solvefast" , lhs , 1:5 )
  //
  // Check type
  apifun_checktype ( "sudoku_solve" , X , "X" , 1 , "constant" )
  //
  // Check size
  apifun_checkdims ( "sudoku_solve" , X , "X" , 1 , [9 9] )
  //
  puzzle = sudoku_writegivens ( X )
  [status, totalsol, score, depth, sol] = _sudoku_solvefast(puzzle)
  if ( status == 1 ) then
    S = sudoku_readgivens ( sol )
  else
    S = []
  end
endfunction


