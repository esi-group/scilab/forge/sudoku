// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function str = sudoku_writegivens ( X )
  //   Returns the given sudoku as a string of 81 givens.
  //
  // Calling Sequence
  //   str = sudoku_writegivens ( X )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // str: a string of 81 letters, with dots "." for unknowns. The entries come row by row.
  //
  // Description
  //   Converts a sudoku in matrix form into a string, where "." represents unknowns.
  //
  // Examples
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 0 0 0   0 0 0   5 3 0
  // 9 0 0   0 3 7   0 0 0
  // 0 0 2   3 0 1   0 9 6
  // 0 4 7   6 9 0   1 8 0
  // 0 0 6   7 8 5   0 0 0
  // 0 5 0   0 2 0   9 0 3
  // 0 3 0   9 0 0   6 0 8
  // 8 0 0   0 1 0   4 7 0
  // ]
  // sudoku_writegivens ( X )
  //
  // See also
  //   sudoku_readsdk
  //   sudoku_readsdm
  //   sudoku_readsdmnb
  //   sudoku_readgivens
  //   sudoku_readss
  //   sudoku_writesdk
  //   sudoku_writegivens
  //   sudoku_writess
  //
  // Authors
  //   Michael Baudin, 2010
  //
  // Bibliography
  //   http://www.sudocue.net/fileformats.php

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_writegivens" , rhs , 1 )
  apifun_checklhs ( "sudoku_writegivens" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_writegivens" , X , "X" , 1 , "constant" )
  //
  // Check size
  apifun_checkdims ( "sudoku_writegivens" , X , "X" , 1 , [9 9] )
  //
  str = ""
  for i = 1 : 9
    for j = 1 : 9
      if ( X(i,j) == 0 ) then
        str = str + "."
      else
        str = str + msprintf("%d",X(i,j))
      end
    end
  end
endfunction


