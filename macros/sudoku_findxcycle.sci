// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ X , C , L ] = sudoku_findxcycle ( varargin )
  //   Search for pairs.
  //
  // Calling Sequence
  //   [ X , C , L ] = sudoku_findxcycle ( X , C , L )
  //   [ X , C , L ] = sudoku_findxcycle ( X , C , L , stopatfirst )
  //   [ X , C , L ] = sudoku_findxcycle ( X , C , L , stopatfirst , verbose )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // stopatfirst: if %t, then stop when one or more candidates have been removed. (default = %t)
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  //
  // Description
  //   Find Alternating Inference Chains.
  //
  //   The algorithm searches for chains of cells with the same candidate d.
  //   The link between cells are either strong or weak, depending on the number of cells 
  //   having candidate d in the same row, column or block. If there are only 2 cells 
  //   having candidate d, the link is strong (symbol "="). If there are more than two cells, the link
  //   is weak (symbol "-"). A chain is represented by a list of cells, separated with the 
  //   type of link, e.g. the link associated with candidate 8 : 8(3,1)=8(2,2)-8(2,8)=8(6,8)-8(6,1)=8(3,1)
  //   This is indeed a chain, since the last element is the same as the first.
  //   (In many texts, it is implicitely written).
  //   Once the chain is computed, we can make deductions.
  //   * Rule #1 : if the chain is made of strictly alternating links i.e. weak, strong, weak, strong, 
  //     etc..., then the candidate d in the same row, column or block as a weak link can 
  //     be removed. This is a continuous X-Cycle.
  //   * Rule #2: if candidate d in cell (i,j) in the chain follows and is followed by a strong 
  //     link, it can be confirmed. This is a discontinuous X-Cycle.
  //     The length of the chain must be odd.
  //     The chain must alternate weak and strong links, except at the discontinuity.
  //   * Rule #3 : if candidate d in cell (i,j) in the chain follows and is followed by a weak  
  //     link, it can be removed. This is a discontinuous X-Cycle.
  //     The length of the chain must be odd.
  //     The chain must alternate weak and strong links, except at the discontinuity.
  //
  //   The X-Wing, Swordfish and 2-colors strategies are superseded by the X-Cycle strategy.
  //
  //   The first algorithm searches for all the locations of the candidate 
  //   d in the sudoku. The result is stored in the arrays rows and cols with length
  //   len: (rows(i),cols(i)) are row and column indices of the i-th cell which 
  //   contains d as candidate. This gives an ordered list of cells containing 
  //   d, numbered from 1 to len. This list of cells can be displayed with printlocations.
  //
  //   Then we compute the list of all conjugate pairs of the candidate d.
  //   This list is computed as a sub-list of the previous list, that is, 
  //   it is made of integers in the [1,len] interval.
  //   The result of the algorithm is an array conjugate where
  //   the number of rows nconj is the number of conjugate pairs and 
  //   conjugate(i,1) and conjugate(i,2) are the i-th conjugate pairs
  //   where 1 <= conjugate(i,1) <= len and 1 <= conjugate(i,2) <= len.
  //
  //   Then the list of all chains is searched.
  //   A chain is represented by a matrix chain where the number of 
  //   rows is the length of the chain:
  //   chain(i,1): the first element of the link #i
  //   chain(i,2): the type of the link #i, equals 0 for a strong link, equal 1 for a weak link.
  //   The element chain(i,1) is linked to chain(i+1,1) by the link of type chain(i,2).
  //   The link type chain($,2) is -1, which stands for undefined.
  //   The chain2str function allows to convert a chain into a string.
  //
  // Examples
  // 
  // X = [
  // 0 2 4   1 0 0   6 7 0 
  // 0 6 0   0 7 0   4 1 0 
  // 7 0 0   9 6 4   0 2 0 
  // ..
  // 2 4 6   5 9 1   3 8 7 
  // 1 3 5   4 8 7   2 9 6 
  // 8 7 9   6 2 3   1 5 4 
  // ..
  // 4 0 0   0 0 9   7 6 0 
  // 3 5 0   7 1 6   9 4 0 
  // 6 9 7   0 4 0   0 3 1 
  // ];
  // [C,L] = sudoku_candidates(X);
  // X = sudoku_findxcycle ( X , C , L , %f , %t );
  //
  // Authors
  //   Michael Baudin, 2010-2011
  //
  // Bibliography
  //   Angus Johnson, http://www.angusj.com/sudoku/hints.php
  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_findxcycle" , rhs , 3:5 )
  apifun_checklhs ( "sudoku_findxcycle" , lhs , 1:3 )
  //
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  stopatfirst = apifun_argindefault ( varargin , 4 , %t )
  verbose = apifun_argindefault ( varargin , 5 , %f )
  //
  // Check type
  apifun_checktype ( "sudoku_findxcycle" , X , "X" , 1 , "constant" )
  apifun_checktype ( "sudoku_findxcycle" , C , "C" , 2 , "hypermat" )
  apifun_checktype ( "sudoku_findxcycle" , L , "L" , 3 , "constant" )
  apifun_checktype ( "sudoku_findxcycle" , stopatfirst , "stopatfirst" , 4 , "boolean" )
  apifun_checktype ( "sudoku_findxcycle" , verbose , "verbose" , 5 , "boolean" )
  //
  // Check size
  apifun_checkdims ( "sudoku_findxcycle" , X , "X" , 1 , [9 9] )
  apifun_checkdims ( "sudoku_findxcycle" , C , "C" , 2 , [9 9 9] )
  apifun_checkdims ( "sudoku_findxcycle" , L , "L" , 3 , [9 9] )
  apifun_checkscalar ( "sudoku_findxcycle" , stopatfirst , "stopatfirst" , 4 )
  apifun_checkscalar ( "sudoku_findxcycle" , verbose , "verbose" , 5 )
  //  
  // TODO : Find a convenient value for maxlen
  maxlen = 8
  
  // Color cells with digit d
  for d = 1 : 9
    // Get all cells, ordered by column, containg d as a candidate
    [rows,cols] = sudoku_candidatefind ( X , C , L , d )
    len = size(rows,"*")
    if ( len == 0 ) then
      // There are no candidate d in this sudoku: go on to the next digit d
      continue
    end
    //
    // Compute conjugate pairs for this digit
    conjugates = computeconjugates ( rows , cols , verbose )
    nconj = size ( conjugates , "r" )
    if ( nconj == 0 ) then
      // There are no conjugate pair in this sudoku: go on to the next digit d
      continue
    end
    //
    // Compute all possible chains
    chainlist = list ()
    success = %f
    for kp = 1 : nconj
      chain = []
      chain(1,1:2) = [conjugates(kp,1) , 0]
      chain(2,1:2) = [conjugates(kp,2) , -1]
      newlist = findchains ( rows , cols , d , maxlen , chain , verbose )
      //
      // Make the chains unique
      chainlist = listunique ( newlist , chainlist )
      //
      // Rule #1 : if the chain is made of strictly alternating links i.e. weak, strong, weak, strong,
      // etc..., then the candidate d in the same row, column or block as a weak link can
      // be removed. This is a continuous X-Cycle.
      [ X , C , L , success ] = continuouscycle ( X , C , L , d , chainlist , stopatfirst , verbose )
      if ( success & stopatfirst ) then
        break
      end
      //
      // Rule #2 : if candidate d in cell (i,j) in the chain follows and is followed by a strong  
      //     link, it can be confirmed. This is a discontinuous X-Cycle.
      [ X , C , L , success ] = twostronglinks ( X , C , L , d , chainlist , stopatfirst , verbose )
      if ( success & stopatfirst ) then
        break
      end
      //
      // Rule #3 : if candidate d in cell (i,j) in the chain follows and is followed by a weak  
      //     link, it can be removed. This is a discontinuous X-Cycle.
      [ X , C , L , success ] = twoweaklinks ( X , C , L , d , chainlist , stopatfirst , verbose )
      if ( success & stopatfirst ) then
        break
      end
    end
    if ( success & stopatfirst ) then
      break
    end
  end
endfunction


//
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction
// Returns the location of a cell inside a block.
// The location is row first, column second
// location(7,7) = 1, location(7,8) = 2, location(7,9) = 3
// location(8,7) = 4, location(8,8) = 5, location(8,9) = 6
// location(9,7) = 7, location(9,8) = 8, location(9,9) = 9
function l = location ( i , j )
  l = pmodulo(i-1,3)*3 +1 + pmodulo(j-1,3)
endfunction
// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction

// Compute conjugate pairs
// conjugates: a nc x 3 matrix with conjugate pairs
// conjugates(kp,1): the first cell of the pair
// conjugates(kp,2): the second cell of the pair
// Note:
//   It might happen that two cells are both row and block conjugates,
//   or both column and block conjugate.
//   Without additionnal processing, this pair will appear twice in the conjugate array.
//   This would lead to unnecessary colorings.
//   This is why we keep only unique conjugate pairs in the conjugates array.
// Ex:
// conjugates = [
//    1     2   
//    6     19  
//    10    15  
//    13    14  
//  ]
//
function conjugates = computeconjugates ( rows , cols , verbose )
  conjugates = []
  kp = 0
  // Search conjugate pairs in rows
  for r = 1 : 9
    fp = find(rows==r)
    if ( size(fp,"*") == 2 ) then
      kp = kp + 1
      conjugates(kp,1) = fp(1)
      conjugates(kp,2) = fp(2)
    end
  end
  // Search conjugate pairs in columns
  for c = 1 : 9
    fp = find(cols==c)
    if ( size(fp,"*") == 2 ) then
      kp = kp + 1
      conjugates(kp,1) = fp(1)
      conjugates(kp,2) = fp(2)
    end
  end
  // Search conjugate pairs in blocks
  for r = [1 4 7]
    for c = [1 4 7]
      trir = tri(r)
      tric = tri(c)
      fp = find ( rows >= min(trir) & rows <= max(trir) & cols >= min(tric) & cols <= max(tric) )
      if ( size(fp,"*") == 2 ) then
        kp = kp + 1
        conjugates(kp,1) = fp(1)
        conjugates(kp,2) = fp(2)
      end
    end
  end
  // Keep only unique conjugate pairs.
  conjugates = unique(conjugates,"r")
endfunction
//
// Remove duplicate entries in the list l1.
// The uniques list contains only unique entries.
// Ex:
//   uniques = list(1,2,5)
//   l1 = list(2,4,6,7)
//   reduced  = listunique ( l1 , uniques ) // returns (1,2,5,4,6,7)
// TODO : use the "unique" function instead.
function reduced  = listunique ( l1 , uniques )
  reduced = uniques
  for i1 = 1 : length(l1)
    isnew = %t
    for ir = 1 : length(reduced)
      if ( l1(i1) == reduced(ir) ) then
        isnew = %f
        break
      end
    end
    if ( isnew ) then
      reduced($+1) = l1(i1)
    end
  end
endfunction
//
// Order a given chain so that the first element is at the top-left 
// corner.
// Ex:
// rows = [3 7 2 3 7 8 2 7 9 1 2 9 3 9 1 2 3 7 8]'
// cols = [2 2 3 3 3 3 4 4 4 6 6 6 7 7 9 9 9 9 9]'
// chain = [
//   19 6  0
//   6  2  1
//   2  1  1
//   1  13 1
//   13 14 0
//   14 19 1
//   ]
// chainorder ( chain , rows , cols ) 
// is reordered so that 1 comes first, because of the 

function ordered = chainorder ( chain , rows , cols ) 
  nlinks = size ( chain , "r" )
  // Search rmin, the minimum row index in the chain
  rmin = %inf
  for kc = 1 : nlinks
    frst = chain ( kc , 1 )
    if ( rows ( frst ) < rmin ) then
      rmin = rows ( frst )
    end
  end
  // In the chain, search the cell index k with row rmin
  // and minimum column index.
  k = %nan
  cmin = %inf
  for kc = 1 : nlinks
    frst = chain ( kc , 1 )
    if ( rows ( frst ) == rmin & cols ( frst ) < cmin ) then
      cmin = cols ( frst )
      k = kc
    end
  end
  // Re-order the chain so that ks is the first cell in the chain
  ordered ( 1 : nlinks - k + 1 , 1:3 ) = chain ( k : nlinks , 1:3 )
  ordered ( nlinks - k + 2 : nlinks , 1:3 ) = chain ( 1 : nlinks - k + 1 , 1:3 )
endfunction
//
// Print a chain.
// Ex:
// rows = [3 7 2 3 7 8 2 7 9 1 2 9 3 9 1 2 3 7 8]'
// cols = [2 2 3 3 3 3 4 4 4 6 6 6 7 7 9 9 9 9 9]'
//  chain = [
//    1  13 1
//    13 14 0
//    14 19 1
//    19 6  0
//    6  2  1
//    2  1  1
//    ]
// chain2str ( chain , rows , cols , 8 )

function str = chain2str ( chain , rows , cols , d )
  str = ""
  nc =  size ( chain , "r" )
  for kc = 1 : nc-1
    frst     = chain ( kc , 1 )
    typelink = chain ( kc , 2 )
    if ( chain ( kc , 2) == 0 ) then
      slink = "="
    else
      slink = "-"
    end
    str = str + msprintf ("%d(%d,%d)%s",d,rows(frst),cols(frst),slink)
  end
  kc = nc
  frst     = chain ( kc , 1 )
  typelink = chain ( kc , 2 )
  str = str + msprintf ("%d(%d,%d)",d,rows(frst),cols(frst))
endfunction
//
// Returns all possible chains starting from the given chain.
// chainlist : a list of complete chains
// TODO: bug - the algorithm might find more than 2 discontinuous links in the chain.
// TODO: stop at the first discontinuous chain: this chain certainly gives information.
function chainlist = findchains ( rows , cols , d , maxlen , chain , verbose )
  len = size(rows,"*")
  chainlist = list()
  // The starting point of the new chain is the last point of the chain
  chlen = size(chain,"r")
  pfrst = chain(chlen-1,1)
  pscnd = chain(chlen,1)
  // Compute the class of the previous link
  // 1 = row - link
  // 2 = column - link
  // 3 = block - link
  if ( rows(pfrst) == rows(pscnd) ) then
    linkclass = 1
  elseif ( cols(pfrst) == cols(pscnd) ) then
    linkclass = 2
  else
    linkclass = 3
  end
  //
  // Set isdis as true if the chain already contains two consecutive weak links
  isdis = %f
  for kc = 1 : chlen - 1
    if ( chain ( kc , 2 ) == 1 & chain ( kc , 2 ) == chain ( kc+1 , 2 ) ) then
      isdis = %t
      break
    end
  end
  // Our new first point is the previous second
  chlen = chlen + 1
  frst = pscnd
  rf = rows(frst)
  cf = cols(frst)
  //
  // Search for ending point in the same row as the last point, if authorized.
  if ( linkclass <> 1 ) then
    r = rows ( frst )
    fscnd = find(rows==r & cols<>cf)
    nbscnd = size(fscnd,"c")
    // Compute the type of link
    if ( nbscnd > 1 ) then
      // This is a weak link
      linktype = 1
    else
      // There are only two candidate d on this row: strong link
      linktype = 0
    end
    for scnd = fscnd
      newlist = checkchain ( rows , cols , d , maxlen , chain , scnd , linktype , isdis , verbose )
      if ( length ( newlist ) > 0 ) then
        chainlist = listunique ( newlist , chainlist )
      end
    end
  end
  //
  // Search for ending point in the same column as the last point, if authorized.
  if ( linkclass <> 2 ) then
    c = cols ( frst )
    fscnd = find(cols==c & rows<>rf)
    nbscnd = size(fscnd,"c")
    // Compute the type of link
    if ( nbscnd > 1 ) then
      // This is a weak link
      linktype = 1
    else
      // There are only two candidate d on this row: strong link
      linktype = 0
    end
    for scnd = fscnd
      newlist = checkchain ( rows , cols , d , maxlen , chain , scnd , linktype , isdis , verbose )
      if ( length ( newlist ) > 0 ) then
        chainlist = listunique ( newlist , chainlist )
      end
    end
  end
  //
  // Search for ending point in the same block as the last point, if authorized.
  // TODO: bug - the link type must be computed correctly !
  if ( linkclass <> 3 ) then
    // Compute in fscnd the list of cells in the same block as frst,
    // and different from it.
    fscnd = []
    trirf = tri(rf)
    tricf = tri(cf)
    for scnd = 1 : len
      rs = rows(scnd)
      cs = cols(scnd)
      if ( tri(rs) == trirf & tri(cs) == tricf & ~(rf==rs & cf==cs) ) then
        fscnd($+1) = scnd
      end
    end
    // Make it a row vector
    fscnd = fscnd(:)'
    nbscnd = size(fscnd,"c")
    // Compute the type of link
    if ( nbscnd > 1 ) then
      // This is a weak link
      linktype = 1
    else
      // There are only two candidate d on this row: strong link
      linktype = 0
    end
    for scnd = fscnd
      newlist = checkchain ( rows , cols , d , maxlen , chain , scnd , linktype , isdis , verbose )
      if ( length ( newlist ) > 0 ) then
        chainlist = listunique ( newlist , chainlist )
      end
    end
  end
endfunction
//
// Returns the list of chains which start with the given chain.
// We add the frst and scnd elements as a new link only if these 
// elements might generate a new valid chain.
// The chain is completed when the last link is equal to the first one.
// If the chain is not completed, recursively call the findchains function
// to compute the list of possible chains.
//
// conjugates: the array of conjugate pairs
// rows: the list of rows containing the candidate d
// cols: the list of columns containing the candidate d
// d: the digit to analyze
// maxlen: the maximum number of links in this chain
// chain: the old chain. 
// scnd: the second element in the new link 
// linktype: the type of the new link
// isdis: true if the chain is already contains two consecutive weak links
function newlist = checkchain ( rows , cols , d , maxlen , chain , scnd , linktype , isdis , verbose )
  // Setup the initial list of chains
  newlist = list()
  frst = chain($,1)
  // Compute the new number of links
  chlen = size(chain,"r")
  // Setup the new chain
  newchain = chain
  chlen = chlen + 1
  newchain(chlen-1,2) = linktype
  newchain(chlen,1) = scnd
  newchain(chlen,2) = -1
  //
  // If the chain is already discontinuous, we cannot add a weak discontinuity
  if ( chlen >= 2 & isdis & and ( newchain(chlen-2:chlen-1,2) == 1 ) ) then
    return
  end
  //
  // With the exception of the first element of the chain,
  // no element can appear twice in the chain.
  elements = newchain(2:chlen-1,1)
  unels = unique(elements)
  if ( size(elements,"r") <> size(unels,"r") ) then
    return
  end
  // If the length of the new chain is larger than 2,
  // and if the second point is equal to the starting point of the chain,
  // this is a new complete chain.
  if ( newchain(1,1) == scnd ) then
    newlist ( $+1 ) = newchain
    if ( %f ) then
      chainlen = size(chain,"r") - 1
      mprintf ("Chain completed (length=%d) : %s!\n",chainlen,chain2str ( newchain , rows , cols , d ))
    end
    return
  end
  // The chain is not complete.
  // If the length of the chain is maximal, we 
  // cannot make new searches. Go on to the next cell.
  if ( chlen >= maxlen ) then
    return
  end
  // We have room for new chains.
  newlist = findchains ( rows , cols , d , maxlen , newchain )
endfunction
//
// Remove candidates at the intersection of two weak links.
function [ X , C , L , success ] = twoweaklinks ( X , C , L , d , chainlist , stopatfirst , verbose )
  nbch = length(chainlist)
  success = %f
  for kch = 1 : nbch
    chain = chainlist(kch)
    chlen = size(chain,"r")
    //
    // If the number of elements in the chain is even,
    // there is no chance that we can find two weak links, 
    // inside alternating chain.
    if ( modulo(chlen-1,2) == 0 ) then
      continue
    end
    //
    // Search for two consecutive weak links in the chain
    nblinks = size(chain,"r") - 1
    wdisc = -1
    for kl = 1 : nblinks - 1
      if ( chain(kl,2) == 1 & chain(kl+1,2) == 1 ) then
        wdisc = kl
        break
      end
    end
    //
    // If there is no discontinuity, try next chain
    if ( wdisc == -1 ) then
      continue
    end
    //
    // Check that the chain alternates from the start to the point of 
    // discontinuity.
    // A strong link (type=0) can be considered as a weak link.
    // A weak link (type=1) cannot be considered as a strong link.
    // Hence, we only have to check for the strong links.
    // The first link of the chain is strong, by design.
    // Therefore, check only starting from the second element, which 
    // should be weak (type=1).
    mustbe = 1
    alternating = %t
    for kl = 2 : wdisc - 1
      if ( mustbe == 1 ) then
        // This must be a weak link : nothing to check, just alternate and continue.
        mustbe = 0
      else
        // mustbe == 0
        // This must be a strong link : check it.
        if ( chain(kl,2) == 1 ) then
          alternating = %f
          break
        else
          // This is indeed a strong link : alternate and continue.
          mustbe = 1
        end
      end
    end
    // If it does not alternate, go on to the next chain
    if ( ~alternating ) then
      continue
    end
    //
    // Check that the chain alternates from the point of 
    // discontinuity to the end.
    // The first link after the weak discontinuity is weak, by design.
    // Therefore, check only starting from the second element, which 
    // should be strong (type=0).
    mustbe = 0
    alternating = %t
    for kl = wdisc + 2 : nblinks
      if ( mustbe == 1 ) then
        // This must be a weak link : nothing to check, just alternate and continue.
        mustbe = 0
      else
        // mustbe == 0
        // This must be a strong link : check it.
        if ( chain(kl,2) == 1 ) then
          alternating = %f
          break
        else
          // This is indeed a strong link : alternate and continue.
          mustbe = 1
        end
      end
    end
    // If it does not alternate, go on to the next chain
    if ( ~alternating ) then
      continue
    end
    //
    // If there is a weak discontinuity, remove the candidate d
    // at the point of discontinuity.
    // We check that the candidate is still there: it might have been
    // removed earlier in the X-Cycle algorithm.
      k = chain(wdisc+1,1)
      r = rows(k)
      c = cols(k)
      if ( find ( find(C(r,c,:)) == d ) <> [] ) then
        [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , d )
        success = %t
        if ( verbose ) then
          chainlen = size(chain,"r") - 1
          mprintf ("X-cycle weakly discontinuous (length %d) : %s\n",chainlen,chain2str ( chain , rows , cols , d ))
          mprintf("Removed X-Cycle candidate %d at (%d,%d) (remaining %d candidates)\n",d,r,c,sum(L))
        end
      end
    if ( stopatfirst ) then
      break
    end
  end
endfunction

//
// Remove candidates at the intersection of two strong links.
function [ X , C , L , success ] = twostronglinks ( X , C , L , d , chainlist , stopatfirst , verbose )
  nbch = length(chainlist)
  success = %f
  for kch = 1 : nbch
    chain = chainlist(kch)
    chlen = size(chain,"r")
    //
    // If the number of elements in the chain is even,
    // there is no chance that we can find two weak links, 
    // inside alternating chain.
    if ( modulo(chlen-1,2) == 0 ) then
      continue
    end
    //
    // Search for two consecutive weak links in the chain
    nblinks = size(chain,"r") - 1
    wdisc = -1
    for kl = 1 : nblinks - 1
      if ( chain(kl,2) == 0 & chain(kl+1,2) == 0 ) then
        wdisc = kl
        break
      end
    end
    //
    // If there is no discontinuity, try next chain
    if ( wdisc == -1 ) then
      continue
    end
    //
    // Check that the chain alternates from the start to the point of 
    // discontinuity.
    // A strong link (type=0) can be considered as a weak link.
    // A weak link (type=1) cannot be considered as a strong link.
    // Hence, we only have to check for the strong links.
    // The first link of the chain is strong, by design.
    // Therefore, check only starting from the second element, which 
    // should be weak (type=1).
    mustbe = 1
    alternating = %t
    for kl = 2 : wdisc - 1
      if ( mustbe == 1 ) then
        // This must be a weak link : nothing to check, just alternate and continue.
        mustbe = 0
      else
        // mustbe == 0
        // This must be a strong link : check it.
        if ( chain(kl,2) == 1 ) then
          alternating = %f
          break
        else
          // This is indeed a strong link : alternate and continue.
          mustbe = 1
        end
      end
    end
    // If it does not alternate, go on to the next chain
    if ( ~alternating ) then
      continue
    end
    //
    // Check that the chain alternates from the point of 
    // discontinuity to the end.
    // The first link after the weak discontinuity is weak, by design.
    // Therefore, check only starting from the second element, which 
    // should be strong (type=0).
    mustbe = 0
    alternating = %t
    for kl = wdisc + 2 : nblinks
      if ( mustbe == 1 ) then
        // This must be a weak link : nothing to check, just alternate and continue.
        mustbe = 0
      else
        // mustbe == 0
        // This must be a strong link : check it.
        if ( chain(kl,2) == 1 ) then
          alternating = %f
          //mprintf("Element #%d has weak link instead of strong.\n",kl)
          break
        else
          // This is indeed a strong link : alternate and continue.
          mustbe = 1
        end
      end
    end
    // If it does not alternate, go on to the next chain
    if ( ~alternating ) then
      continue
    end
    //
    // If there is a strong discontinuity, confirm the candidate d
    // at the point of discontinuity.
      k = chain(wdisc+1,1)
      r = rows(k)
      c = cols(k)
      if ( find ( find(C(r,c,:)) == d ) <> [] ) then
        if ( verbose ) then
          chainlen = size(chain,"r") - 1
          mprintf ("X-cycle strongly discontinuous (length %d) : %s\n",chainlen,chain2str ( chain , rows , cols , d ))
        end
        [ X , C , L ] = sudoku_confirmcell ( X , C , L , r , c , d , verbose )
        success = %t
      end
    if ( stopatfirst ) then
      break
    end
  end
endfunction


//
// If the chain is made of strictly alternating links i.e. weak, strong, weak, strong,
// etc..., then the candidate d in the same row, column or block as a weak link can
// be removed. This is a continuous X-Cycle.
function [ X , C , L , success ] = continuouscycle ( X , C , L , d , chainlist , stopatfirst , verbose )
  nbch = length(chainlist)
  success = %f
  for kch = 1 : nbch
    chain = chainlist(kch)
    chlen = size(chain,"r")
    //
    // If the number of elements in the chain is odd,
    // there is no chance that we can find a continuous cycle.
    if ( modulo(chlen-1,2) == 1 ) then
      continue
    end
    //
    // If the number of items in the chain is lower than 4, this is 
    // not a continuous X-Cycle.
    if ( chlen-1 < 4 ) then
      continue
    end
    //
    // Check that the chain alternates from the start to the end.
    // Each type of link is to be exactly respected here.
    // The first link of the chain is strong, by design.
    // Therefore, check only starting from the second element, which 
    // should be weak (type=1).
    nblinks = size(chain,"r") - 1
    mustbe = 1
    alternating = %t
    for kl = 2 : nblinks
      if ( chain(kl,2) <> mustbe ) then
        alternating = %f
        break
      end
      mustbe = 1 - mustbe
    end
    // If it does not alternate, go on to the next chain
    if ( ~alternating ) then
      continue
    end
    // Brows the cells of the chain which are weakly linked
    firstverb = %t
    for kl = 2 : 2 : nblinks
      ca = chain(kl,1)
      cb = chain(kl+1,1)
      V = sudoku_visiblefrom2 ( [rows(ca) cols(ca)] , [rows(cb) cols(cb)] )
      // Browse the unfixed cells in the sudoku which are visible from both a and b,
      // and different from a or b.
      for s = find(V & (X==0))
        [r,c] = whatcell ( s )
        if ( ~(r == rows(ca) & c == cols(ca)) & ~(r == rows(cb) & c == cols(cb)) ) then
          // Remove the candidate d from the cell (r,c)
          if ( find ( find(C(r,c,:)) == d ) <> [] ) then
            [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , d )
            success = %t
            if ( verbose ) then
              if ( firstverb ) then
                chainlen = size(chain,"r") - 1
                mprintf ("X-cycle continuous (length %d) : %s\n",chainlen,chain2str ( chain , rows , cols , d ))
                firstverb = %f
              end
              mprintf("Removed X-Cycle candidate %d at (%d,%d) (remaining %d candidates)\n",d,r,c,sum(L))
            end
            if ( stopatfirst ) then
              break
            end
          end
        end
      end
    end
    if ( stopatfirst & success ) then
      break
    end
  end
endfunction

