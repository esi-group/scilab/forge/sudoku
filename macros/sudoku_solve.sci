// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [S,iter,maxlevel] = sudoku_solve ( varargin )
  //   Solves Sudoku.
  //
  // Calling Sequence
  //   S = sudoku_solve ( X )
  //   S = sudoku_solve ( X , verbose )
  //   S = sudoku_solve ( X , verbose , params )
  //   S = sudoku_solve ( X , verbose , params , level )
  //   S = sudoku_solve ( X , verbose , params , level , nsmax )
  //   [S,iter] = sudoku_solve ( ... )
  //   [S,iter,maxlevel] = sudoku_solve ( ... )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  // params: an array of integers. When the number of unknown cells is strictly lower than p, then the associated algorithm is used. (set to 0 to disable this algorithm) (default = 64, i.e. the algorithm works only when there is less than 64 unknowns)
  // params(1): solve by logic
  // params(2): guessing 
  // level : integer, the current level of recursive backtracking. (default = 0)
  // nsmax : integer. The maximum number of solution to find. Set to %inf to compute all the solutions of the sudoku. (default = 1)
  // S: a list of 9-by-9 matrix. The solutions of the sudoku. If S(i) contains zeros, they stand for unsolved entries. S contains at most nsmax elements.
  // iter(1): number naked singles steps
  // iter(2): number hidden singles steps
  // iter(3): number locked candidates steps
  // iter(4): number naked pairs steps
  // iter(5): number hidden pair steps
  // iter(6): number naked triples steps
  // iter(7): number hidden triples steps
  // iter(8): number naked quad steps 
  // iter(9): number hidden quad steps 
  // iter(10): number X-Wing steps 
  // iter(11): number Bi-Color steps 
  // iter(12): number X-Cycle steps
  // iter(13): number of recursive calls
  // maxlevel: the maximum level of the call tree in the nested recursive search used in the guess-based strategy
  //
  // Description
  //   Using logic-based algorithm and Recursive Backtracking (i.e. guessing) when logic failed to solve the puzzle.
  //   This uses two algorithms
  //   * a logic-based algorithm with naked singles, hidden singles, locked candidates,
  //   naked pairs, hidden pairs, naked triples, hidden triples, 
  //   naked quads, hidden quads, X-Wings, and, if this fails,
  //   * a recursive backtracking algorithm designed by Cleve Moler and 
  //     published in The Mathworks News and Notes, 2009.
  //
  //   On output, the test and(S(:) > 0) returns %t if the puzzle is solved.
  //
  //   When recursive backtracking is necessary, we randomly permute
  //   the guesses. This allows to use the algorithm to generate random sudokus
  //   because it allows to sample the search space more evenly.
  //   In case where the sudoku has multiple solutions, it is likely that the 
  //   solver might produce different solutions if run several times.
  //
  //   The strategy for chosing the cell which serves to guess is to 
  //   select the cell which has the least number of candidates.
  //   This allows to systematically chose the tree which has the least size,
  //   and dramatically increase the speed of the algorithm when several guesses
  //   are needed. Indeed, this allows to reduce the probability that 
  //   each candidates is a wrong guess and increases the probability that, 
  //   if our guess is wrong, the algorithm will know it as quickly as possible.
  //
  //   The default threshold is set to 81-17 = 64.
  //   Indeed, Royle Gordon has found many 17-hints sudoku with unique solutions,
  //   but no 16-hint uniquely solvable sudokus. This suggests that if 
  //   there is less than 17 givens, therefore we have to use guessing and
  //   that no logic-based algorithm can find the solution.
  //
  // Examples
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 0 0 0   0 0 0   5 3 0
  // 9 0 0   0 3 7   0 0 0
  // 0 0 2   3 0 1   0 9 6
  // 0 4 7   6 9 0   1 8 0
  // 0 0 6   7 8 5   0 0 0
  // 0 5 0   0 2 0   9 0 3
  // 0 3 0   9 0 0   6 0 8
  // 8 0 0   0 1 0   4 7 0
  // ]
  // sudoku_solve(X)
  // sudoku_solve(X,%t)
  // sudoku_solve(X,%t,[64 %inf]) // Default settings
  // sudoku_solve(X,%t,[0 %inf]) // For easy cases (it is generally faster to use just guessing)
  //
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 9 0 0   0 0 4   0 3 6
  // 0 0 0   0 7 2   0 0 9
  // 3 0 0   0 0 0   0 0 5
  // 0 1 4   0 0 0   8 0 0
  // 0 2 5   0 1 0   0 0 0
  // 0 0 6   1 0 0   0 0 3
  // 0 0 0   0 0 6   0 4 0
  // 0 0 2   4 8 9   0 0 0
  // ]
  // sudoku_solve(X)
  //
  // // An impossible sudoku
  // X = [
  // 1 2 0    0 3 0    0 4 0  
  // 6 0 0    0 0 0    0 0 3  
  // 3 0 4    0 0 0    5 0 0  
  // 2 0 0    8 0 6    0 0 0  
  // 8 0 0    0 1 0    0 0 6  
  // 0 0 0    7 0 5    0 0 0  
  // 0 0 7    0 0 0    6 0 0  
  // 4 0 0    0 0 0    0 0 8  
  // 0 3 0    0 4 0    0 2 0  
  // ]
  // sudoku_solve(X)
  //
  // X = [
  // 0 2 0   0 3 0   0 4 0
  // 6 0 0   0 0 0   0 0 3
  // 0 0 4   0 0 0   5 0 0
  // 0 0 0   8 0 6   0 0 0
  // 8 0 0   0 1 0   0 0 6
  // 0 0 0   7 0 5   0 0 0
  // 0 0 7   0 0 0   6 0 0
  // 4 0 0   0 0 0   0 0 8
  // 0 3 0   0 4 0   0 2 0
  // ]
  // sudoku_solve(X)
  //
  // Authors
  // Cleve Moler, 2009
  // Michael Baudin, 2010-2011
  //
  // Bibliography
  //   Programming Sudoku, Wei-Meng Lee, 2006
  //   Royle Gordon, http://units.maths.uwa.edu.au/~gordon/sudokumin.php
  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_solve" , rhs , 1:5 )
  apifun_checklhs ( "sudoku_solve" , lhs , 1:3 )
  //
  X = varargin(1)
  verbose = apifun_argindefault ( varargin , 2 , %f )
  params = apifun_argindefault ( varargin , 3 , [64 %inf] )
  level = apifun_argindefault ( varargin , 4 , 0 )
  nsmax = apifun_argindefault ( varargin , 5 , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_solve" , X , "X" , 1 , "constant" )
  apifun_checktype ( "sudoku_solve" , verbose , "verbose" , 2 , "boolean" )
  apifun_checktype ( "sudoku_solve" , params , "params" , 3 , "constant" )
  apifun_checktype ( "sudoku_solve" , level , "level" , 4 , "constant" )
  apifun_checktype ( "sudoku_solve" , nsmax , "nsmax" , 5 , "constant" )
  //
  // Check size
  apifun_checkdims ( "sudoku_solve" , X , "X" , 1 , [9 9] )
  apifun_checkscalar ( "sudoku_solve" , verbose , "verbose" , 2 )
  apifun_checkvector ( "sudoku_solve" , params , "params" , 3  , 2 )
  apifun_checkscalar ( "sudoku_solve" , level , "level" , 4 )
  apifun_checkscalar ( "sudoku_solve" , nsmax , "nsmax" , 5 )
  //
  if ( verbose ) then
    mprintf("[%d] Number of solutions to find: %d\n",level,nsmax)
  end
  //
  // First, solve by logic only
  // nLogic is the number of strategies in the logic-based solver
  nLogic = 12
  if ( size(find(X==0),"*") <= params(1) ) then
    [X,iterLogic,C,L] = sudoku_solvebylogic ( X , verbose )
  else
    [C,L] = sudoku_candidates(X)
    iterLogic = zeros(nLogic,1)
  end
  //
  // Compute the iter array
  iter(1:nLogic) = iterLogic(1:nLogic)
  iter(nLogic+1) = 0
  //
  // Initialize the maxlevel
  maxlevel = level
  //
  // Finished puzzle
  if ( and(X(:) > 0) ) then
    S = list ( X )
    return
  end
  //
  // Disable algorithm if necessary
  if ( size(find(X==0),"*") > params(2) ) then
    // Puzzle unsolved by logic
    if ( verbose ) then
      mprintf("Puzzle unsolved by logic only.\n")
    end
    S = list ( )
    return
  end
  if ( verbose ) then
    mprintf("Logic was not sufficient to solve this puzzle.\n")
    mprintf("State before guessing\n")
    mprintf("X = ""%s"".\n",sudoku_writegivens(X))
    mprintf("Fixed = %d, Candidates = %d.\n",sum(X>0),sum(L))
  end
  //
  // Compute the cell k = 1,2,...,81 used for guessing.
  // Consider the unknown cell which is associated
  // with the least number of candidates (but nonzero).
  k = mincandidates ( X , L )
  if ( k == [] ) then
    if ( verbose ) then
      mprintf("Impossible puzzle!\n")
    end
    S = list ( )
    return
  end
  //
  // Compute the list of candidates clist.
  [i,j] = whatcell ( k )
  clist = find(C(i,j,:))
  if ( clist == [] ) then
    // Impossible puzzle
    if ( verbose ) then
      mprintf("Impossible puzzle!\n")
    end
    S = list ( )
    return
  end
  //
  // Recursive backtracking. 
  Y = X
  nc = size(clist,"*")
  S = list ()
  //
  // Iterate over candidates (randomize for the sudoku generator).
  for r = grand(1,"prm",clist')'
    if ( verbose ) then
      mprintf("[%d] Guessing %d (of %d candidates) at (%d,%d) (remaining %d candidates)\n",level,r,nc,i,j,sum(L))
    end
    X = Y
    // Insert a tentative value.
    X(i,j) = r
    [SS,iterGuess,Gmaxlevel] = sudoku_solve ( X , verbose , params , level + 1 , nsmax - size(S) )
    if ( verbose ) then
      mprintf("[%d] Number of solutions : %d\n",level,size(SS))
    end
    // Copy all the solutions into S
    Stotal = size(S)
    for iSS = 1 : size(SS)
      // Check that the new solution is different from the older ones
      isnew = %t
      for jS = 1 : size(S)
        if ( and(S(jS)==SS(iSS)) ) then
          isnew = %f
        end
      end
      if ( isnew ) then
        S ( $ + 1 ) = SS ( iSS )
        Stotal = Stotal + 1
        if ( verbose ) then
          mprintf("[%d] Adding solution #%d (Total = %d/%d): ""%s""\n",level,iSS,Stotal,nsmax,sudoku_writegivens(SS(iSS)))
        end
        if ( Stotal == nsmax ) then
          break
        end
      end
    end
    iter = iter + iterGuess
    iter(nLogic+1) = iter(nLogic+1) + 1
    maxlevel = max( [Gmaxlevel level] )
    if ( size(S) == nsmax ) then
      return
    end
  end
endfunction

function i = findfirst ( A )
  i = find( A )
  if ( i <> [] ) then
    i = i(1)
  end
endfunction

// Returns the index of the unknown cell with the minimum number of candidates
function k = mincandidates ( X , L )
  nbc = %inf
  k = []
  for i = find(X == 0 )
    if ( L(i) < nbc ) then
      k =  i
      nbc = L(i)
    end
  end
endfunction
//
// A collection of strategies for chosing the 
// cell to consider for guessing.
// Returns the index k of the cell to consider
function k = myotherstrategies ( X , C , L )
  // Try various strategies for chosing the cell to guess:
  if ( %f ) then
    // Find the first unknown cell
    k = find ( X == 0 )
    k = k(1)
  elseif ( %f ) then
    // Consider the unknown cell which is associated
    // with the maximum number of candidates (but nonzero).
    k = maxcandidates ( X , C )
  else
    // Consider the unknown cell which is associated
    // with the least number of candidates (but nonzero).
    k = mincandidates ( X , C )
  end
endfunction
// Returns the index of the unknown cell with the max possible number of candidates
function k = maxcandidates ( X , C )
  nbc = 0
  k = []
  for i = find(X == 0 )
    if ( size(C(i).entries,"*") > nbc ) then
      k =  i
      nbc = size(C(i).entries,"*")
    end
  end
endfunction

// Returns a cell where the number of candidates is not too large.
function k = approximatecandidate ( X )
  // D(i,j) = number of unknowns in rows i + number of unknowns in rows j
  D = zeros(9,9)
  z = sum ( X == 0 , "c" )
  D = D + z * ones(1,9)
  z = sum ( X == 0 , "r" )
  D = D + (z' * ones(1,9))'
  // Set %inf in known cells of D
  D(find(X<>0)) = %inf
  [m,k] = min ( D )
endfunction

// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction


