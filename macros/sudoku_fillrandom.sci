// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function X = sudoku_fillrandom ( varargin )
  //   Generate a random solved sudoku.
  //
  // Calling Sequence
  //   X = sudoku_fillrandom ( )
  //   X = sudoku_fillrandom ( verbose )
  //
  // Parameters
  // X: the 9-by-9 matrix of doubles (without zeros)
  //
  // Description
  //   Generates a random sudoku. Fills the first block in canonical form.
  //   Then fills the upper row by picking a random row in all possible
  //   rows. Then solves the sudoku (this can take time).
  //   Then permute the rows to make it lose its canonical form.
  //
  // Examples
  // X = sudoku_fillrandom ( )
  //
  // Authors
  //   Michael Baudin, 2010
  // Bibliography
  //   "Enumerating possible Sudoku grids", Bertram Felgenhauer, Frazer Jarvis, June 2005

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_fillrandom" , rhs , 0:1 )
  apifun_checklhs ( "sudoku_fillrandom" , lhs , 1 )
  //
  verbose = apifun_argindefault ( varargin , 1 , %f )
  //
  // Check type
  apifun_checktype ( "sudoku_fillrandom" , verbose , "verbose" , 1 , "boolean" )
  //
  // Check size
  apifun_checkscalar ( "sudoku_fillrandom" , verbose , "verbose" , 1 )
  //
  X=zeros(9,9)
  // Fill the first block in canonical form
  X(1:3,1:3) = [
  1 2 3
  4 5 6
  7 8 9
  ]
  // Enumerate the possibles upper top row,
  // given that the first block is in canonical form.
  possible = [
  4 5 6   7 8 9
  4 5 7   6 8 9
  4 5 8   6 7 9
  4 5 9   6 7 8
  4 6 7   5 8 9
  4 6 8   5 7 9
  4 6 9   5 7 8
  5 6 7   4 8 9
  5 6 8   4 7 9
  5 6 9   4 7 8
  7 8 9   4 5 6
  6 8 9   4 5 7
  6 7 9   4 5 8
  6 7 8   4 5 9
  5 8 9   4 6 7
  5 7 9   4 6 8
  5 7 8   4 6 9
  4 8 9   5 6 7
  4 7 9   5 6 8
  4 7 8   5 6 9
  ]
  // Select a random upper top row (pick an integer in 1,2,...,20)
  r = grand(1,1,"uin",1,20)
  // Fill the top row of the two upper right blocks
  X(1,4:9) = possible(r,:)
  // Solve it !
  // Use most of the time guessing, since the search space is so large.
  //X = sudoku_solve ( X , verbose , [10 %inf] )
  X = sudoku_solverecursive ( X , verbose )
  // Permute the sudoku to make it loose its canonical form
  X = sudoku_permute ( X )
endfunction

