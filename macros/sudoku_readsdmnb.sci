// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function nb = sudoku_readsdmnb ( filename )
  //   Returns the number of sudoku in a .sdm file.
  //
  // Calling Sequence
  //   X = sudoku_readsdm ( filename , k )
  //
  // Parameters
  // filename: a string representing a file
  // nb: the number of sudoku in the file
  //
  // Description
  //   Read a sudoku in a .sdm and return the number of sudokus in the file.
  //   Lines begining with # are considered as comments and are ignored.
  //
  // Examples
  //    path = sudoku_getpath();
  //    filename = fullfile(path,"tests","unit_tests","mycollection.sdm");
  //    X = sudoku_readsdmnb ( filename )
  //
  // See also
  //   sudoku_readsdk
  //   sudoku_readsdm
  //   sudoku_readsdmnb
  //   sudoku_readgivens
  //   sudoku_readss
  //   sudoku_writesdk
  //   sudoku_writegivens
  //   sudoku_writess
  //
  // Authors
  //   Michael Baudin, 2010-2011
  //
  // Bibliography
  //   http://www.sudocue.net/fileformats.php

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_readsdmnb" , rhs , 1 )
  apifun_checklhs ( "sudoku_readsdmnb" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_readsdmnb" , filename , "filename" , 1 , "string" )
  //
  // Check size
  apifun_checkscalar ( "sudoku_readsdmnb" , filename , "filename" , 1 )
  //
  if ( fileinfo(filename)==[] ) then
    errmsg = msprintf(gettext("%s: The file %s does not exist."), "sudoku_readsdm", filename);
    error(errmsg)
  end

  buffer = read(filename,-1,1,"(A)")
  nblines = size(buffer,"r")
  nb = 0
  for i = 1 : nblines
    row = stripblanks ( buffer ( i ) )
    if ( row <> "" & part(row,1) <> "#" ) then
      nb = nb + 1
    end
  end
endfunction


