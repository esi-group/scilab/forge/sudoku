// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [X,C,L] = sudoku_findxwing ( varargin )
  //   Find X-Wings.
  //
  // Calling Sequence
  //   X = sudoku_findxwing ( X , C , L )
  //   X = sudoku_findxwing ( X , C , L , stopatfirst )
  //   X = sudoku_findxwing ( X , C , L , stopatfirst , verbose )
  //   [X,C] = sudoku_findxwing ( ... )
  //   [X,C,L] = sudoku_findxwing ( ... )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // stopatfirst: if %t, then stop after one or more candidates have been removed. (default = %t)
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  //
  // Description
  //   Search for x-wings.
  //
  // Examples
  // X = [
  // 0 0 0   1 3 0   0 0 5 
  // 0 4 0   0 0 0   2 0 0 
  // 8 0 0   9 0 0   0 0 0 
  // ..
  // 0 0 0   0 5 0   9 0 0 
  // 0 0 2   0 0 0   4 0 0 
  // 0 0 3   0 6 0   0 0 0 
  // ..
  // 0 0 0   0 0 3   0 0 6 
  // 0 0 5   0 0 0   0 1 0 
  // 7 0 0   0 2 8   0 0 0 
  // ];
  // [C,L] = sudoku_candidates(X);
  // sudoku_findxwing ( X , C , L , %f , %t );
  //
  // Authors
  //   Michael Baudin, 2010 - 2011
  //
  // Bibliography
  //   http://www.sadmansoftware.com/sudoku/xwing.htm

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_findxwing" , rhs , 3:5 )
  apifun_checklhs ( "sudoku_findxwing" , lhs , 1:3 )
  //
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  stopatfirst = apifun_argindefault ( varargin , 4 , %t )
  verbose = apifun_argindefault ( varargin , 5 , %f )
  //
  // Check type
  apifun_checktype ( "sudoku_findxwing" , X , "X" , 1 , "constant" )
  apifun_checktype ( "sudoku_findxwing" , C , "C" , 2 , "hypermat" )
  apifun_checktype ( "sudoku_findxwing" , L , "L" , 3 , "constant" )
  apifun_checktype ( "sudoku_findxwing" , stopatfirst , "stopatfirst" , 4 , "boolean" )
  apifun_checktype ( "sudoku_findxwing" , verbose , "verbose" , 5 , "boolean" )
  //
  // Check size
  apifun_checkdims ( "sudoku_findxwing" , X , "X" , 1 , [9 9] )
  apifun_checkdims ( "sudoku_findxwing" , C , "C" , 2 , [9 9 9] )
  apifun_checkdims ( "sudoku_findxwing" , L , "L" , 3 , [9 9] )
  apifun_checkscalar ( "sudoku_findxwing" , stopatfirst , "stopatfirst" , 4 )
  apifun_checkscalar ( "sudoku_findxwing" , verbose , "verbose" , 5 )
  //  
  before = sum(L)
  
  // Search for X-wings
  for d = 1 : 9
    // Get all cells, ordered by column, containg d as a candidate
    [rows,cols] = sudoku_candidatefind ( X , C , L , d )
    len = size(rows,"*")
    // Search for X-wings in rows
    for a = 1 : 8
      // Search for X-Wings in row a
      // Get all cells of row a containing d
      da = find(rows==a)
      //mprintf("[rows] d = %d, a = %d, sda = %d\n", d , a , size(da,"*") )
      if ( size(da,"*") <> 2 ) then
        // This cannot be a X-Wing: try next row
        continue
      end
      for b = a + 1 : 9
        // Search for X-Wings in rows a and b, with b <> a
        // Get all cells of row b containing d
        db = find(rows==b)
        // Check that there are exactly two d in row a and exactly two d in row b
        if ( size(db,"*") <> 2 ) then
          // This cannot be a X-Wing: try next row
          continue
        end
        //mprintf("[rows] d = %d, a = %d, b = %d, sda = %d, sdb = %d\n", d , a , b , size(da,"*")  , size(db,"*") )
        // Check that the 4 cells form a square.
        // These cells should be along the scheme:
        // row a   da(1) ------- da(2)
        //           |             |
        //           |             |
        //           |             |
        // row b   db(1) ------- db(2)
        // The fact that column(da(1)) <= column(da(2)) is already 
        // forced by the ordering generated by candidatefind, which 
        // searches column by column.
        if ( cols(da(1)) <> cols(db(1)) | cols(da(2)) <> cols(db(2)) ) then
          // This cannot be a row X-Wing: try next row
          continue
        end
        // This is a row X-Wing
        // Remove all candidates d from the two columns 
        // da(1) <-> db(1) and da(2) <-> db(2).
        first = %t
        Wrows = [rows(da(1)) rows(db(1))]
        Wcols = [cols(da(1)) cols(da(2))]
        for c = Wcols
          for r = 1 : 9
            if ( X(r,c) == 0 & and(r <> Wrows) ) then
              // This is a cell which does not belong to the X-Wing
              // remove d from the candidates
              if ( find ( find(C(r,c,:)) == d ) <> [] ) then
                [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , d )
                if ( verbose ) then
                  if ( first ) then
                    mprintf("Found row X-Wing %d at rows %s and columns %s \n" , ..
                    d , strcat(string(Wrows), " ") , strcat(string(Wcols), " "))
                    first = %f
                  end
                  mprintf("Removed row X-Wing candidate %d at (%d,%d) (remaining %d candidates)\n",d,r,c,sum(L))
                end
              end
            end
          end
        end
        if ( stopatfirst & sum(L) <> before ) then
          return
        end
      end
    end
    // Search for X-wings in columns
    for a = 1 : 8
      // Search for X-Wings in column a
      // Get all cells of column a containing d
      da = find(cols==a)
      //mprintf("[cols] d = %d, a = %d, sda = %d\n", d , a , size(da,"*") )
      if ( size(da,"*") <> 2 ) then
        // This cannot be a X-Wing: try next column
        continue
      end
      for b = a + 1 : 9
        // Get all cells of column b containing d
        db = find(cols==b)
        // Check that there are exactly two d in column a and exactly two d in column b
        if ( size(db,"*") <> 2 ) then
          // This cannot be a X-Wing: try next column
          continue
        end
        //mprintf("[cols] d = %d, a = %d, b = %d, sda = %d, sdb = %d\n", d , a , b , size(da,"*")  , size(db,"*") )
        // Check that the 4 cells form a square.
        // These cells should be along the scheme:
        //         col a         col b
        //
        //         da(1) ------- db(1)
        //           |             |
        //           |             |
        //           |             |
        //         da(2) ------- db(2)
        // The fact that column(da(1)) <= column(db(1)) is already 
        // forced by the ordering generated by candidatefind, which 
        // searches column by column.
        if ( rows(da(1)) <> rows(db(1)) | rows(da(2)) <> rows(db(2)) ) then
          // This cannot be a row X-Wing: try next row
          continue
        end
        // This is a row X-Wing
        // Remove all candidates d from the two rows 
        // da(1) <-> db(1) and da(2) <-> db(2).
        first = %t
        Wrows = [rows(da(1)) rows(da(2))]
        Wcols = [cols(da(1)) cols(db(1))]
        for r = Wrows
          for c = 1 : 9
            if ( X(r,c) == 0 & and(c <> Wcols) ) then
              // This is a cell which does not belong to the X-Wing
              // remove d from the candidates
              if ( find ( find(C(r,c,:)) == d ) <> [] ) then
                [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , d )
                if ( verbose ) then
                  if ( first ) then
                    mprintf("Found column X-Wing %d at rows %s and columns %s \n" , ..
                    d , strcat(string(Wrows), " ") , strcat(string(Wcols), " "))
                    first = %f
                  end
                  mprintf("Removed column X-Wing candidate %d at (%d,%d) (remaining %d candidates)\n",d,r,c,sum(L))
                end
              end
            end
          end
        end
        if ( stopatfirst & sum(L) <> before ) then
          return
        end
      end
    end
  end
  
endfunction
