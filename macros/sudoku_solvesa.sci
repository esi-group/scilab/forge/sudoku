// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [Y,iter] = sudoku_solvesa ( varargin )
  //   Solves Sudoku.
  //
  // Calling Sequence
  //   Y = sudoku_solvesa ( X )
  //   Y = sudoku_solvesa ( X , timeout )
  //   Y = sudoku_solvesa ( X , timeout , verbose )
  //   [Y,iter] = sudoku_solvesa ( ... )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // timeout: number of seconds after which we give up (default 60)
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  // Y: a 9-by-9 matrix. If Y contains zeros, they stand for unsolved entries. The test and(Y(:) > 0) allows to see if the puzzle is solved.
  // iter: the number of iterations required
  //
  // Description
  //   Uses Simulated Annealing to solve a sudoku.
  //   It might happen that, on output, the sudoku is not solved.
  //
  //   Because of the randomized behaviour of this algorithm, running 
  //   the solver again might lead to a sucessful resolution.
  //   This solver uses the "grand" function to generate random numbers.
  //   Hence, to get reproductible results, we may use the statement 
  //   grand("setsd",123456), which initializes the seed of the random 
  //   number generator.
  //
  //   In general, this algorithm finds a solution of a given sudoku, but 
  //   rather slowly. It might be compared to a fast version of filling 
  //   a sudoku with a pair of dice.
  //
  //   The initial grid is filled with digits so that each block contains 
  //   exactly one digit 1 to 9. The rows and columns constraints are violated
  //   in this initial grid.
  // 
  //   The algorithms then uses a neighbourhood function which select a block,
  //   select two cells in that block, and switch the values.
  //
  //   The improvement is measured with a cost function, which 
  //   count the number of times the row and column constraints are violated.
  //   This is a very inaccurate measure of the progress of the algorithm.
  //   It might lead to sudokus where the cost function value is very low (e.g. 4),
  //   but the number of wrong entries is very high (e.g. 44).
  //
  // Examples
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 0 0 0   0 0 0   5 3 0
  // 9 0 0   0 3 7   0 0 0
  // 0 0 2   3 0 1   0 9 6
  // 0 4 7   6 9 0   1 8 0
  // 0 0 6   7 8 5   0 0 0
  // 0 5 0   0 2 0   9 0 3
  // 0 3 0   9 0 0   6 0 8
  // 8 0 0   0 1 0   4 7 0
  // ]
  // sudoku_solvesa(X)
  // sudoku_solvesa(X,10,%t)
  //
  // Authors
  // Michael Baudin, 2010-2011
  //
  // Bibliography
  //   "Metaheuristics can solve sudoku puzzles", Rhyd Lewis, J Heuristics (2007) 13: 387-401
  //   http://xianblog.wordpress.com/2010/02/23/sudoku-via-simulated-annealing/
  //   http://www.feynmanlectures.info/sudoku/pss.html
  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_solvesa" , rhs , 1:3 )
  apifun_checklhs ( "sudoku_solvesa" , lhs , 1:2 )
  //
  X = varargin(1)
  timeout = apifun_argindefault ( varargin , 2 , 60 )
  verbose = apifun_argindefault ( varargin , 3 , %f )
  //
  // Check type
  apifun_checktype ( "sudoku_solvesa" , X , "X" , 1 , "constant" )
  apifun_checktype ( "sudoku_solvesa" , timeout , "timeout" , 2 , "constant" )
  apifun_checktype ( "sudoku_solvesa" , verbose , "verbose" , 3 , "boolean" )
  //
  // Check size
  apifun_checkdims ( "sudoku_solvesa" , X , "X" , 1 , [9 9] )
  apifun_checkscalar ( "sudoku_solvesa" , timeout , "timeout" , 2 )
  apifun_checkscalar ( "sudoku_solvesa" , verbose , "verbose" , 3 )
  //
  // F(i,j) is true if the cell (i,j) is fixed, i.e. X(i,j) > 0 at input
  F = ( X > 0 )
  //
  // Fill the grid, block by block.
  Y = X
  for i = [1 4 7]
    for j = [1 4 7]
      // Fill the unknown cells for the block (i,j)
      for r = tri(i)
        for c = tri(j)
          if ( Y(r,c) == 0 ) then
            // Get nonzero values in the block
            nnzz = nonzeros(Y(tri(i),tri(j)))'
            // Get the list of digits not fixed yet in this block.
            z = 1:9
            // Remove the fixed digits from the set {1,2,...,9}.
            z(nnzz) = 0
            z = nonzeros(z)
            // Randomly select a digit to fix
            len = size(z,"*")
            t = grand(1,1,"uin",1,len)
            Y(r,c) = z(t)
          end
        end
      end
    end
  end
  //
  // Check that the sudoku is not already solved
  solved = sudoku_issolved ( Y )
  if ( solved ) then
    return
  end
  //
  // Configure the optimization
  tic();
  Proba_start = 0.7;
  It_Pre      = 50;
  It_extern   = %inf;
  // The number of internal loops must be on the order on the cells in the sudoku (i.e. 81)
  It_intern   = 100; 
  Log = %f
  saparams = init_param();
  saparams = add_param ( saparams , "neigh_func" , saneighfunc );
  saparams = add_param ( saparams , "output_func" , saoutfunc );
  saparams = add_param ( saparams , "SDK_timeout" , timeout );
  saparams = add_param ( saparams , "SDK_verbose" , verbose );
  saparams = add_param ( saparams , "SDK_F" , F );
  saparams = add_param ( saparams , "SDK_Tmin" , 0 );
  //
  // Compute the initial temperature and solve
  T0 = mycompute_initial_temp ( Y , sacostfunc , Proba_start , It_Pre , saparams );
  [Y, f_best, mean_list, var_list, temp_list, f_history, x_history , iter] = ..
    myoptim_sa ( Y , sacostfunc , It_extern , It_intern , T0 , Log , saparams );
  // 
  // Extract data
  solved = sudoku_issolved ( Y )
  if ( ~solved ) then
    Y = X
  end
endfunction

// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction
// nonzeros --
// Returns the values in A which are nonzero.
//   nonzeros([0 1 0 2 3 4]) = [1 2 3 4]
//   nonzeros([0 0 0 4]) = 4
function s = nonzeros ( A )
  [i,j,s] = mtlb_find( A )
endfunction
//
// saneighfunc --
// Returns a neighbor sudoku by swapping two non-fixed cells in the current sudoku.
//
// Note:
//   There is an improvement over the algorithm provided by Lewis.
//   Lewis choose randomly a nonfixed cell a in the sudoku.
//   Then, he picks a nonfixed cell b in the same block as a such that
//   a <> b. Nothing guarantees that such couple (a,b) exist.
//   In particular, it may happen that the algorithm picks
//   a cell a in a block where there are 8 fixed cells.
//   In this case, we cannot find a nonfixed cell b different from a. 
//   Therefore, we created algorithm for the search of a, where we
//   search a nonfixed cell in the sudoku. For each cell a,
//   we check that the number of nonfixed cells in the block
//   is greater than 2. This guarantees that we can find
//   a nonfixed cell b in the same block, if we assume that the 
//   sudoku is not already solved. Indeed, it may happen
//   that there is only 1 nonfixed candidate by block.
//   In this case, the initial filled sudoku is a solution
//   and we assume that the algorithm has already returned.
//
//   Once we have found a, there are several ways to arrange
//   the loop so that a nonfixed cell b is found.
//   The naive way searches in the whole sudoku,
//   and checks that 1) b is nonfixed, 2) b is 
//   different from a, 3) b is in the same block as a.
//   In general, this algorithm will perform
//   many loops.
//   Instead, we search directly for random cells 
//   in the same block as a. Then we check that 1) a is 
//   different from b, 2) b is nonfixed.
//
// Arguments
//   X: the current sudoku
//   saparams: the parameters of the SA
//   Y: the neighbourY sudoku
//
function Y = saneighfunc ( X , T , saparams )
  Y = X
  F = get_param ( saparams , "SDK_F" );
  //
  // Check that the suduku is not already solved.
  // This would produce an infinite loop.
  solved = sudoku_issolved ( Y )
  if ( solved ) then
    return
  end
  // Compute the list of non-fixed cells
  nonfixed = find(~F)
  nf = size(nonfixed,"*")
  // Randomly select a non-fixed cell (r,c)
  // for which the block contains more than two nonfixed candidates.
  while ( %t )
    t = grand(1,1,"uin",1,nf)
    s = nonfixed ( t )
    [r,c] = whatcell ( s )
    trir = tri(r)
    tric = tri(c)
    if ( size(find(~F(trir,tric)),"*") > 1 ) then
      break
    end
  end
  // Select a non-fixed cell in the same block
  while ( %t )
    t = grand(1,1,"uin",1,3)
    rr = trir(t)
    t = grand(1,1,"uin",1,3)
    cc = tric(t)
    if ( ~F(rr,cc) & ~(rr==r & cc==c) ) then
      break
    end
  end
  //
  // Swaps the cells (r,c) and (rr,cc)
  if ( %f ) then
    mprintf("Swap (%d,%d)=%d and (%d,%d)=%d\n",r,c,Y(r,c),rr,cc,Y(rr,cc))
  end
  v = Y(r,c)
  Y(r,c) = Y(rr,cc)
  Y(rr,cc) = v
endfunction
//
// Example
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction
//
// sacostfunc --
//   Returns the value of the cost function for the current sudoku.
//   For each row, compute the number of digits from 1 to 9 which are 
//   not present. Same for column. The cost is the sum of these two costs.
//
//   Note
//   This cost function, designed by Lewis, does not favor
//   any digit, but requires two nested loops.
//
//   Note
//   A low value of the cost function does not necessarily mean
//   an improved sudoku, in the sense that the total number of 
//   correct entries is not necessarily reduced.
//   Consider the AI escargot :
//   X = sudoku_readgivens("100007090030020008009600500005300900010080002600004000300000010040000007007000300")
//   The matrix of fixed entries is computed by 
//   F = ( X > 0 )
//   The solution is
//   Z = sudoku_readgivens("162857493534129678789643521475312986913586742628794135356478219241935867897261354")
//   Consider now the two attempts:
//   Y1 = sudoku_readgivens("182817792536429368749653541835369913412185482679274567315942416842357897967681325")
//   Y2 = sudoku_readgivens("158137692736425138429698574285379961914286752673514483392764815541893627867152349")
//   These attempts achieve:
//   Y1: cost = 45, #Wrong = 49 // mprintf("cost=%d, #Wrong = %d", sacostfunc ( list(Y1,F,%f) ) , size(find(Y1<>Z),"*"))
//   Y2: cost = 13, #Wrong = 53 // mprintf("cost=%d, #Wrong = %d", sacostfunc ( list(Y2,F,%f) ) , size(find(Y2<>Z),"*"))
//
//   Note
//   A very low value of the cost function does not necessarily mean
//   a sudoku which is nearly solved, in the sense that the cost function
//   might be very low, but the number of wrong entries might be very large.
//   Consider the AI escargot :
//   X = sudoku_readgivens("100007090030020008009600500005300900010080002600004000300000010040000007007000300")
//   Consider now the attempt:
//   Y3 = sudoku_readgivens("158437296736925148429618573275362981913587462684194735392756814541893627867241359")
//   Y3: cost = 4, #Wrong = 44 // mprintf("cost=%d, #Wrong = %d", sacostfunc ( list(Y3,F,%f) ) , size(find(Y3<>Z),"*"))
//
function cost = sacostfunc ( X )
  // Compute the row cost
  rowcost = zeros(9,1)
  for i = 1 : 9
    for v = 1 : 9
      if ( find(X(i,:)==v) == [] ) then
        rowcost(i) = rowcost(i) + 1;
      end
    end
  end
  // Compute the column cost
  colcost = zeros(9,1)
  for j = 1 : 9
    for v = 1 : 9
      if ( find(X(:,j)==v) == [] ) then
        colcost(j) = colcost(j) + 1;
      end
    end
  end
  // Combine two costs
  cost = sum(rowcost) + sum(colcost)
endfunction
//
// saoutfunc --
//   Allows to stop the algorithm when the sudoku is solved.
//
function stop = saoutfunc ( itExt , x_best , f_best , T , saparams )
  verbose = get_param ( saparams , "SDK_verbose" );
  timeout = get_param ( saparams , "SDK_timeout" );
  Tmin = get_param ( saparams , "SDK_Tmin" );
  if ( verbose ) then
    X = x_best
    mprintf ( "Iter = #%d, f_best = %d, T = %e, x_best=""%s""\n",itExt , f_best , T , sudoku_writegivens(X) )
  end
  stop = ( f_best == 0 | T < Tmin | toc() > timeout )
endfunction
//
// blockcost --
//   Returns the cost function value of the given block in the sudoku.
//   When both these costs are zero, the block is ok.
// Arguments
//   X: the sudoku
//   i: integer in the set [1 4 7], the first row of the block
//   j: integer in the set [1 4 7], the first column of the block
//   rowc: the sum of the costs of the rows,
//   colc: the sum of the costs of the cols.
// Note
//   This measure is a very inaccurate measure of the 
//   location of the wrong blocks in the sudoku.
//   Consider the AI escargot :
//   X = sudoku_readgivens("100007090030020008009600500005300900010080002600004000300000010040000007007000300")
//   The matrix of fixed entries is computed by 
//   F = ( X > 0 )
//   The solution is
//   Z = sudoku_readgivens("162857493534129678789643521475312986913586742628794135356478219241935867897261354")
//   Consider now the attempt:
//   Y = sudoku_readgivens("156847293234521768879693541485312976713986852692574134328765419541239687967148325")
//   The block (7,7) has a rowc and a colc equal to 0, but the number of wrong entries in this 
//   block is equal to 5!
//
function [rowc,colc] = blockcost ( X , i , j )
  colc = sum(abs(sum(X(tri(i),:),"c") - 45))
  rowc = sum(abs(sum(X(:,tri(j)),"r") - 45))
endfunction

///////////////////////////////////////////////////////////////
// These are temporary bug fixes an improvements for some Scilab functions.

// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008 - Yann COLLETTE <yann.collette@renault.com>
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x_best, f_best, mean_list, var_list, temp_list, f_history, x_history , It ] = myoptim_sa(x0, sa_f, ItExt, ItInt, T0, Log, param)
// Simulated annealing
// x0         : initial solution
// f          : objective function
// ItExt      : number of temperature decrease
// ItInt      : number of iterations during one temperature step
// T0         : initial temperature
// Log        : print some message during the run of the optimization
// param      : a parameter list. this list contains the neighobrhood ('neigh_func') and some parameters related to this neighborhood functions (see the 
//              related function to list the available parameters)

[nargout, nargin] = argn();

if ~isdef("param","local") then
  param = [];
end

[temp_law,err]    = myget_param(param,"temp_law",temp_law_default);
[neigh_func,err]  = myget_param(param,"neigh_func",neigh_func_default);
[accept_func,err] = myget_param(param,"accept_func",accept_func_default);
[output_func,err] = myget_param(param,"output_func",[]);

if (~isdef("Log","local")) then
  Log = %F;
end

if (nargout>=6) then
  f_history_defined = %T;
  f_history = list();
else
  f_history_defined = %F;
end

if (nargout>=5) then
  temp_list_defined = %T;
  temp_list = [];
else
  temp_list_defined = %F;
end

if (nargout>=7) then
  x_history_defined = %T;
  x_history = list();
else
  x_history_defined = %F;
end

if ~isdef("sa_f","local") then
  error(gettext("myoptim_sa: sa_f is mandatory"));
else
  if typeof(sa_f)=="list" then
    deff("y=_sa_f(x)","y=sa_f(1)(x, sa_f(2:$))");
  else
    deff("y=_sa_f(x)","y=sa_f(x)");
  end
end

T = T0;

// Some variables needed to record the behavior of the SA
var_list  = [];
mean_list = [];
temp_list = [];

x_current = x0;
f_current = _sa_f(x_current);

x_best = x_current;
f_best = f_current;

for It=1:ItExt
  if ( output_func <> [] ) then
    stop = output_func ( It , x_best , f_best , T , saparams );
    if (stop) then
      break
    end
  end

  f_list = [];
  x_list = list();
  for j=1:ItInt
    x_neigh = neigh_func(x_current,T,param);
    f_neigh = _sa_f(x_neigh);
	trand = grand(1,1,"def")
    if ((f_neigh<=f_current)|(accept_func(f_current,f_neigh,T)>trand)) then
      x_current = x_neigh;
      f_current = f_neigh;
    end
    
    f_list = [f_list f_current];
    
    if (f_best>f_current) then
      x_best = x_current;
      f_best = f_current;
    end
    
    if (x_history_defined) then
      x_list($+1) = x_current;
    end
  end

  if (temp_list_defined) then
    temp_list = [temp_list T];
  end
  if (x_history_defined) then
    x_history($+1) = x_list;
  end
  if (f_history_defined) then
    f_history($+1) = f_list;
  end

  // Computation of step_mean and step_var
  step_mean = mean(f_list);
  step_var  = stdev(f_list);
  mean_list = [mean_list step_mean];
  var_list  = [var_list step_var];
  
  if (Log) then
    printf(gettext("%s: Temperature step %d / %d - T = %f, E(f(T)) = %f var(f(T)) = %f f_best = %f\n"), "myoptim_sa", It, ItExt, T, step_mean, step_var, f_best);
  end

  T = temp_law(T, step_mean, step_var, It, max(size(x_current)), param);
end
endfunction

function T_init = mycompute_initial_temp(x0, cit_f, proba_init, ItMX, param)

if (~isdef('param','local')) then
  param = [];
end

[neigh_func,err]  = myget_param(param,'neigh_func',neigh_func_default);
[type_accept,err] = myget_param(param,'type_accept','sa');

if ~isdef('cit_f','local') then
  error(sprintf(gettext("%s: cit_f is mandatory"),"mycompute_initial_temp"));
else
  if typeof(cit_f)=='list' then
    deff('y=_cit_f(x)','y=cit_f(1)(x, cit_f(2:$))');
  else
    deff('y=_cit_f(x)','y=cit_f(x)');
  end
end

f_list    = [];
x_current = x0;
f_current = _cit_f(x_current);
f_list    = [f_list f_current];

for i=1:ItMX
  x_current = neigh_func(x_current, 0, param);
  f_current = _cit_f(x_current);
  f_list = [f_list f_current];
end

NbInc = 0;
f_sum = 0;

for i=2:size(f_list,2)
  if (f_list(i-1)<f_list(i)) then
    NbInc = NbInc + 1;
    f_sum = f_sum + (f_list(i)-f_list(i-1));
  end
end

if (NbInc>0) then
  f_sum = f_sum / NbInc;
end

if type_accept=='sa' then
  // proba_init = exp(-delta_f/T_init) -> -delta_f / log(proba_init) = T_init
  T_init = - f_sum ./ log(proba_init);
elseif type_accept=='vfsa' then
  T_init = abs(f_sum / log(1/proba_init - 1));
else
  error(sprintf(gettext("%s: error - wrong accept type"),"mycompute_initial_temp"));
end
endfunction

function [result,err] = myget_param(list_name,param_name,param_default)
[nargout,nargin] = argn();
if ~isdef('param_default','local') then
  param_default = [];
end

if ( type(param_default) == 13 ) then
  prot=funcprot()
  funcprot(0);
end
result = param_default;
if ( type(param_default) == 13 ) then
  funcprot(prot);
end

if typeof(list_name)=='plist' then
  if is_param(list_name,param_name) then
    if ( type(param_default) == 13 ) then
      prot=funcprot()
      funcprot(0);
    end
    result = list_name(param_name);
    if ( type(param_default) == 13 ) then
      funcprot(prot);
    end
    if nargout==2 then 
      err = %F; 
    end
  else
    if nargout==2 then 
      err = %T; 
    else
      warning(sprintf(gettext("%s: parameter %s not defined"),"myget_param",param_name));
    end
  end
else
  if nargout==2 then 
    err = %T; 
  else
    warning(sprintf(gettext("%s: not a plist"),"myget_param"));
  end
end
endfunction

