// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [puzzle,solution] = sudoku_generate ( varargin )
  //   Generate a puzzle and its solution.
  //
  // Calling Sequence
  //   puzzle = sudoku_generate ( difficulty )
  //   puzzle = sudoku_generate ( difficulty , verbose )
  //   [puzzle,solution] = sudoku_generate ( ... )
  //
  // Parameters
  // difficulty: a 1-by-1 matrix of doubles, the level of the puzzle, difficulty=1 is easy, difficulty=2 is medium, difficulty=3 is more difficult, difficulty=4 is extremely difficult (default difficulty=2)
  // verbose: a 1-by-1 matrix of booleans. Set to %t to display the state of the matrix. (default = %f)
  // puzzle: a 9-by-9 matrix of doubles, the puzzle matrix, with zeros for unknown entries
  // solution: a 9-by-9 matrix of doubles, the solution matrix without any zero
  //
  // Description
  //   Generates a puzzle and its solution based on the difficulty level.
  //  
  //   The algorithm generates a random sudoku with sudoku_fillrandom. 
  //   Then delete some entries according to the
  //   difficulty. Solves the puzzle and the transposed puzzle. If the
  //   two solutions are equal, the solution is probably unique: returns it.
  //   If not, continue to generate puzzles until unicity is achieved.
  //   This algorithm is less slow (typically less than 30 seconds).
  //   
  //   This algorithm is limited in the sense that the difficulty 
  //   is implied by the number of givens.
  //   Easy               : unknowns from 40 to 45 (i.e. from 36 to 41 givens)
  //   Medium             : unknowns from 46 to 49 (i.e. from 32 to 35 givens)
  //   Difficult          : unknowns from 50 to 53 (i.e. from 28 to 31 givens)
  //   Extremely difficult: unknowns from 54 to 58 (i.e. from 27 to 23 givens)
  //   There is no real link between the number of unknowns and the actual
  //   difficulty of a sudoky puzzle. Therefore, this procedure is of limited
  //   usefulness.
  //
  //   The puzzle created by this algorithm is not symetric.
  //
  // Examples
  // [puzzle,solution] = sudoku_generate ( 1 )
  // [puzzle,solution] = sudoku_generate ( 1 , %t )
  //
  // Authors
  //   Stefan Bleeck, 2005
  //   Michael Baudin, Scilab port, comments, 2010 - 2011
  //
  // Bibliography
  //   "Enumerating possible Sudoku grids", Bertram Felgenhauer, Frazer Jarvis, June 2005

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_generate" , rhs , 0:2 )
  apifun_checklhs ( "sudoku_generate" , lhs , 1:2 )
  //
  difficulty = apifun_argindefault ( varargin , 1 , 2 )
  verbose = apifun_argindefault ( varargin , 2 , %f )
  //
  // Check type
  apifun_checktype ( "sudoku_generate" , difficulty , "difficulty" , 1 , "constant" )
  apifun_checktype ( "sudoku_generate" , verbose , "verbose" , 2 , "boolean" )
  //
  // Check size
  apifun_checkscalar ( "sudoku_generate" , difficulty , "difficulty" , 1 )
  apifun_checkscalar ( "sudoku_generate" , verbose , "verbose" , 5 )
  //
  // Generate a filled puzzle
  solution = sudoku_fillrandom ( verbose )

  iter = 0
  
  //
  // While the solution of the puzzle is not unique, generate a new 
  // puzzle
  while ( %t )
    iter = iter + 1
    if ( verbose ) then
      mprintf ( "==============================\n")
      mprintf ( "[%d] Searching for a puzzle...\n",iter)
    end
    while ( %t ) then
      select difficulty
      case 1
        puzzle = sudoku_delrandom ( solution , 4.72 , 0.5 )
      case 2
        puzzle = sudoku_delrandom ( solution , 5.27 , 0.5 )
      case 3
        puzzle = sudoku_delrandom ( solution , 5.72 , 0.5 )
      case 4
        puzzle = sudoku_delrandom ( solution , 6.22 , 0.5 )
      else
        errmsg = msprintf(gettext("%s: Unexpected difficulty : %d provided while 1 to 4 is expected."), "sudoku_generate", difficulty);
        error(errmsg)
      end
      if ( verbose ) then
        mprintf ( "Number of givens: %d\n",sum(puzzle>0))
      end
      if ( sum(puzzle>0) > 17 ) then
        break
      end
    end
    
    if ( verbose ) then
      mprintf ( "Solve...\n")
    end
	[S, totalsol, score, depth] = sudoku_solvefast( puzzle )
    if ( verbose ) then
      mprintf ( "Number of solutions: %d\n",totalsol)
    end
    if ( totalsol == 1 ) then
      // If unique, this is fine.
      break
    else
      if ( verbose ) then
        mprintf ( "Solution is not unique, try again...\n")
      end
    end
  end
endfunction
