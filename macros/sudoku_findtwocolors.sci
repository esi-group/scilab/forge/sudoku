// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ X , C , L ] = sudoku_findtwocolors ( varargin )
  //   Search for pairs.
  //
  // Calling Sequence
  //   X = sudoku_findtwocolors ( X , C , L )
  //   X = sudoku_findtwocolors ( X , C , L , stopatfirst )
  //   X = sudoku_findtwocolors ( X , C , L , stopatfirst , verbose )
  //   [ X , C ] = sudoku_findtwocolors ( ... )
  //   [ X , C , L ] = sudoku_findtwocolors ( ... )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // stopatfirst: if %t, then stop when one or more candidates have been removed. (default = %t)
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  //
  // Description
  //   Color the sudoku with 2 colors and find inconsistencies.
  //   The algorithm is the following:
  //   * compute conjugate pairs (i.e. pairs of two cells which 
  //     are the only 2 candidates in their rows, columns or blocks),
  //   * add coloring with 2 colors,
  //   * make deductions:
  //     * strategy #1 : if there are two colored cells with color X
  //       in the same row, column or block, remove the candidate d in these cells,
  //     * strategy #2 : if there exist one uncolored cell A visible by two colored 
  //       cells, then A is not a candidate of the uncolored cell.
  //   The Turbot-fish strategy is superseded by the 2-colors strategy.
  //
  // Examples
  // 
  // X = [
  // 1 9 6   4 5 3   7 2 8 
  // 4 3 0   7 6 0   9 5 1 
  // 0 5 7   0 1 9   4 6 3 
  // ..
  // 3 6 5   9 8 7   1 4 2 
  // 9 8 4   1 2 5   6 3 7 
  // 7 2 1   6 3 4   8 9 5 
  // ..
  // 6 7 0   0 4 1   5 8 9 
  // 0 4 9   0 7 0   3 1 6 
  // 0 1 0   0 9 6   2 7 4 
  // ];
  // [C,L] = sudoku_candidates(X);
  // X = sudoku_findtwocolors ( X , C , L , %f , %t );
  //
  // Authors
  //   Michael Baudin, 2010-2011
  //
  // Bibliography
  //   Angus Johnson, http://www.angusj.com/sudoku/hints.php
  

  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_findtwocolors" , rhs , 3:5 )
  apifun_checklhs ( "sudoku_findtwocolors" , lhs , 1:3 )
  //
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  stopatfirst = apifun_argindefault ( varargin , 4 , %t )
  verbose = apifun_argindefault ( varargin , 5 , %f )
  //
  // Check type
  apifun_checktype ( "sudoku_findtwocolors" , X , "X" , 1 , "constant" )
  apifun_checktype ( "sudoku_findtwocolors" , C , "C" , 2 , "hypermat" )
  apifun_checktype ( "sudoku_findtwocolors" , L , "L" , 3 , "constant" )
  apifun_checktype ( "sudoku_findtwocolors" , stopatfirst , "stopatfirst" , 4 , "boolean" )
  apifun_checktype ( "sudoku_findtwocolors" , verbose , "verbose" , 5 , "boolean" )
  //
  // Check size
  apifun_checkdims ( "sudoku_findtwocolors" , X , "X" , 1 , [9 9] )
  apifun_checkdims ( "sudoku_findtwocolors" , C , "C" , 2 , [9 9 9] )
  apifun_checkdims ( "sudoku_findtwocolors" , L , "L" , 3 , [9 9] )
  apifun_checkscalar ( "sudoku_findtwocolors" , stopatfirst , "stopatfirst" , 4 )
  apifun_checkscalar ( "sudoku_findtwocolors" , verbose , "verbose" , 5 )
  //  
  // Color cells with digit d
  for d = 1 : 9
    if ( %f ) then
      mprintf("==============================\n")
      mprintf("Coloring cells with candidate %d\n", d)
    end
    // Get all cells, ordered by column, containg d as a candidate
    [rows,cols] = sudoku_candidatefind ( X , C , L , d )
    len = size(rows,"*")
    if ( len == 0 ) then
      // There are no candidate d in this sudoku: go on to the next digit d
      continue
    end
    //
    // Compute conjugate pairs for this digit
    conjugates = computeconjugates ( rows , cols , verbose )
    nconj = size ( conjugates , "r" )
    if ( nconj == 0 ) then
      // There are no conjugate pair in this sudoku: go on to the next digit d
      continue
    end
    //
    // Try all possible colorings.
    // Make sure that all conjugate pairs are colored at least in one coloring.
    // colpairs(kp) : true if the conjugate pair kp has already been colored once
    colpairs = []
    colpairs(1:nconj) = %f
    // success becomes true at the first successful candidate removal
    success = %f
    // By default, start by coloring the first conjugate pair kp = 1
    startkp = 1
    while ( %t )
      [colors,pairs] = addcoloring ( rows , cols , conjugates , startkp , verbose )
      //
      // Make deductions from this coloring.
      // Break the loop for digit d at the first success, since the 
      // candidates are changed, which, in turn, changes the list of cells
      // containing d.
      //
      // Strategy #1: If there are two colored cells with color X
      // in the same row, column or block,
      // remove the candidate d in these cells.
      [X,C,L,success] = twoinsamegroup ( X , C , L , rows , cols , colors , d , verbose )
      if ( success ) then
        break
      end
      //
      // Strategy #2: If there exist one uncolored cell A visible by two colored
      // cells, then A is not a candidate of the uncolored cell,
      // success: true if we were able to remove a candidate in some cell
      [X,C,L,success] = uncoloredvisible ( X , C , L , rows , cols , colors , d , stopatfirst , verbose )
      if ( success ) then
        break
      end
      //
      // Update the colpairs array
      colpairs = ( colpairs | pairs )
      // If all conjugate pairs have been colored once, then all
      // colorings have been tried: finish.
      if ( size ( find(colpairs) , "*" ) == nconj ) then
        break
      end
      // There is at least one conjugate pair which has never been colored:
      // start the coloring by this pair.
      notcolored = find(~colpairs)
      startkp = notcolored ( 1 )
    end
    if ( success & stopatfirst ) then
      break
    end
  end
endfunction



//
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction
// Returns the location of a cell inside a block.
// The location is row first, column second
// location(7,7) = 1, location(7,8) = 2, location(7,9) = 3
// location(8,7) = 4, location(8,8) = 5, location(8,9) = 6
// location(9,7) = 7, location(9,8) = 8, location(9,9) = 9
function l = location ( i , j )
  l = pmodulo(i-1,3)*3 +1 + pmodulo(j-1,3)
endfunction
// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction

// Compute conjugate pairs
// conjugates: a nc x 3 matrix with conjugate pairs
// conjugates(kp,1): the first cell of the pair
// conjugates(kp,2): the second cell of the pair
// Note:
//   It might happen that two cells are both row and block conjugates,
//   or both column and block conjugate.
//   Without additionnal processing, this pair will appear twice in the conjugate array.
//   This would lead to unnecessary colorings.
//   This is why we keep only unique conjugate pairs in the conjugates array.
function conjugates = computeconjugates ( rows , cols , verbose )
  conjugates = []
  kp = 0
  // Search conjugate pairs in rows
  for r = 1 : 9
    fp = find(rows==r)
    if ( size(fp,"*") == 2 ) then
      kp = kp + 1
      conjugates(kp,1) = fp(1)
      conjugates(kp,2) = fp(2)
    end
  end
  // Search conjugate pairs in columns
  for c = 1 : 9
    fp = find(cols==c)
    if ( size(fp,"*") == 2 ) then
      kp = kp + 1
      conjugates(kp,1) = fp(1)
      conjugates(kp,2) = fp(2)
    end
  end
  // Search conjugate pairs in blocks
  for r = [1 4 7]
    for c = [1 4 7]
      trir = tri(r)
      tric = tri(c)
      fp = find ( rows >= min(trir) & rows <= max(trir) & cols >= min(tric) & cols <= max(tric) )
      if ( size(fp,"*") == 2 ) then
        kp = kp + 1
        conjugates(kp,1) = fp(1)
        conjugates(kp,2) = fp(2)
      end
    end
  end
  // Keep only unique conjugate pairs.
  conjugates = unique(conjugates,"r")
endfunction

// Pring the current 2-colors coloring.
// If colors(k) == -1, color is "*"
// If colors(k) == 0, color is "0"
// If colors(k) == 1, color is "1"
function printcoloring ( rows, cols, colors )
  //
  // Create a matrix of strings
  len = size(rows,"*")
  S(1:9,1:9) = ".";
  for k = 1 : len
    if ( colors(k) == -1 ) then
      S(rows(k),cols(k)) = "*";
    elseif ( colors(k) == 0 ) then
      S(rows(k),cols(k)) = "0";
    elseif ( colors(k) == 1 ) then
      S(rows(k),cols(k)) = "1";
    end
  end
  //
  // Create a matrix of rows suitable for human reading
  k = 0
  for i = 1 : 9
    k = k + 1
    str(k) = ""
    for j = 1 : 9
      str(k) = str(k) + S(i,j)
      if ( modulo(j,3) == 0 ) then
        str(k) = str(k) + "|"
      end
    end
    if ( modulo(i,3) == 0 & i < 9 ) then
      k = k + 1
      str(k) = "------------"
    end
  end
  //
  // Print the rows
  for r = 1 : size(str,"r")
    mprintf("%s\n",str(r))
  end
endfunction

// Print the candidate d, which appears in cells associated with rows and cols.
function printlocations ( rows, cols )
  //
  // Create a matrix of strings
  len = size(rows,"*")
  S(1:9,1:9) = "";
  for k = 1 : len
    S(rows(k),cols(k)) = string(k)
  end
  //
  // Compute the maximum number of digits in a cell
  lmax = 0
  for k = 1 : len
    lmax = max ( [length(S(rows(k),cols(k))) lmax ] )
  end
  // Add a blank at the end
  lmax = lmax + 1
  //
  // Update the matrix of string and pad with blanks up to the max
  for i = 1 : 9
    for j = 1 : 9
      lS = length(S(i,j))
      if ( lS < lmax ) then
        S(i,j) = S(i,j) + blanks(lmax - lS)
      end
    end
  end
  //
  // Create a matrix of rows suitable for human reading
  str = []
  k = 0
  for i = 1 : 9
    k = k + 1
    str(k) = ""
    for j = 1 : 9
      str(k) = str(k) + S(i,j)
      if ( modulo(j,3) == 0 & j < 9 ) then
        str(k) = str(k) + "|"
      end
    end
    if ( modulo(i,3) == 0 & i < 9 ) then
      k = k + 1
      str(k) = ""
      for km = 1 : lmax*9 + 3
        str(k) = str(k) + "-"
      end
    end
  end
  //
  // Print the rows
  for r = 1 : size(str,"r")
    mprintf("%s\n",str(r))
  end
endfunction
//
// Compute the colorings, starting from the conjugate pair #kp.
// Default choice is kp = 1, but other values lead to other colorings.
// colors: the array of colors of the cells
// colors(i): the color of the cell #i, equals -1 if the cell is uncolored, equals 0 or 1 if the cell is colored
// pairs: the array of conjugate pairs which have been colored
// pairs(kp): true if the pair #kp has been colored, false if not
function [colors,pairs] = addcoloring ( rows , cols , conjugates , kp , verbose )
  len = size(rows,"*")
  nconj = size(conjugates,"r")
  // There are two colors : 0 and 1
  // The color -1 stands for "not colorable"
  colors(1:len) = -1
  pairs(1:nconj) = %f
  // 
  // Color the conjugate pair #kp with opposite colors
  pairs(kp) = %t
  colors(conjugates(kp,1)) = 0
  colors(conjugates(kp,2)) = 1
  // Color cells as much as possible
  while ( %t )
    if ( verbose ) then
      //printcoloring ( rows, cols, colors )
    end
    // Stop if the prog variable does not change in this loop:
    // this means that we were not able to color any uncolored cell.
    prog = 0
    // Search a conjugate pair for which one cell is uncolored and the other is colored.
    for kp = 1 : nconj
      k1 = conjugates(kp,1); 
      k2 = conjugates(kp,2);
      if ( colors(k1) == -1 & colors(k2) == 0 ) then
        colors(k1) = 1;
        prog = 1;
        pairs(kp) = %t
        break
      elseif ( colors(k1) == -1 & colors(k2) == 1 ) then
        colors(k1) = 0;
        prog = 1;
        pairs(kp) = %t
        break
      elseif ( colors(k1) == 0 & colors(k2) == -1 ) then
        colors(k2) = 1;
        prog = 1;
        pairs(kp) = %t
        break
      elseif ( colors(k1) == 1 & colors(k2) == -1 ) then
        colors(k2) = 0;
        prog = 1;
        pairs(kp) = %t
        break
      end
    end
    if ( prog == 0 ) then
      // No coloring possible anymore.
      break
    end
  end
endfunction
//
// If there are two colored cells with color X
// in the same row, column or block,
// remove the candidate d in these cells.
// success: true if we were able to remove a candidate in some cell
function [X,C,L,success] = twoinsamegroup ( X , C , L , rows , cols , colors , d , verbose )
  removecolor = -1
  success = %f
  // Search in rows
  if ( removecolor == -1 ) then
    for r = unique(rows)'
      for kc = 0 : 1
        fp = find(rows==r & colors==kc)
        if ( size(fp,"*") > 1 ) then
          // There are two cells with the same color in this row
          removecolor = kc
          break
        end
      end
      if ( removecolor <> -1 ) then
        break
      end
    end
  end
  // Search in columns
  if ( removecolor == -1 ) then
    for c = unique(cols)'
      for kc = 0 : 1
        fp = find(cols==c & colors==kc)
        if ( size(fp,"*") > 1 ) then
          // There are two cells with the same color in this column
          removecolor = kc
          break
        end
      end
      if ( removecolor <> -1 ) then
        break
      end
    end
  end
  // Search in blocks
  if ( removecolor == -1 ) then
    for r = [1 4 7]
      for c = [1 4 7]
        trir = tri(r)
        tric = tri(c)
        for kc = 0 : 1
          fp = find ( rows >= min(trir) & rows <= max(trir) & cols >= min(tric) & cols <= max(tric) & colors==kc )
          if ( size(fp,"*") == 2 ) then
            // There are two cells with the same color in this block
            removecolor = kc
            break
          end
        end
        if ( removecolor <> -1 ) then
          break
        end
      end
      if ( removecolor <> -1 ) then
        break
      end
    end
  end
  // If a removing is to be done.
  firstprint = %t
  if ( removecolor <> -1 ) then
    success = %t
    // Loop over the cells which have the color to be removed.
    for kr = find(colors == removecolor )
      r = rows(kr)
      c = cols(kr)
      [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , d )
      if ( verbose & firstprint ) then
        firstprint = %f
        //mprintf("Colored cells with candidate %d\n", d)
        //printlocations ( rows, cols )
        //printcoloring ( rows, cols, colors )
        // TODO : print the 2-colors chain as a string
      end
      if ( verbose ) then
        mprintf("Removed candidate %d at (%d,%d) in 2-colors chain (same color in row, column or block) (remaining %d candidates)\n",d,r,c,sum(L))
      end
    end  
  end
endfunction
//
// If there exist one uncolored cell A visible by two colored 
// cells, then A is not a candidate of the uncolored cell,
// success: true if we were able to remove a candidate in some cell
function [X,C,L,success] = uncoloredvisible ( X , C , L , rows , cols , colors , d , stopatfirst , verbose )
  len = size(rows,"*")
  nconj = size(conjugates,"r")
  success = %f
  firstverbose = %t
  // Search for an uncolored cell ku
  for ku = find(colors == -1)
    // Compute the witnesses of the cell ku
    witnesses = searchwitnesses ( X , C , L , rows , cols , colors , d , ku , verbose )
    // If there is less than 2 witnesses, there is no hope to apply the strategy
    if ( size ( witnesses , "*" ) < 2 ) then
      continue
    end
    // If the colors of the witnesses are all the same, we cannot 
    // apply our strategy
    witcolors = colors(witnesses)
    if ( min ( witcolors ) == max ( witcolors ) ) then
      continue
    end
    // If, among the witnesses, there are at least two oppositely colored cells,
    // the digit d is certainly not a candidate of the uncolored cell.
    r = rows ( ku )
    c = cols ( ku )
    [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , d )
    if ( verbose ) then
      if ( firstverbose ) then
        firstverbose = %f
        //mprintf("Colored cells with candidate %d\n", d)
        //printlocations ( rows, cols )
        //printcoloring ( rows, cols, colors )
        // TODO : print the 2-colors chain as a string
      end
      mprintf ( "Removed candidate %d at (%d,%d) after 2-colors chain (visibility by oppositely colored cells) (remaining %d candidates)\n",d,r,c,sum(L))
    end
    success = %t
    if ( stopatfirst ) then
      break
    end
  end
endfunction
// 
// Returns the list of colored cells which "see" the given uncolored cell ku.
function witnesses = searchwitnesses ( X , C , L , rows , cols , colors , d , ku , verbose )
  witnesses = []
  ru = rows ( ku )
  cu = cols ( ku )
  // Search for colored kw cells witnesses in rows, columns and blocks
  for kw = find(colors <> -1)
    rw = rows ( kw )
    cw = cols ( kw )
    // visible is true if the cell ku is visible from the cell kw
    visible = %f 
    if ( rw == ru ) then 
      visible = %t
    elseif ( cw == cu ) then
      visible = %t
    elseif ( tri(rw) == tri(ru) & tri(cw) == tri(cu) ) then
      visible = %t
    end
    if ( visible ) then
      witnesses ( $ + 1 ) = kw
    end
  end
endfunction

