// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function C = sudoku_candidatescell ( X , i , j )  
  //   Returns candidates for a cell in a sudoku.
  //
  // Calling Sequence
  //   C = sudoku_candidatescell ( X , i , j )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries.
  // i: an integer index in the interval 1, 2, ..., 9
  // j: an integer index in the interval 1, 2, ..., 9
  // C: a row matrix of candidates for the current cell
  //
  // Description
  //   Returns candidates for the entry (i,j) in a sudoku X.
  //
  // Examples
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 0 0 0   0 0 0   5 3 0
  // 9 0 0   0 3 7   0 0 0
  // 0 0 2   3 0 1   0 9 6
  // 0 4 7   6 9 0   1 8 0
  // 0 0 6   7 8 5   0 0 0
  // 0 5 0   0 2 0   9 0 3
  // 0 3 0   9 0 0   6 0 8
  // 8 0 0   0 1 0   4 7 0
  // ];
  // C = sudoku_candidatescell(X,1,3)
  // C = sudoku_candidatescell(X,3,1)
  // C = sudoku_candidatescell(X,4,7)
  //
  // See also
  //   sudoku_candidateremove
  //   sudoku_candidates
  //   sudoku_candidatescell
  //   sudoku_confirmcell
  //
  // Authors
  //   Cleve Moler, 2009
  //   Michael Baudin, Scilab port and comments, 2010
  //
  // Bibliography
  //   Programming Sudoku, Wei-Meng Lee, 2006
  
    [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_candidatescell" , rhs , 3 )
  apifun_checklhs ( "sudoku_candidatescell" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_candidatescell" , X , "X" , 1 , "constant" )
  apifun_checktype ( "sudoku_candidatescell" , i , "i" , 2 , "constant" )
  apifun_checktype ( "sudoku_candidatescell" , j , "j" , 3 , "constant" )
  //
  // Check size
  apifun_checkdims ( "sudoku_candidatescell" , X , "X" , 1 , [9 9] )
  apifun_checkdims ( "sudoku_candidatescell" , i , "i" , 2 , 1 )
  apifun_checkdims ( "sudoku_candidatescell" , j , "j" , 3 , 1 )
  //
  // Check content
  apifun_checkrange ( "sudoku_candidatescell" , X , "X" , 1 , 0 , 9 )
  apifun_checkrange ( "sudoku_candidatescell" , i , "i" , 2 , 1 , 9 )
  apifun_checkrange ( "sudoku_candidatescell" , j , "j" , 3 , 1 , 9 )
  //
  if ( X(i,j) <> 0 ) then
    C = []
    return
  end
  z = 1:9
  z(nonzeros(X(i,:))) = 0
  z(nonzeros(X(:,j))) = 0
  z(nonzeros(X(tri(i),tri(j)))) = 0
  C = nonzeros(z)
endfunction


// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction
// nonzeros --
// Returns the values in A which are nonzero.
//   nonzeros([0 1 0 2 3 4]) = [1 2 3 4]
//   nonzeros([0 0 0 4]) = 4
function s = nonzeros ( A )
  [i,j,s] = mtlb_find( A )
endfunction


