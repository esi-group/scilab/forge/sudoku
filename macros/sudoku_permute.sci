// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function Y = sudoku_permute ( X )
  //   Permute a sudoku.
  //
  // Calling Sequence
  //   Y = sudoku_permute ( X )
  //
  // Parameters
  // X: a 9x9 matrix
  // Y: a permuted 9x9 matrix.
  //
  //
  // Description
  //   Generates a permuted sudoku Y from the sudoku X by performing permutations of rows within a band,
  //   columns within a band and bands (a set of 3 rows inside a block is a band).
  //   This way, X and Y are within the same class and, if X is a valid sudoku,
  //   then Y is a valid sudoku.
  //
  // Examples
  // X = [
  //     6    7    3    1    5    8    2    4    9  
  //     4    1    8    2    6    9    5    3    7  
  //     9    2    5    4    3    7    8    6    1  
  //     5    8    2    3    4    1    7    9    6  
  //     3    4    7    6    9    2    1    8    5  
  //     1    9    6    7    8    5    3    2    4  
  //     7    5    4    8    2    6    9    1    3  
  //     2    3    1    9    7    4    6    5    8  
  //     8    6    9    5    1    3    4    7    2  
  //  ];
  // Y = sudoku_permute ( X )
  // Y = sudoku_permute ( X )
  // Y = sudoku_permute ( X )
  // Y = sudoku_permute ( X )
  //
  // Authors
  //   Michael Baudin, 2010-2011

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_permute" , rhs , 1 )
  apifun_checklhs ( "sudoku_permute" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_permute" , X , "X" , 1 , "constant" )
  //
  // Check size
  apifun_checkdims ( "sudoku_permute" , X , "X" , 1 , [9 9] )
  //
  Y = X
  // Permute the rows within row-band #1, 2, 3
  for i = [1 4 7]
    r=grand(1,"prm",tri(i)')
    Y(r,:) = Y(tri(i),:)
  end
  // Permute the columns within column-band #1, 2, 3
  for j = [1 4 7]
    r=grand(1,"prm",tri(j)')'
    Y(:,r) = Y(:,tri(j))
  end
  // Permute the row-bands #1, 2, 3
  r = []
  rb=grand(1,"prm",[1 4 7]')
  r(1:3) = tri(rb(1))'
  r(4:6) = tri(rb(2))'
  r(7:9) = tri(rb(3))'
  Y(r,:) = Y(:,:)
  // Permute the columns-bands #1, 2, 3
  r = []
  rb=grand(1,"prm",[1 4 7]')'
  r(1,1:3) = tri(rb(1))
  r(1,4:6) = tri(rb(2))
  r(1,7:9) = tri(rb(3))
  Y(:,r) = Y(:,:)
endfunction
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction

