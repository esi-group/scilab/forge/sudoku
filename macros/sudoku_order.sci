// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function Y = sudoku_order ( X )
  //   Order a sudoku.
  //
  // Calling Sequence
  //   Y = sudoku_order ( X )
  //
  // Parameters
  // X: a 9x9 matrix
  // Y: a permuted 9x9 matrix.
  //
  //
  // Description
  //   Order the rows and columns of a sudoku so that the first block is 
  //
  //   1 2 3
  //
  //   4 5 6
  //
  //   7 8 9
  //
  //   by using relabelling.
  //
  // Examples
  //  X = [
  // 6 7 3   1 5 8   2 4 9
  // 4 1 8   2 6 9   5 3 7
  // 9 2 5   4 3 7   8 6 1
  // ..
  // 5 8 2   3 4 1   7 9 6
  // 3 4 7   6 9 2   1 8 5
  // 1 9 6   7 8 5   3 2 4
  // ..
  // 7 5 4   8 2 6   9 1 3
  // 2 3 1   9 7 4   6 5 8
  // 8 6 9   5 1 3   4 7 2
  // ];
  // Y = sudoku_order ( X )
  //
  // Authors
  //   Michael Baudin, 2010-2011

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_order" , rhs , 1 )
  apifun_checklhs ( "sudoku_order" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_order" , X , "X" , 1 , "constant" )
  //
  // Check size
  apifun_checkdims ( "sudoku_order" , X , "X" , 1 , [9 9] )
  //
  Y = X
  // Set the map from (i,j) to the target
  map = [
  1 1 1
  1 2 2
  1 3 3
  2 1 4
  2 2 5
  2 3 6
  3 1 7
  3 2 8
  3 3 9
  ]
  // Relabel the (i,j) into the target
  for i = 1:3
  for j=  1:3
    target = find(map(:,1)==i & map(:,2)==j)
    source = Y(i,j)
    if ( target <> source ) then
    a = find(Y==target)
    b = find(Y==source)
    T = Y
    T(a) = Y(b)
    T(b) = Y(a)
    Y = T
    end
  end
  end
endfunction
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction

