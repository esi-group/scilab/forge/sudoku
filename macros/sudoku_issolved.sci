// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [solved,code,irow,icol] = sudoku_issolved ( X )
  // See if the sudoku is solved.
  //
  // Calling Sequence
  //   solved = sudoku_issolved ( X )
  //   [solved,code] = sudoku_issolved ( X )
  //   [solved,code,irow] = sudoku_issolved ( X )
  //   [solved,code,irow,icol] = sudoku_issolved ( X )
  //
  // Parameters
  // X: a 9-by-9 matrix of doubles, with 0 for unknown entries
  // solved: a 1-by-1 matrix of booleans, %t if the sudoku X is solved
  // code: a 1-by-1 matrix of strings. If code="totalsum", then the total of the entries is not 405. If code="subsquare" then a subsquare has a sum different from 45. If code="rowsum", then the sum of entries in a row is not 45. If code="colsum", then the sum of entries in a column is not 45.
  // irow: an empty matrix, or the integer for the failing row or failing subsquare row.
  // icol: an empty matrix, or the integer for the failing column or failing subsquare column.
  //
  // Description
  // Find out if the solution is found.
  //
  // Examples
  // X = [
  //     6    7    3    1    5    8    2    4    9
  //     4    1    8    2    6    9    5    3    7
  //     2    9    5    4    3    7    8    6    1
  //     5    8    2    3    4    1    7    9    6
  //     3    4    7    6    9    2    1    8    5
  //     1    9    6    7    8    5    3    2    4
  //     7    5    4    8    2    6    9    1    3
  //     2    3    1    9    7    4    6    5    8
  //     8    6    9    5    1    3    4    7    2
  // ];
  // [solved,code,irow,icol] = sudoku_issolved(X)
  //
  // X = [
  //     6    7    3    1    5    8    2    4    9  
  //     4    1    8    2    6    9    5    3    7  
  //     9    2    5    4    3    7    8    6    1  
  //     5    8    2    3    4    1    7    9    6  
  //     3    4    7    6    9    2    1    8    5  
  //     1    9    6    7    8    5    3    2    4  
  //     7    5    4    8    2    6    9    1    3  
  //     2    3    1    9    7    4    6    5    8  
  //     8    6    9    5    1    3    4    7    2  
  //  ];
  // [solved,code,irow,icol] = sudoku_issolved(X)
  //
  // Authors
  // Stefan Bleeck 2005
  // Michael Baudin, Scilab port, comments and improvements, 2010-2011

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_issolved" , rhs , 1:5 )
  apifun_checklhs ( "sudoku_issolved" , lhs , 1:4 )
  //
  // Check type
  apifun_checktype ( "sudoku_issolved" , X , "X" , 1 , "constant" )
  //
  // Check size
  apifun_checkdims ( "sudoku_issolved" , X , "X" , 1 , [9 9] )
  //

  solved=%t; 
  code = []
  irow = []
  icol = []
  // assume found
  // shortcut:
  if sum(X) ~= 405 then
    solved=%f
    code = "totalsum"
    return
  end
  
  for irow=1:9
    for icol=1:9
      // subsquare
      iii=tri ( irow )
      jjj=tri ( icol )
      if sum(X(iii,jjj)) ~= 45 then
        solved=%f
        code = "subsquare"
        return
      end
    end
  end
  for irow=1:9
    if sum(X(irow,:))~=45 then
      // test all rows
      solved=%f
      code = "rowsum"
      return
    end
  end
  for icol=1:9
    if sum(X(:,icol))~=45 then
      // test all columns
      solved=%f
      code = "colsum"
      return
    end
  end
  code = []
  irow = []
  icol = []
endfunction

// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction

