// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [X, maxlevel] = sudoku_solverecursive ( varargin )
  //   Solves Sudoku using naked singles and recursive backtracking.
  //
  // Calling Sequence
  //   Y = sudoku_solverecursive ( X )
  //   Y = sudoku_solverecursive ( X , verbose )
  //   Y = sudoku_solverecursive ( X , verbose , level )
  //   [Y,maxlevel] = sudoku_solverecursive ( ... )
  //
  // Parameters
  // X: a 9-by-9 matrix of doubles, with 0 for unknown entries
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  // level : the current level of recursive backtracking (default level = 0). This parameter is used internally to compute maxlevel. Users should not use this argument.
  // maxlevel: the maximum level of the call tree in the nested recursive search used in the guess-based strategy
  //
  // Description
  //   Using Recursive Backtracking.
  //   From an algorithm published in The Mathworks News and Notes, 2009.
  // 
  //   There are several main improvements over the original algorithm.
  //   The matrix of candidates used in the logic process searching for naked singles
  //   is updated instead of being re-computed each time.
  //   When guessing is necessary, we do not take the first empty cell,
  //   but chose the cell with the lowest number of candidates.
  //   When we browse the candidates, we permute randomly the candidates
  //   so that the routine can be used to generate random grids.
  //   
  //
  // Examples
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 0 0 0   0 0 0   5 3 0
  // 9 0 0   0 3 7   0 0 0
  // 0 0 2   3 0 1   0 9 6
  // 0 4 7   6 9 0   1 8 0
  // 0 0 6   7 8 5   0 0 0
  // 0 5 0   0 2 0   9 0 3
  // 0 3 0   9 0 0   6 0 8
  // 8 0 0   0 1 0   4 7 0
  // ]
  // sudoku_solverecursive(X)
  //
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 9 0 0   0 0 4   0 3 6
  // 0 0 0   0 7 2   0 0 9
  // 3 0 0   0 0 0   0 0 5
  // 0 1 4   0 0 0   8 0 0
  // 0 2 5   0 1 0   0 0 0
  // 0 0 6   1 0 0   0 0 3
  // 0 0 0   0 0 6   0 4 0
  // 0 0 2   4 8 9   0 0 0
  // ]
  // sudoku_solverecursive(X)
  //
  // X = [
  // 1 2 0    0 3 0    0 4 0  
  // 6 0 0    0 0 0    0 0 3  
  // 3 0 4    0 0 0    5 0 0  
  // 2 0 0    8 0 6    0 0 0  
  // 8 0 0    0 1 0    0 0 6  
  // 0 0 0    7 0 5    0 0 0  
  // 0 0 7    0 0 0    6 0 0  
  // 4 0 0    0 0 0    0 0 8  
  // 0 3 0    0 4 0    0 2 0  
  // ]
  // sudoku_solverecursive(X)
  //
  // X = [
  // 0 2 0   0 3 0   0 4 0
  // 6 0 0   0 0 0   0 0 3
  // 0 0 4   0 0 0   5 0 0
  // 0 0 0   8 0 6   0 0 0
  // 8 0 0   0 1 0   0 0 6
  // 0 0 0   7 0 5   0 0 0
  // 0 0 7   0 0 0   6 0 0
  // 4 0 0   0 0 0   0 0 8
  // 0 3 0   0 4 0   0 2 0
  // ]
  // sudoku_solverecursive(X)
  //
  // Authors
  //   Cleve Moler, 2009
  //   Michael Baudin, Scilab port, 2010-2011
  
  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_solverecursive" , rhs , 1:3 )
  apifun_checklhs ( "sudoku_solverecursive" , lhs , 1:3 )
  //
  X = varargin(1)
  verbose = apifun_argindefault ( varargin , 2 , %f )
  level = apifun_argindefault ( varargin , 3 , 0 )
  //
  // Check type
  apifun_checktype ( "sudoku_solverecursive" , X , "X" , 1 , "constant" )
  apifun_checktype ( "sudoku_solverecursive" , verbose , "verbose" , 2 , "boolean" )
  apifun_checktype ( "sudoku_solverecursive" , level , "level" , 3 , "constant" )
  //
  // Check size
  apifun_checkdims ( "sudoku_solverecursive" , X , "X" , 1 , [9 9] )
  apifun_checkscalar ( "sudoku_solverecursive" , verbose , "verbose" , 2 )
  apifun_checkscalar ( "sudoku_solverecursive" , level , "level" , 3 )
  //
  // Initialize the maxlevel
  maxlevel = level
  //
  // Solve with naked singles.
  [C,L] = sudoku_candidates ( X )
  while ( %t )
    if ( verbose ) then
      mprintf("Eliminating naked singles (remaining %d unknowns)\n",sum(X==0))
    end
    before = sum(L)
    [X,C,L] = sudoku_findnakedsubset ( X , C , L , 1 , %f , verbose )
    if ( sum(L) == before ) then
      break
    end
  end
  // Return for impossible puzzles.
  e = findfirst(X==0 & L==0)
  if ( e <> [] ) then
    if ( verbose ) then
      mprintf("Impossible puzzle !\n")
    end
    return
  end
  s = findfirst(X==0 & L==1)
  
  if ( and(X(:) > 0) ) then
    return
  end
  
  // 
  // Recursive backtracking. 
  Y = X 
  // The cell which provides the lowest number of candidates
  cmin = mincandidates ( X , L )
  [imin,jmin] = whatcell ( cmin )
  // Iterate over candidates. 
  for r = grand(1,"prm",find(C(imin,jmin,:))')' 
    if ( verbose ) then
      mprintf("[%d] Guessing %d at (%d,%d) (remaining %d unknowns)\n",level,r,imin,jmin,sum(X==0))
    end
    X = Y 
    // Insert a tentative value. 
    X(cmin) = r              
    // Recursive call. 
    [X,Gmaxlevel] = sudoku_solverecursive ( X , verbose , level + 1 )
    maxlevel = max( [Gmaxlevel level] )
    if and(X(:) > 0) then
      // Found a solution.
      return 
    end 
  end 
endfunction

function i = findfirst ( A )
  i = find( A )
  if ( i <> [] ) then
    i = i(1)
  end
endfunction

// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction

// Returns the index of the unknown cell with the minimum number of candidates
function k = mincandidates ( X , L )
  nbc = %inf
  k = []
  for s = find(X == 0)
    if ( L(s) < nbc ) then
      k =  s
      nbc = L(s)
    end
  end
endfunction

