// Copyright (C) 2010-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function V = sudoku_visiblefrom2 ( a , b )
  //   Compute the cells visible from both cell a and b.
  //
  // Calling Sequence
  //   V = sudoku_isvisiblefrom2 ( a , b )
  //
  // Parameters
  // a: a 1x2 matrix, where a(1) is a row index and a(2) is a column index
  // b: a 1x2 matrix, where b(1) is a row index and b(2) is a column index
  // V: a 9x9 boolean matrix. V(i,j) is true if the cell (i,j) is visible from both cell a and cell b.
  //
  // Description
  //   Returns the matrix of cells which are visible from both the cells
  //   a and b. The cell p is visible from both cell a and b if :
  //   * a and b are in the same row and p is on this row,
  //   * a and b are in the same column and p is on this column,
  //   * p in the same row as a and on the same column as b,
  //   * p in the same column as a and on the same row as b,
  //   * p in the same box as a and on the same row as b,
  //   * p in the same box as a and on the same column as b,
  //   * p in the same row as a and on the same box as b,
  //   * p in the same column as a and on the same box as b,
  //   * p in the same box as a and b.
  //
  // Examples
  // 
  // V = sudoku_visiblefrom ( [1 2] , [2 3] ); sudoku_print(V)
  //
  // Authors
  //   Michael Baudin, 2010-2011
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "sudoku_isvisiblefrom2" , rhs , 2 )
  apifun_checklhs ( "sudoku_isvisiblefrom2" , lhs , 1 )
  //
  // Check type
  apifun_checktype ( "sudoku_isvisiblefrom2" , a , "a" , 1 , "constant" )
  apifun_checktype ( "sudoku_isvisiblefrom2" , b , "b" , 2 , "constant" )
  //
  // Check size
  apifun_checkvector ( "sudoku_isvisiblefrom2" , a , "a" , 1 , 2 )
  apifun_checkvector ( "sudoku_isvisiblefrom2" , b , "b" , 2 , 2 )
  //
  V = ( zeros(9,9) == 1 )
  Va = sudoku_visiblefrom ( a )
  Vb = sudoku_visiblefrom ( b )
  V = ( Va & Vb )
endfunction
//
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction

