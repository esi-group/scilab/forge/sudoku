// Copyright (C) 2010 - DIGITEO - Michael Baudin

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.

//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
mprintf("Updating algos\n");
helpdir = fullfile(cwd,"algos");
funmat = [
"sudoku_candidatefind"
"sudoku_candidateprint"
"sudoku_candidateremove"
"sudoku_candidates"
"sudoku_candidatescell"
"sudoku_candidatesmerge"
"sudoku_cellcandidates"
"sudoku_confirmcell"
"sudoku_findhiddensubset"
"sudoku_findlocked"
"sudoku_findnakedsubset"
"sudoku_findtwocolors"
"sudoku_findxcycle"
"sudoku_findxwing"
"sudoku_iscandsingle"
"sudoku_islatin"
"sudoku_issolved"
"sudoku_locatenakedsubset"
"sudoku_visiblefrom"
"sudoku_visiblefrom2"
];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "sudoku";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
mprintf("Updating generate\n");
helpdir = fullfile(cwd,"generate");
funmat = [
"sudoku_create"
"sudoku_delrandom"
"sudoku_fillrandom"
"sudoku_generate"
"sudoku_generate2"
"sudoku_generate3"
"sudoku_rootsolution"
];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "sudoku";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
mprintf("Updating readwrite\n");
helpdir = fullfile(cwd,"readwrite");
funmat = [
"sudoku_print"
"sudoku_readgivens"
"sudoku_readsdk"
"sudoku_readsdm"
"sudoku_readsdmnb"
"sudoku_readss"
"sudoku_writegivens"
"sudoku_writesdk"
"sudoku_writess"
];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "sudoku";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
mprintf("Updating solve\n");
helpdir = fullfile(cwd,"solve");
funmat = [
"sudoku_solve"
"sudoku_solveai"
"sudoku_solvebylogic"
"sudoku_solverecursive"
"sudoku_solvesa"
"sudoku_solvefast"
];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "sudoku";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
//
mprintf("Updating util\n");
helpdir = fullfile(cwd,"util");
funmat = [
"sudoku_order"
"sudoku_permute"
"sudoku_latinsquare"
"sudoku_getpath"
];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "sudoku";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

















