// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


function candidates = candcell ( C , i , j )
  candidates = find(C(i,j,:))
endfunction

X = [
0 0 0   0 0 0   0 0 0
0 0 0   0 0 0   5 3 0
9 0 0   0 3 7   0 0 0
..
0 0 2   3 0 1   0 9 6
0 4 7   6 9 0   1 8 0
0 0 6   7 8 5   0 0 0
..
0 5 0   0 2 0   9 0 3
0 3 0   9 0 0   6 0 8
8 0 0   0 1 0   4 7 0
];
[C,L] = sudoku_candidates(X);

if ( %f ) then
for i = 1 : 9
  for j = 1 : 9
    if ( C(i,j).entries <> [] ) then
      mprintf("CE(%d,%d).entries = [%s]'';\n",i,j,strcat(string(C(i,j).entries)," "))
    end
  end
end
end  
assert_checkequal ( candcell(C,1,1) , [1 2 3 4 5 6 7] ) ;
assert_checkequal ( candcell(C,1,2) , [1 2 6 7 8] ) ;
assert_checkequal ( candcell(C,1,3) , [1 3 4 5 8] ) ;
assert_checkequal ( candcell(C,1,4) , [1 2 4 5 8] ) ;
assert_checkequal ( candcell(C,1,5) , [4 5 6] ) ;
assert_checkequal ( candcell(C,1,6) , [2 4 6 8 9] ) ;
assert_checkequal ( candcell(C,1,7) , [2 7 8] ) ;
assert_checkequal ( candcell(C,1,8) , [1 2 4 6] ) ;
assert_checkequal ( candcell(C,1,9) , [1 2 4 7 9] ) ;
assert_checkequal ( candcell(C,2,1) , [1 2 4 6 7] ) ;
assert_checkequal ( candcell(C,2,2) , [1 2 6 7 8] ) ;
assert_checkequal ( candcell(C,2,3) , [1 4 8] ) ;
assert_checkequal ( candcell(C,2,4) , [1 2 4 8] ) ;
assert_checkequal ( candcell(C,2,5) , [4 6] ) ;
assert_checkequal ( candcell(C,2,6) , [2 4 6 8 9] ) ;
assert_checkequal ( candcell(C,2,9) , [1 2 4 7 9] ) ;
assert_checkequal ( candcell(C,3,2) , [1 2 6 8] ) ;
assert_checkequal ( candcell(C,3,3) , [1 4 5 8] ) ;
assert_checkequal ( candcell(C,3,4) , [1 2 4 5 8] ) ;
assert_checkequal ( candcell(C,3,7) , [2 8] ) ;
assert_checkequal ( candcell(C,3,8) , [1 2 4 6] ) ;
assert_checkequal ( candcell(C,3,9) , [1 2 4] ) ;
assert_checkequal ( candcell(C,4,1) , [5] ) ;
assert_checkequal ( candcell(C,4,2) , [8] ) ;
assert_checkequal ( candcell(C,4,5) , [4] ) ;
assert_checkequal ( candcell(C,4,7) , [7] ) ;
assert_checkequal ( candcell(C,5,1) , [3 5] ) ;
assert_checkequal ( candcell(C,5,6) , [2] ) ;
assert_checkequal ( candcell(C,5,9) , [2 5] ) ;
assert_checkequal ( candcell(C,6,1) , [1 3] ) ;
assert_checkequal ( candcell(C,6,2) , [1 9] ) ;
assert_checkequal ( candcell(C,6,7) , [2 3] ) ;
assert_checkequal ( candcell(C,6,8) , [2 4] ) ;
assert_checkequal ( candcell(C,6,9) , [2 4] ) ;
assert_checkequal ( candcell(C,7,1) , [1 4 6 7] ) ;
assert_checkequal ( candcell(C,7,3) , [1 4] ) ;
assert_checkequal ( candcell(C,7,4) , [4 8] ) ;
assert_checkequal ( candcell(C,7,6) , [4 6 8] ) ;
assert_checkequal ( candcell(C,7,8) , [1] ) ;
assert_checkequal ( candcell(C,8,1) , [1 2 4 7] ) ;
assert_checkequal ( candcell(C,8,3) , [1 4] ) ;
assert_checkequal ( candcell(C,8,5) , [4 5 7] ) ;
assert_checkequal ( candcell(C,8,6) , [4] ) ;
assert_checkequal ( candcell(C,8,8) , [1 2 5] ) ;
assert_checkequal ( candcell(C,9,2) , [2 6 9] ) ;
assert_checkequal ( candcell(C,9,3) , [9] ) ;
assert_checkequal ( candcell(C,9,4) , [5] ) ;
assert_checkequal ( candcell(C,9,6) , [3 6] ) ;
assert_checkequal ( candcell(C,9,9) , [2 5] ) ;

LE = [
    7.    5.    5.    5.    3.    5.    3.    4.    5.  
    5.    5.    3.    4.    2.    5.    0.    0.    5.  
    0.    4.    4.    5.    0.    0.    2.    4.    3.  
    1.    1.    0.    0.    1.    0.    1.    0.    0.  
    2.    0.    0.    0.    0.    1.    0.    0.    2.  
    2.    2.    0.    0.    0.    0.    2.    2.    2.  
    4.    0.    2.    2.    0.    3.    0.    1.    0.  
    4.    0.    2.    0.    3.    1.    0.    3.    0.  
    0.    3.    1.    1.    0.    2.    0.    0.    2.  
];
assert_checkequal ( L , LE );

// Check that each entry is unique in the candidates
// Notice that the 3 appears 6 times :
// * in the column 2, rows 7,8,9
// * in the column 3, rows 7,8,9
X = [
    1.    4.    7.    0.    0.    8.    0.    9.    0.  
    2.    5.    0.    0.    0.    0.    0.    0.    0.  
    3.    6.    0.    0.    0.    0.    0.    0.    0.  
    4.    1.    0.    0.    0.    0.    0.    0.    0.  
    5.    2.    0.    0.    0.    0.    0.    0.    0.  
    6.    0.    0.    0.    0.    0.    0.    0.    0.  
    7.    0.    0.    0.    0.    0.    0.    0.    0.  
    8.    0.    0.    0.    0.    0.    0.    0.    0.  
    9.    0.    0.    0.    0.    0.    0.    0.    0.  
];
[C,L] = sudoku_candidates(X);
assert_checkequal ( candcell(C,1,4) , [2 3 5 6] ) ;
assert_checkequal ( candcell(C,1,5) , [2 3 5 6] ) ;
assert_checkequal ( candcell(C,1,7) , [2 3 5 6] ) ;
assert_checkequal ( candcell(C,1,9) , [2 3 5 6] ) ;
assert_checkequal ( candcell(C,2,3) , [8 9] ) ;
assert_checkequal ( candcell(C,2,4) , [1 3 4 6 7 9] ) ;
assert_checkequal ( candcell(C,2,5) , [1 3 4 6 7 9] ) ;
assert_checkequal ( candcell(C,2,6) , [1 3 4 6 7 9] ) ;
assert_checkequal ( candcell(C,2,7) , [1 3 4 6 7 8] ) ;
assert_checkequal ( candcell(C,2,8) , [1 3 4 6 7 8] ) ;
assert_checkequal ( candcell(C,2,9) , [1 3 4 6 7 8] ) ;
assert_checkequal ( candcell(C,3,3) , [8 9] ) ;
assert_checkequal ( candcell(C,3,4) , [1 2 4 5 7 9] ) ;
assert_checkequal ( candcell(C,3,5) , [1 2 4 5 7 9] ) ;
assert_checkequal ( candcell(C,3,6) , [1 2 4 5 7 9] ) ;
assert_checkequal ( candcell(C,3,7) , [1 2 4 5 7 8] ) ;
assert_checkequal ( candcell(C,3,8) , [1 2 4 5 7 8] ) ;
assert_checkequal ( candcell(C,3,9) , [1 2 4 5 7 8] ) ;
assert_checkequal ( candcell(C,4,3) , [3 8 9] ) ;
assert_checkequal ( candcell(C,4,4) , [2 3 5 6 7 8 9] ) ;
assert_checkequal ( candcell(C,4,5) , [2 3 5 6 7 8 9] ) ;
assert_checkequal ( candcell(C,4,6) , [2 3 5 6 7 9] ) ;
assert_checkequal ( candcell(C,4,7) , [2 3 5 6 7 8 9] ) ;
assert_checkequal ( candcell(C,4,8) , [2 3 5 6 7 8] ) ;
assert_checkequal ( candcell(C,4,9) , [2 3 5 6 7 8 9] ) ;
assert_checkequal ( candcell(C,5,3) , [3 8 9] ) ;
assert_checkequal ( candcell(C,5,4) , [1 3 4 6 7 8 9] ) ;
assert_checkequal ( candcell(C,5,5) , [1 3 4 6 7 8 9] ) ;
assert_checkequal ( candcell(C,5,6) , [1 3 4 6 7 9] ) ;
assert_checkequal ( candcell(C,5,7) , [1 3 4 6 7 8 9] ) ;
assert_checkequal ( candcell(C,5,8) , [1 3 4 6 7 8] ) ;
assert_checkequal ( candcell(C,5,9) , [1 3 4 6 7 8 9] ) ;
assert_checkequal ( candcell(C,6,2) , [3 7 8 9] ) ;
assert_checkequal ( candcell(C,6,3) , [3 8 9] ) ;
assert_checkequal ( candcell(C,6,4) , [1 2 3 4 5 7 8 9] ) ;
assert_checkequal ( candcell(C,6,5) , [1 2 3 4 5 7 8 9] ) ;
assert_checkequal ( candcell(C,6,6) , [1 2 3 4 5 7 9] ) ;
assert_checkequal ( candcell(C,6,7) , [1 2 3 4 5 7 8 9] ) ;
assert_checkequal ( candcell(C,6,8) , [1 2 3 4 5 7 8] ) ;
assert_checkequal ( candcell(C,6,9) , [1 2 3 4 5 7 8 9] ) ;
assert_checkequal ( candcell(C,7,2) , [3] ) ;
assert_checkequal ( candcell(C,7,3) , [1 2 3 4 5 6] ) ;
assert_checkequal ( candcell(C,7,4) , [1 2 3 4 5 6 8 9] ) ;
assert_checkequal ( candcell(C,7,5) , [1 2 3 4 5 6 8 9] ) ;
assert_checkequal ( candcell(C,7,6) , [1 2 3 4 5 6 9] ) ;
assert_checkequal ( candcell(C,7,7) , [1 2 3 4 5 6 8 9] ) ;
assert_checkequal ( candcell(C,7,8) , [1 2 3 4 5 6 8] ) ;
assert_checkequal ( candcell(C,7,9) , [1 2 3 4 5 6 8 9] ) ;
assert_checkequal ( candcell(C,8,2) , [3] ) ;
assert_checkequal ( candcell(C,8,3) , [1 2 3 4 5 6] ) ;
assert_checkequal ( candcell(C,8,4) , [1 2 3 4 5 6 7 9] ) ;
assert_checkequal ( candcell(C,8,5) , [1 2 3 4 5 6 7 9] ) ;
assert_checkequal ( candcell(C,8,6) , [1 2 3 4 5 6 7 9] ) ;
assert_checkequal ( candcell(C,8,7) , [1 2 3 4 5 6 7 9] ) ;
assert_checkequal ( candcell(C,8,8) , [1 2 3 4 5 6 7] ) ;
assert_checkequal ( candcell(C,8,9) , [1 2 3 4 5 6 7 9] ) ;
assert_checkequal ( candcell(C,9,2) , [3] ) ;
assert_checkequal ( candcell(C,9,3) , [1 2 3 4 5 6] ) ;
assert_checkequal ( candcell(C,9,4) , [1 2 3 4 5 6 7 8] ) ;
assert_checkequal ( candcell(C,9,5) , [1 2 3 4 5 6 7 8] ) ;
assert_checkequal ( candcell(C,9,6) , [1 2 3 4 5 6 7] ) ;
assert_checkequal ( candcell(C,9,7) , [1 2 3 4 5 6 7 8] ) ;
assert_checkequal ( candcell(C,9,8) , [1 2 3 4 5 6 7 8] ) ;
assert_checkequal ( candcell(C,9,9) , [1 2 3 4 5 6 7 8] ) ;
LE = [
0 0 0   4 4 0   4 0 4 
0 0 2   6 6 6   6 6 6 
0 0 2   6 6 6   6 6 6 
..
0 0 3   7 7 6   7 6 7 
0 0 3   7 7 6   7 6 7 
0 4 3   8 8 7   8 7 8 
..
0 1 6   8 8 7   8 7 8 
0 1 6   8 8 8   8 7 8 
0 1 6   8 8 7   8 8 8 
];
assert_checkequal ( L , LE );

