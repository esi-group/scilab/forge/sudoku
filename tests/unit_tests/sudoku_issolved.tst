// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


X = [
0 0 0   0 0 0   0 0 0
0 0 0   0 0 0   5 3 0
9 0 0   0 3 7   0 0 0
0 0 2   3 0 1   0 9 6
0 4 7   6 9 0   1 8 0
0 0 6   7 8 5   0 0 0
0 5 0   0 2 0   9 0 3
0 3 0   9 0 0   6 0 8
8 0 0   0 1 0   4 7 0
];
[solved,code,irow,icol] = sudoku_issolved(X);
assert_checkequal ( solved , %f );
assert_checkequal ( code , "totalsum" );
assert_checkequal ( irow , [] );
assert_checkequal ( icol , [] );

X = [
    6    7    3    1    5    8    2    4    9  
    4    1    8    2    6    9    5    3    7  
    9    2    5    4    3    7    8    6    1  
    5    8    2    3    4    1    7    9    6  
    3    4    7    6    9    2    1    8    5  
    1    9    6    7    8    5    3    2    4  
    7    5    4    8    2    6    9    1    3  
    2    3    1    9    7    4    6    5    8  
    8    6    9    5    1    3    4    7    2  
 ];
[solved,code,irow,icol] = sudoku_issolved(X);
assert_checkequal ( solved , %t );
assert_checkequal ( code , [] );
assert_checkequal ( irow , [] );
assert_checkequal ( icol , [] );

X = [
    6    7    3    1    5    8    2    4    9  
    4    1    8    2    6    9    5    3    7  
    2    9    5    4    3    7    8    6    1  
    5    8    2    3    4    1    7    9    6  
    3    4    7    6    9    2    1    8    5  
    1    9    6    7    8    5    3    2    4  
    7    5    4    8    2    6    9    1    3  
    2    3    1    9    7    4    6    5    8  
    8    6    9    5    1    3    4    7    2  
 ];
[solved,code,irow,icol] = sudoku_issolved(X);
assert_checkequal ( solved , %f );
assert_checkequal ( code , "colsum" );
assert_checkequal ( irow , 9 );
assert_checkequal ( icol , 1 );

