// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


path = sudoku_getpath();
filename = fullfile(path,"tests","unit_tests","mycollection.sdm");

nb = sudoku_readsdmnb (filename);
assert_checkequal ( nb , 10 );


