// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction
////////////////////////////////////////
// X-cycle (length 5) : 2(9,4)=2(9,6)=2(2,6)=2(2,4)-2(7,4)-2(9,4)
// Removed X-Cycle candidate 2 at (7,4) (remaining 57 candidates)
// X-cycle (length 7) : 5(1,5)=5(7,5)=5(7,9)=5(9,7)=5(3,7)=5(3,9)-5(1,9)-5(1,5)
// Removed X-Cycle candidate 5 at (1,9) (remaining 56 candidates)
// X-cycle (length 7) : 5(7,5)=5(7,9)-5(2,9)-5(3,9)=5(3,7)=5(9,7)=5(9,6)=5(7,5)
// Removed X-Cycle candidate 5 at (2,9) (remaining 55 candidates)
// X-cycle (length 7) : 8(3,2)=8(7,2)-8(7,3)-8(8,3)=8(8,9)-8(9,7)=8(3,7)-8(3,2)
// Removed X-Cycle candidate 8 at (7,3) (remaining 54 candidates)
// X-cycle (length 5) : 8(3,2)=8(7,2)-8(7,9)-8(9,7)=8(3,7)-8(3,2)
// Removed X-Cycle candidate 8 at (7,9) (remaining 53 candidates)
// X-cycle (length 5) : 8(3,2)=8(7,2)-8(8,3)=8(8,9)-8(3,9)-8(3,2)
// Removed X-Cycle candidate 8 at (3,9) (remaining 52 candidates)
// X-cycle (length 7) : 8(3,2)=8(7,2)-8(8,3)=8(8,9)-8(9,7)=8(3,7)-8(3,3)-8(3,2)
// Removed X-Cycle candidate 8 at (3,3) (remaining 51 candidates)
// X-cycle (length 5) : 8(1,6)=8(1,9)-8(3,7)=8(9,7)-8(9,6)-8(1,6)
// Removed X-Cycle candidate 8 at (9,6) (remaining 50 candidates)
X = sudoku_readgivens(".241..67..6..7.41.7..964.2.2465913871354872968796231544....976.35.71694.697.4..31");
[C,L] = sudoku_candidates(X);
[ Y , C , L ] = sudoku_findxcycle ( X , C , L , %f , %f );
assert_checkequal ( find(C(7,4,:)) , [3 8] );
assert_checkequal ( find(C(1,9,:)) , [3 8 9] );
assert_checkequal ( find(C(2,9,:)) , [3 8 9] );
assert_checkequal ( find(C(7,3,:)) , [1 2] );
assert_checkequal ( find(C(7,9,:)) , [2 5] );
assert_checkequal ( find(C(3,9,:)) , [3 5] );
assert_checkequal ( find(C(3,3,:)) , [1 3] );
assert_checkequal ( find(C(9,6,:)) , [2 5] );
assert_checkequal ( Y , X );
assert_checkequal ( sum(L) , 50 );
////////////////////////////////////////
// Discontinuous X-Cycle with strong discontinuity.
X = sudoku_readgivens("804537000023614085605982034000105870500708306080203450200859003050371208008426507");
[C,L] = sudoku_candidates(X);
[ Y , C , L ] = sudoku_findxcycle ( X , C , L , %f , %f );
// X-cycle strongly discontinuous (length 7) : 1(9,1)=1(7,3)-1(7,7)=1(7,8)=1(5,8)=1(5,3)-1(6,1)=1(9,1)
// Confirmed (7,8) as 1 (remains 30 unknowns)
// X-cycle weakly discontinuous (length 7) : 1(9,1)=1(7,3)-1(7,8)=1(5,8)=1(5,3)-1(6,3)-1(6,1)=1(9,1)
// Removed X-Cycle candidate 1 at (6,3) (remaining 69 candidates)
[ Y , C , L ] = sudoku_findxcycle ( X , C , L , %f , %f );
E = [
8 0 4   5 3 7   0 0 0 
0 2 3   6 1 4   0 8 5 
6 0 5   9 8 2   0 3 4 
..
0 0 0   1 0 5   8 7 0 
5 0 0   7 0 8   3 0 6 
0 8 0   2 0 3   4 5 0 
..
2 0 0   8 5 9   0 1 3 
0 5 0   3 7 1   2 0 8 
0 0 8   4 2 6   5 0 7 
];
assert_checkequal ( Y , E );
assert_checkequal ( sum(L) , 69 );
////////////////////////////////////////
// Continuous X-Cycle with candidate 8 length 6
//
// X-cycle weakly discontinuous (length 5) : 2(9,4)=2(9,6)=2(2,6)=2(2,4)-2(7,4)-2(9,4)
// Removed X-Cycle candidate 2 at (7,4) (remaining 57 candidates)
// X-cycle weakly discontinuous (length 7) : 5(1,5)=5(7,5)=5(7,9)=5(9,7)=5(3,7)=5(3,9)-5(1,9)-5(1,5)
// Removed X-Cycle candidate 5 at (1,9) (remaining 56 candidates)
// X-cycle weakly discontinuous (length 7) : 5(7,5)=5(7,9)-5(2,9)-5(3,9)=5(3,7)=5(9,7)=5(9,6)=5(7,5)
// Removed X-Cycle candidate 5 at (2,9) (remaining 55 candidates)
// X-cycle continuous (length 6) : 8(3,2)=8(7,2)-8(8,3)=8(8,9)-8(9,7)=8(3,7)-8(3,2)
// Removed X-Cycle candidate 8 at (7,3) (remaining 54 candidates)
// Removed X-Cycle candidate 8 at (7,9) (remaining 53 candidates)
// Removed X-Cycle candidate 8 at (3,3) (remaining 52 candidates)
// Removed X-Cycle candidate 8 at (3,9) (remaining 51 candidates)
// X-cycle weakly discontinuous (length 5) : 8(1,6)=8(1,9)-8(3,7)=8(9,7)-8(9,6)-8(1,6)
// Removed X-Cycle candidate 8 at (9,6) (remaining 50 candidates)
X = sudoku_readgivens("024100670060070410700964020246591387135487296879623154400009760350716940697040031");
[C,L] = sudoku_candidates(X);
[ Y , C , L ] = sudoku_findxcycle ( X , C , L , %f , %f );
assert_checkequal ( sum(L) , 50 );
assert_checkequal ( Y,X);
assert_checkequal ( find(C(7,4,:)) , [3 8] );
assert_checkequal ( find(C(1,9,:)) , [3 8 9] );
assert_checkequal ( find(C(2,9,:)) , [3 8 9] );
assert_checkequal ( find(C(7,3,:)) , [1 2] );
assert_checkequal ( find(C(7,9,:)) , [2 5] );
assert_checkequal ( find(C(3,3,:)) , [1 3] );
assert_checkequal ( find(C(3,9,:)) , [3 5] );
assert_checkequal ( find(C(9,6,:)) , [2 5] );
