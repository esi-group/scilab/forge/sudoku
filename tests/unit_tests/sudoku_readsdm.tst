// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


path = sudoku_getpath();
filename = fullfile(path,"tests","unit_tests","mycollection.sdm");

X = sudoku_readsdm (filename,3);
XE = [
9 0 0   7 0 3   0 0 4 
6 0 0   0 9 0   2 0 0 
0 0 7   0 6 0   0 0 0 
..
0 8 0   0 0 0   5 0 0 
0 0 6   0 4 0   7 0 0 
0 0 9   0 0 0   0 8 0 
..
0 0 0   0 5 0   8 0 0 
0 0 1   0 2 0   0 0 3 
2 0 0   9 0 1   0 0 5 
];
assert_checkequal ( X , XE );


