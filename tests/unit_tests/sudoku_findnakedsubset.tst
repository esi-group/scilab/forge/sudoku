// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->



function candidates = candcell ( C , i , j )
  candidates = find(C(i,j,:))
endfunction

// Test naked subset on row, column and block
X = [
3 2 0   9 0 1   4 0 0 
9 0 0   4 0 2   0 0 3 
0 0 6   3 7 8   2 0 9 
..
8 0 1   2 0 5   0 0 0 
0 0 0   1 0 6   0 0 0 
0 0 0   7 0 9   1 0 8 
..
1 0 0   6 9 3   5 0 0 
2 0 0   8 1 4   0 0 7 
6 0 4   5 2 7   0 3 1 
];

[C,L] = sudoku_candidates ( X );
// sudoku_findnakedsubset ( X , C , L , 2 , %f , %t )
// Found naked 2-set {5 6} in row 1 at columns (5 9)
// Removed subset row candidate 5 at (1,3) (remaining 116 candidates)
// Removed subset row candidate 5 at (1,8) (remaining 115 candidates)
// Removed subset row candidate 6 at (1,8) (remaining 114 candidates)
// Found naked 2-set {7 8} in row 7 at columns (2 3)
// Removed subset row candidate 8 at (7,8) (remaining 113 candidates)
// Found naked 2-set {6 9} in row 8 at columns (7 8)
// Removed subset row candidate 9 at (8,2) (remaining 112 candidates)
// Removed subset row candidate 9 at (8,3) (remaining 111 candidates)
// Found naked 2-set {4 5} in column 1 at rows (3 6)
// Removed subset column candidate 4 at (5,1) (remaining 110 candidates)
// Removed subset column candidate 5 at (5,1) (remaining 109 candidates)
// Found naked 2-set {7 8} in column 3 at rows (1 7)
// Removed subset column candidate 7 at (2,3) (remaining 108 candidates)
// Removed subset column candidate 8 at (2,3) (remaining 107 candidates)
// Removed subset column candidate 7 at (5,3) (remaining 106 candidates)
// Found naked 2-set {3 4} in column 5 at rows (4 6)
// Removed subset column candidate 3 at (5,5) (remaining 105 candidates)
// Removed subset column candidate 4 at (5,5) (remaining 104 candidates)
// Found naked 2-set {4 5} in block (1,1) at rows 2 3 and cols 3 1 
// Removed subset block candidate 5 at (2,2) (remaining 103 candidates)
// Removed subset block candidate 4 at (3,2) (remaining 102 candidates)
// Removed subset block candidate 5 at (3,2) (remaining 101 candidates)
// Found naked 2-set {7 8} in block (7,1) at rows 7 7 and cols 2 3 
// Removed subset block candidate 8 at (9,2) (remaining 100 candidates)
// Found naked 2-set {6 9} in block (7,7) at rows 8 8 and cols 7 8 
// Removed subset block candidate 9 at (9,7) (remaining 99 candidates)
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 2 , %f );
LE = [
0 0 2   0 2 0   0 2 2 
0 3 1   0 2 0   3 5 0 
2 1 0   0 0 0   0 2 0 
..
0 5 0   0 2 0   4 4 2 
1 5 4   0 1 0   3 5 3 
2 4 3   0 2 0   0 4 0 
..
0 2 2   0 0 0   0 2 2 
0 2 2   0 0 0   2 2 0 
0 1 0   0 0 0   1 0 0 
];
assert_checkequal ( L , LE );
assert_checkequal ( candcell(C,7,8) , [2 4] );
assert_checkequal ( candcell(C,5,5) , [8] );
assert_checkequal ( candcell(C,9,2) , [9] );
assert_checkequal ( candcell(C,9,7) , [8] );
/////////////////////////////////////////////////////////////
// Test stop at first
X = [
3 2 0   9 0 1   4 0 0 
9 0 0   4 0 2   0 0 3 
0 0 6   3 7 8   2 0 9 
..
8 0 1   2 0 5   0 0 0 
0 0 0   1 0 6   0 0 0 
0 0 0   7 0 9   1 0 8 
..
1 0 0   6 9 3   5 0 0 
2 0 0   8 1 4   0 0 7 
6 0 4   5 2 7   0 3 1 
];

[C,L] = sudoku_candidates ( X );
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 2 , %t );
assert_checkequal ( sum(L) , 114 );
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 2 , %t );
assert_checkequal ( sum(L) , 113 );
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 2 , %t );
assert_checkequal ( sum(L) , 111 );
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 2 , %t );
assert_checkequal ( sum(L) , 109 );

/////////////////////////////////////////////////////////////////
// Find naked triples

////////////////////////////////
// Found triple {1,7,8} in row 1 at (1,2), (1,3), (1,6)
// Removed triple row candidate 7 at (1,7)
// Removed triple row candidate 7 at (1,8)
// Found triple {1,3,7} in column 6 at (1,6), (4,6), (5,6)
// Removed triple column candidate 7 at (3,6)
// Removed triple column candidate 1 at (9,6)

// http://www.palmsudoku.com/pages/techniques-6.php
X = [
4 0 0   3 9 0   0 0 2
2 6 0   0 5 8   3 9 0
5 9 3   6 0 0   1 8 0
..
1 0 0   8 6 0   0 0 9
6 0 5   9 0 0   2 0 0 
0 3 9   2 4 5   0 1 6
..
0 5 6   0 0 9   0 2 0
0 1 4   7 0 0   9 0 5
9 0 0   5 3 0   0 0 0
];
[C,L] = sudoku_candidates(X);
[Y,C,L] = sudoku_findnakedsubset ( X , C , L , 3 , %f );
assert_checkequal ( candcell(C,3,6) , [2 4] );
assert_checkequal ( L(3,6) , 2 );
assert_checkequal ( candcell(C,9,6) , [2 4 6] );
assert_checkequal ( L(9,6) , 3 );
assert_checkequal ( candcell(C,1,7) , [5 6] );
assert_checkequal ( L(1,7) , 2 );
assert_checkequal ( candcell(C,1,8) , [5 6] );
assert_checkequal ( L(1,8) , 2 );
assert_checkequal ( Y , X );
////////////////////////////////
// Find the triple {1,5,8} in block (1,1) 
// Removed triple candidate 1 at (3,2)
// Removed triple candidate 5 at (3,2)
// Removed triple candidate 1 at (3,3)
// Removed triple candidate 8 at (3,3)
// http://www.palmsudoku.com/pages/techniques-6.php
X = [
4 0 6   0 5 1   2 0 0
0 3 0   0 2 0   0 0 9
0 0 0   0 0 0   0 0 0
..
0 6 4   2 0 5   7 9 8
7 0 0   0 0 0   5 2 4
2 8 5   7 4 9   6 3 1
..
0 0 0   0 0 0   9 0 0
9 0 0   0 6 0   0 4 0
0 4 7   5 9 0   1 0 2
];
[C,L] = sudoku_candidates(X);
[Y,C,L] = sudoku_findnakedsubset ( X , C , L , 3 , %f );
assert_checkequal ( candcell(C,3,2) , [2 7 9] );
assert_checkequal ( L(3,2) , 3 );
assert_checkequal ( candcell(C,3,3) , [2 9] );
assert_checkequal ( L(3,3) , 2 );
LE = [
0 2 0   3 0 0   0 2 2 
3 0 2   3 0 4   2 5 0 
3 3 2   5 3 5   3 5 4 
..
2 0 0   0 2 0   0 0 0 
0 2 3   4 3 3   0 0 0 
0 0 0   0 0 0   0 0 0 
..
5 3 4   4 4 5   0 4 4 
0 3 4   3 0 4   2 0 3 
3 0 0   0 0 2   0 2 0 
];
assert_checkequal ( L , LE );
assert_checkequal ( Y , X );
////////////////////////////////
// Test stopatfirst
X = [
4 0 0   3 9 0   0 0 2
2 6 0   0 5 8   3 9 0
5 9 3   6 0 0   1 8 0
..
1 0 0   8 6 0   0 0 9
6 0 5   9 0 0   2 0 0 
0 3 9   2 4 5   0 1 6
..
0 5 6   0 0 9   0 2 0
0 1 4   7 0 0   9 0 5
9 0 0   5 3 0   0 0 0
];
[C,L] = sudoku_candidates(X);
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 3 , %t );
assert_checkequal ( sum(L) , 101 );
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 3 , %t );
assert_checkequal ( sum(L) , 99 );
////////////////////////////////////////////////////////////////////////
// Found naked 2-set {7 8} in row 7 at columns (2 3)
// Removed subset row candidate 8 at (7,8) (remaining 116 candidates)
// Found naked 2-set {3 4} in column 5 at rows (4 6)
// Removed subset column candidate 3 at (5,5) (remaining 115 candidates)
// Removed subset column candidate 4 at (5,5) (remaining 114 candidates)
// Found naked 2-set {7 8} in block (7,1) at rows 7 7 and cols 2 3 
// Removed subset block candidate 8 at (9,2) (remaining 113 candidates)
// Found naked 2-set {6 9} in block (7,7) at rows 8 8 and cols 7 8 
// Removed subset block candidate 9 at (9,7) (remaining 112 candidates)

X = [
3 2 0   9 0 1   4 0 0 
9 0 0   4 0 2   0 0 3 
0 0 6   3 7 8   2 0 9 
..
8 0 1   2 0 5   0 0 0 
0 0 0   1 0 6   0 0 0 
0 0 0   7 0 9   1 0 8 
..
1 0 0   6 9 3   5 0 0 
2 0 0   8 1 4   0 0 7 
6 0 4   5 2 7   0 3 1 
];

[C,L] = sudoku_candidates ( X );
[Y,C,L] = sudoku_findnakedsubset ( X , C , L , 2 , %f );
LE = [
0 0 2   0 2 0   0 2 2 
0 3 1   0 2 0   3 5 0 
2 1 0   0 0 0   0 2 0 
..
0 5 0   0 2 0   4 4 2 
1 5 4   0 1 0   3 5 3 
2 4 3   0 2 0   0 4 0 
..
0 2 2   0 0 0   0 2 2 
0 2 2   0 0 0   2 2 0 
0 1 0   0 0 0   1 0 0 
];
assert_checkequal ( L , LE );
assert_checkequal ( candcell(C,7,8) , [2 4] );
assert_checkequal ( candcell(C,5,5) , [8] );
assert_checkequal ( candcell(C,9,2) , [9] );
assert_checkequal ( candcell(C,9,7) , [8] );
assert_checkequal ( Y , X );

////////////////////////////////////////////////////////////////////////
// Test naked pairs with stop at first.
X = [
3 2 0   9 0 1   4 0 0 
9 0 0   4 0 2   0 0 3 
0 0 6   3 7 8   2 0 9 
..
8 0 1   2 0 5   0 0 0 
0 0 0   1 0 6   0 0 0 
0 0 0   7 0 9   1 0 8 
..
1 0 0   6 9 3   5 0 0 
2 0 0   8 1 4   0 0 7 
6 0 4   5 2 7   0 3 1 
];

[C,L] = sudoku_candidates ( X );
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 2 , %t );
assert_checkequal ( sum(L) , 114 );
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 2 , %t );
assert_checkequal ( sum(L) , 113 );
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 2 , %t );
assert_checkequal ( sum(L) , 111 );
////////////////////////////////////////////////////////////////////////
// Test naked singles
X = [
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   5 3 0 
9 0 0   0 3 7   0 0 0 
..
0 0 2   3 0 1   0 9 6 
0 4 7   6 9 0   1 8 0 
0 0 6   7 8 5   0 0 0 
..
0 5 0   0 2 0   9 0 3 
0 3 0   9 0 0   6 0 8 
8 0 0   0 1 0   4 7 0 
];
[C,L] = sudoku_candidates(X);
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 1 , %f );
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 1 , %f );
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 1 , %f );
assert_checkequal ( sum(L) == 0 , %t );

////////////////////////////////////////
// "Programming Sudoku", Figure 4-8
// A case with no naked single.
X = [
5 8 3   0 2 0   0 9 0 
0 0 0   5 9 3   2 0 0 
4 9 2   0 0 0   0 0 0 
..
0 0 5   7 0 0   1 0 0 
0 6 7   0 0 0   5 3 0 
0 0 8   0 5 9   0 0 0
..
0 0 0   0 0 5   0 1 2 
0 0 9   2 0 6   0 0 0 
2 5 0   0 8 0   0 6 7 
];
[C,L] = sudoku_candidates(X);
[Y,C,L] = sudoku_findnakedsubset ( X , C , L , 1 , %f );
assert_checkequal ( and(X == Y) , %t );

////////////////////////////////////////

// TODO : find the conflict in this inconsistent sudoku
if ( %f ) then
X = [
1 2 0   0 3 0   0 4 0 
3 5 0   0 0 0   0 0 0 
4 6 0   0 0 0   0 0 0 
..
2 1 0   0 0 0   0 0 0 
5 3 0   0 0 0   0 0 0 
6 0 0   0 0 0   0 0 0 
..
7 0 0   0 0 0   0 0 0 
8 0 0   0 0 0   0 0 0 
9 0 0   0 0 0   0 0 0 
];
[C,L] = sudoku_candidates(X);
[Y,C,L] = sudoku_findnakedsingles ( X , C , L );
assert_checkequal ( sum(Y>0) > sum(X>0) , %t );
end

/////////////////////////////////////////////////////////
// Find naked 4-set in block
// Found naked 4-set {4 5 7 8} in block (7,7) at rows 7 8 8 8 and cols 7 7 8 9 
X = [
5 0 2   6 0 0   7 0 0 
0 0 0   9 0 0   6 1 2 
0 0 6   0 0 0   3 8 5 
..
0 0 4   0 9 6   1 0 0 
0 0 0   0 0 0   0 0 0 
0 0 5   2 7 0   9 0 0 
..
8 3 7   0 0 0   0 0 0 
2 6 1   0 0 9   0 0 0 
4 5 9   0 0 8   2 0 3 
];
[C,L] = sudoku_candidates(X);
// sum(L) before 158
[Y,C,L] = sudoku_findnakedsubset ( X , C , L , 4 , %f );
// sum(L) after 154
assert_checkequal ( sum(L) , 154 );
assert_checkequal ( and(X==Y) , %t);

