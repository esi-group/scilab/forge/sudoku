// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


////////////////////////////////////////

X = sudoku_latinsquare(4);
islatin = sudoku_islatin(X);
assert_checkequal ( islatin , %t );

////////////////////////////////////////
X = [
0 1 2
0 0 0
0 0 0
]
islatin = sudoku_islatin(X);
assert_checkequal ( islatin , %f );

////////////////////////////////////////
X = [
1 2 3
3 2 1
]
islatin = sudoku_islatin(X);
assert_checkequal ( islatin , %f );
////////////////////////////////////////
X = [
1 2 3
1 2 3
1 2 3
]
islatin = sudoku_islatin(X);
assert_checkequal ( islatin , %f );
////////////////////////////////////////
X = [
1 1 1
2 2 2
3 3 3
]
islatin = sudoku_islatin(X);
assert_checkequal ( islatin , %f );

