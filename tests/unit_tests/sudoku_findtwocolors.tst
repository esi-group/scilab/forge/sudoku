// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->



// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction


////////////////////////////////////////
// Find the colors : remove the candidates when 2 cells have the same color
// Removed candidate 2 at (8,1) in 2-colors chain (same color in row, column or block) (remaining 26 candidates)
// Removed candidate 2 at (2,3) in 2-colors chain (same color in row, column or block) (remaining 25 candidates)
// Removed candidate 2 at (3,4) in 2-colors chain (same color in row, column or block) (remaining 24 candidates)
// Removed candidate 2 at (7,4) in 2-colors chain (same color in row, column or block) (remaining 23 candidates)
// Removed candidate 2 at (8,6) in 2-colors chain (same color in row, column or block) (remaining 22 candidates)
// Removed candidate 8 at (8,1) after 2-colors chain (visibility by oppositely colored cells) (remaining 21 candidates)
// Removed candidate 8 at (9,4) after 2-colors chain (visibility by oppositely colored cells) (remaining 20 candidates)
X = sudoku_readgivens("19645372843.76.951.57.1946336598714298412563772163489567..41589.49.7.316.1..96274");
[C,L] = sudoku_candidates(X);
[ Y , C , L ] = sudoku_findtwocolors ( X , C , L , %f , %f );
assert_checkequal ( find(C(2,3,:)) , 8 );
assert_checkequal ( find(C(3,4,:)) , 8 );
assert_checkequal ( find(C(8,1,:)) , [5] );
assert_checkequal ( find(C(7,4,:)) , 3 );
assert_checkequal ( find(C(8,6,:)) , 8 );
assert_checkequal ( Y , X );
assert_checkequal ( sum(L) , 20 );

////////////////////////////////////////
// Find the colors : remove the uncolored cell which is visible by two oppositely colored cells
// Remove the 5 as candidates in (5,2)
// Remove the 7 as candidates in (8,5)
X = sudoku_readgivens("1.9...7...27...5..645387921.78...69...16.92.8962...14...64253.92.3...46...4...8.2");
[C,L] = sudoku_candidates(X);
[X,C,L] = sudoku_findlocked ( X , C , L , %f , %f );
[X,C,L] = sudoku_findnakedsubset ( X , C , L , 2 , %f , %f );
[X,C,L] = sudoku_findhiddensubset ( X , C , L , 2 , %f , %f );
[Y,C,L] = sudoku_findtwocolors ( X , C , L , %f , %f );
assert_checkequal ( find(C(5,2,:)) , 3 );
assert_checkequal ( find(C(8,5,:)) , [1 9] );
assert_checkequal ( Y , X );
assert_checkequal ( sum(L) , 100 );

