// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->



// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction

function candidates = candcell ( C , i , j )
  candidates = find(C(i,j,:))
endfunction

////////////////////////////////////////////////////////////////////
// Find Hidden pair in row/block
X = [
0 6 0   3 9 0   1 0 0 
0 0 3   1 5 0   0 9 0 
1 9 0   4 2 6   3 0 0 
..
8 0 0   5 0 9   4 1 0 
9 0 0   0 6 1   0 0 0 
0 5 1   0 4 3   0 0 9 
..
4 1 9   6 3 5   0 2 7 
0 2 0   9 0 4   5 0 1 
0 0 0   0 1 2   9 4 0 
];
[C,L] = sudoku_candidates(X);
[Y,C,L] = sudoku_findhiddensubset ( X , C , L , 2 , %f );
// Found hidden pair {2,6} in row 4 at (4,3), (4,9)
// Removed other candidates 7 in hidden pair at (4,3) (remaining 109 candidates)
// Removed other candidates 3 in hidden pair at (4,9) (remaining 108 candidates)
// Found hidden pair {2,8} in block (4,4) at (5,4), (6,4)
// Removed other candidates 7 in hidden pair at (5,4) (remaining 107 candidates)
// Removed other candidates 7 in hidden pair at (6,4) (remaining 106 candidates)
// Found hidden pair {3,5} in block (4,7) at (5,8), (5,9)
// Removed other candidates 7 in hidden pair at (5,8)
// Removed other candidates 8 in hidden pair at (5,8)
// Removed other candidates 2 in hidden pair at (5,9)
// Removed other candidates 8 in hidden pair at (5,9)
// Found hidden pair {3,6} in block (7,7) at (8,8), (9,9)
// Removed other candidates 8 in hidden pair at (8,8) (remaining 101 candidates)
// Removed other candidates 8 in hidden pair at (9,9) (remaining 100 candidates)
//
assert_checkequal ( candcell(C,4,3) , [2 6] );
assert_checkequal ( L(4,3) , 2 );
assert_checkequal ( candcell(C,4,9) , [2 6] );
assert_checkequal ( L(4,9) , 2 );
//
assert_checkequal ( candcell(C,5,4) , [2 8] );
assert_checkequal ( L(5,4) , 2 );
assert_checkequal ( candcell(C,6,4) , [2 8] );
assert_checkequal ( L(6,4) , 2 );
//
assert_checkequal ( candcell(C,5,8) , [3 5] );
assert_checkequal ( L(5,8) , 2 );
assert_checkequal ( candcell(C,5,9) , [3 5] );
assert_checkequal ( L(5,9) , 2 );
//
assert_checkequal ( candcell(C,8,8) , [3 6] );
assert_checkequal ( L(8,8) , 2 );
assert_checkequal ( candcell(C,9,9) , [3 6] );
assert_checkequal ( L(9,9) , 2 );
//
assert_checkequal ( Y , X );
LE=  [
3 0 5   0 0 2   0 3 4 
2 3 0   0 0 2   4 0 4 
0 0 3   0 0 0   0 3 2 
..
0 2 2   0 1 0   0 0 2 
0 3 3   2 0 0   3 2 2 
3 0 0   2 0 0   4 3 0 
..
0 0 0   0 0 0   1 0 0 
3 0 3   0 2 0   0 2 0 
4 3 4   2 0 0   0 0 2 
];
assert_checkequal ( LE , L );


////////////////////////////////////////////////////////////////////
// Find Hidden pair in block (and also all hidden singles...)
X = [
0 6 0   0 9 0   0 0 0 
0 0 3   1 0 0   0 9 0 
1 9 0   0 2 6   3 0 0 
..
8 0 0   5 0 0   4 1 0 
0 0 0   0 6 0   0 0 0 
0 5 1   0 0 3   0 0 9 
..
0 0 9   6 3 0   0 2 7 
0 2 0   0 0 4   5 0 0 
0 0 0   0 1 0   0 4 0 
];
[C,L] = sudoku_candidates(X);
[Y,C,L] = sudoku_findhiddensubset ( X , C , L , 2 , %f );
LE=  [
4 0 5   2 0 2   4 3 5 
4 3 0   0 1 2   4 0 5 
0 0 4   1 0 0   0 3 3 
..
0 2 3   0 1 3   0 0 3 
5 3 3   5 0 5   3 4 4 
4 0 0   4 1 0   4 3 0 
..
1 2 0   0 0 1   2 0 0 
3 0 3   1 1 0   0 3 1 
4 3 4   2 0 2   3 0 3 
];
assert_checkequal ( LE , L );
assert_checkequal ( Y , X );


////////////////////////////////////////////////////////////////////
// Find Hidden pair in row and column
X = [
8 0 1  0 0 6  0 9 4
3 0 0  0 0 9  0 8 0
9 7 0  0 8 0  5 0 0
..
5 4 7  0 6 2  0 3 0
6 3 2  0 0 0  0 5 0
1 9 8  3 7 5  2 4 6
..
0 8 3  6 2 0  9 1 5
0 6 5  1 9 8  0 0 0
2 1 9  5 0 0  0 0 8
];
[C,L] = sudoku_candidates(X);
[Y,C,L] = sudoku_findhiddensubset ( X , C , L , 2 , %f );
// Found hidden pair {1,3} in row 3 at (3,6), (3,9)
// Removed other candidates 4 in hidden pair at (3,6) (remaining 79 candidates)
// Removed other candidates 2 in hidden pair at (3,9) (remaining 78 candidates)
// Found hidden pair {8,9} in column 4 at (4,4), (5,4)
// Removed other candidates 4 in hidden pair at (5,4) (remaining 77 candidates)
LE=  [
0 2 0   2 2 0   2 0 0 
0 2 2   3 3 0   3 0 3 
0 0 2   2 0 2   0 2 2 
..
0 0 0   2 0 0   2 0 2 
0 0 0   2 2 2   3 0 3 
0 0 0   0 0 0   0 0 0 
..
2 0 0   0 0 2   0 0 0 
2 0 0   0 0 0   3 2 3 
0 0 0   0 2 3   4 2 0 
];
assert_checkequal ( LE , L );
assert_checkequal ( Y , X );
assert_checkequal ( candcell(C,3,6) , [1 3] );
assert_checkequal ( L(3,6) , 2 );
assert_checkequal ( candcell(C,3,9) , [1 3] );
assert_checkequal ( L(3,9) , 2 );

////////////////////////////////////////////////////////////////////
// Test stopatfirst
X = [
8 0 1  0 0 6  0 9 4
3 0 0  0 0 9  0 8 0
9 7 0  0 8 0  5 0 0
..
5 4 7  0 6 2  0 3 0
6 3 2  0 0 0  0 5 0
1 9 8  3 7 5  2 4 6
..
0 8 3  6 2 0  9 1 5
0 6 5  1 9 8  0 0 0
2 1 9  5 0 0  0 0 8
];
[C,L] = sudoku_candidates(X);
// Found hidden pair {1,3} in row 3 at (3,6), (3,9)
// Removed other candidates 4 in hidden pair at (3,6) (remaining 79 candidates)
// Removed other candidates 2 in hidden pair at (3,9) (remaining 78 candidates)
[X,C,L] = sudoku_findhiddensubset ( X , C , L , 2 , %t );
assert_checkequal ( sum(L) , 78 );
// Found hidden pair {8,9} in column 4 at (4,4), (5,4)
// Removed other candidates 4 in hidden pair at (5,4) (remaining 77 candidates)
[X,C,L] = sudoku_findhiddensubset ( X , C , L , 2 , %t );
assert_checkequal ( sum(L) , 77 );

/////////////////////////////////////////////////////////////////////
// Check hidden singles
X = [
0 0 0   0 0 0   0 0 0
0 0 0   0 0 0   5 3 0
9 0 0   0 3 7   0 0 0
..
0 0 2   3 0 1   0 9 6
0 4 7   6 9 0   1 8 0
0 0 6   7 8 5   0 0 0
..
0 5 0   0 2 0   9 0 3
0 3 0   9 0 0   6 0 8
8 0 0   0 1 0   4 7 0
];
[C,L] = sudoku_candidates(X);
Y = sudoku_findhiddensubset ( X , C , L , 1 , %f )
YE = [
6 0 3   0 5 8   0 0 9 
0 0 8   0 6 9   5 3 7 
9 0 5   0 3 7   8 6 0 
..
5 8 2   3 4 1   7 9 6 
3 4 7   6 9 0   1 8 5 
0 9 6   7 8 5   3 0 0 
..
7 5 0   8 2 6   9 1 3 
2 3 0   9 7 0   6 5 8 
8 6 9   5 1 3   4 7 2 
];
assert_checkequal ( Y , YE );

// "Sudoku Programming", Chapter 4 , Figure 4-8
// A hidden single in a row.

X = [
5 8 3   0 2 0   0 9 0 
0 0 0   5 9 3   2 0 0 
4 9 2   0 0 0   0 0 0 
..
0 0 5   7 0 0   1 0 0 
0 6 7   0 0 0   5 3 0 
0 0 8   0 5 9   0 0 0 
..
0 0 0   0 0 5   0 1 2 
0 0 9   2 0 6   0 0 0 
2 5 0   0 8 0   0 6 7 
];
[C,L] = sudoku_candidates ( X );
Y = sudoku_findhiddensubset ( X , C , L , 1 , %f )
YE = [
5 8 3   0 2 0   0 9 0 
0 0 0   5 9 3   2 0 0 
4 9 2   0 0 0   0 0 0 
..
0 0 5   7 0 0   1 0 0 
0 6 7   0 0 2   5 3 0 
0 0 8   0 5 9   0 0 0 
..
0 0 0   0 0 5   0 1 2 
0 0 9   2 0 6   0 0 0 
2 5 0   0 8 0   0 6 7 
];
assert_checkequal ( Y , YE );

////////////////////////////////////////

// "Sudoku Programming", Chapter 4 , Figure 4-8
// A hidden single in a mini grid.
X = [
6 0 7   0 0 1   0 0 0 
0 8 0   7 0 0   0 6 9 
9 3 1   6 4 5   2 8 7
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
];
[C,L] = sudoku_candidates ( X );
Y = sudoku_findhiddensubset ( X , C , L , 1 , %f )
YE = [
6 0 7   0 0 1   0 0 0 
0 8 0   7 0 0   1 6 9 
9 3 1   6 4 5   2 8 7
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
];
assert_checkequal ( Y , YE );



////////////////////////////////////////
// A hidden single in a column
X = [
1 5 8   6 0 0   0 0 2
0 7 2   0 1 0   8 0 6
6 0 0   8 2 0   0 0 0
..
0 8 0   0 0 0   0 0 0
0 0 1   0 9 0   2 0 0
0 2 0   0 0 0   0 3 0
..
8 0 0   0 3 1   0 2 4
4 1 3   0 5 0   0 7 0
2 0 0   0 0 4   0 1 0
];
[C,L] = sudoku_candidates(X);
Y = sudoku_findhiddensubset ( X , C , L , 1 , %f )
YE = [
1 5 8   6 0 0   0 0 2
0 7 2   0 1 0   8 0 6
6 0 0   8 2 0   0 0 0
..
0 8 0   0 0 0   0 0 0
0 0 1   0 9 0   2 8 0
0 2 0   0 0 0   0 3 0
..
8 0 0   0 3 1   0 2 4
4 1 3   0 5 0   0 7 0
2 0 0   0 0 4   0 1 0
];
assert_checkequal ( Y , YE );


/////////////////////////////////////////////////////////
// Find naked 4-set in row
// Found hidden subset {2,6,7,9} in row 5 at columns 1 2 8 9
X = [
5 0 2   6 0 0   7 0 0 
0 0 0   9 0 0   6 1 2 
0 0 6   0 0 0   3 8 5 
..
0 0 4   0 9 6   1 0 0 
0 0 0   0 0 0   0 0 0 
0 0 5   2 7 0   9 0 0 
..
8 3 7   0 0 0   0 0 0 
2 6 1   0 0 9   0 0 0 
4 5 9   0 0 8   2 0 3 
];
[C,L] = sudoku_candidates(X);
// sum(L) before 158
[Y,C,L] = sudoku_findhiddensubset ( X , C , L , 4 , %f );
// sum(L) after 149
assert_checkequal ( sum(L) , 149 );
assert_checkequal ( and(X==Y) , %t);

/////////////////////////////////////////////////////////
// Find naked 4-set in column
// Found hidden subset {2,6,7,9} in column 5 at rows 1 2 8 9
X = [
5 0 2   6 0 0   7 0 0 
0 0 0   9 0 0   6 1 2 
0 0 6   0 0 0   3 8 5 
..
0 0 4   0 9 6   1 0 0 
0 0 0   0 0 0   0 0 0 
0 0 5   2 7 0   9 0 0 
..
8 3 7   0 0 0   0 0 0 
2 6 1   0 0 9   0 0 0 
4 5 9   0 0 8   2 0 3 
]';
[C,L] = sudoku_candidates(X);
// sum(L) before 158
[Y,C,L] = sudoku_findhiddensubset ( X , C , L , 4 , %f );
// sum(L) after 149
assert_checkequal ( sum(L) , 149 );
assert_checkequal ( and(X==Y) , %t);


