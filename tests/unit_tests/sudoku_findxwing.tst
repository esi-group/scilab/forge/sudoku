// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


function candidates = candcell ( C , i , j )
  candidates = find(C(i,j,:))
endfunction

////////////////////////////////////////
// http://fr.wikipedia.org/wiki/X-wing_(Sudoku)
X = [
6 7 9   4 1 0   3 5 2
2 4 8   0 9 5   7 0 1
1 5 3   7 6 2   9 8 4
..
0 0 0   0 0 0   4 2 0
7 0 0   0 8 0   5 3 6
0 0 0   0 0 0   1 7 0
..
5 8 7   1 2 9   6 4 3
4 6 1   8 0 7   2 0 5
3 0 2   0 5 4   8 1 7
];
[C,L] = sudoku_candidates(X);
[Y,C,L] = sudoku_findxwing ( X , C , L , %f );
// Found column X-Wing 3 at rows 4 6 and columns 2 6 
// Removed column X-Wing candidate 3 at (4,4) (remaining 50 candidates)
// Removed column X-Wing candidate 3 at (4,5) (remaining 49 candidates)
// Removed column X-Wing candidate 3 at (6,4) (remaining 48 candidates)
// Removed column X-Wing candidate 3 at (6,5) (remaining 47 candidates)
// Found column X-Wing 6 at rows 4 6 and columns 3 6 
// Removed column X-Wing candidate 6 at (4,4) (remaining 46 candidates)
// Removed column X-Wing candidate 6 at (6,4) (remaining 45 candidates)
// Found column X-Wing 9 at rows 4 6 and columns 1 9 
// Removed column X-Wing candidate 9 at (4,2) (remaining 44 candidates)
// Removed column X-Wing candidate 9 at (4,4) (remaining 43 candidates)
// Removed column X-Wing candidate 9 at (6,2) (remaining 42 candidates)
// Removed column X-Wing candidate 9 at (6,4) (remaining 41 candidates)

assert_checkequal ( candcell ( C , 4,2) , [1 3] );
assert_checkequal ( candcell ( C , 4,4) , 5 );
assert_checkequal ( candcell ( C , 4,5) , 7 );
assert_checkequal ( candcell ( C , 6,2) , [2 3] );
assert_checkequal ( candcell ( C , 6,4) , [2 5] );
LE = [
0 0 0   0 0 1   0 0 0 
0 0 0   1 0 0   0 1 0 
0 0 0   0 0 0   0 0 0 
..
2 2 2   1 1 3   0 0 2 
0 3 1   2 0 1   0 0 0 
2 2 3   2 1 2   0 0 2 
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 1 0   0 1 0 
0 1 0   1 0 0   0 0 0 
];
assert_checkequal ( L , LE );

////////////////////////////////////////
// http://fr.wikipedia.org/wiki/X-wing_(Sudoku) - transposed
X = [
6 7 9   4 1 0   3 5 2
2 4 8   0 9 5   7 0 1
1 5 3   7 6 2   9 8 4
..
0 0 0   0 0 0   4 2 0
7 0 0   0 8 0   5 3 6
0 0 0   0 0 0   1 7 0
..
5 8 7   1 2 9   6 4 3
4 6 1   8 0 7   2 0 5
3 0 2   0 5 4   8 1 7
]';
[C,L] = sudoku_candidates(X);
[Y,C,L] = sudoku_findxwing ( X , C , L , %f );
// Found row X-Wing 3 at rows 2 6 and columns 4 6 
// Removed row X-Wing candidate 3 at (4,4) (remaining 50 candidates)
// Removed row X-Wing candidate 3 at (5,4) (remaining 49 candidates)
// Removed row X-Wing candidate 3 at (4,6) (remaining 48 candidates)
// Removed row X-Wing candidate 3 at (5,6) (remaining 47 candidates)
// Found row X-Wing 6 at rows 3 6 and columns 4 6 
// Removed row X-Wing candidate 6 at (4,4) (remaining 46 candidates)
// Removed row X-Wing candidate 6 at (4,6) (remaining 45 candidates)
// Found row X-Wing 9 at rows 1 9 and columns 4 6 
// Removed row X-Wing candidate 9 at (2,4) (remaining 44 candidates)
// Removed row X-Wing candidate 9 at (4,4) (remaining 43 candidates)
// Removed row X-Wing candidate 9 at (2,6) (remaining 42 candidates)
// Removed row X-Wing candidate 9 at (4,6) (remaining 41 candidates)

assert_checkequal ( candcell ( C , 2,4) , [1 3] );
assert_checkequal ( candcell ( C , 4,4) , 5 );
assert_checkequal ( candcell ( C , 5,4) , 7 );
assert_checkequal ( candcell ( C , 2,6) , [2 3] );
assert_checkequal ( candcell ( C , 4,6) , [2 5] );
LE = [
0 0 0   0 0 1   0 0 0 
0 0 0   1 0 0   0 1 0 
0 0 0   0 0 0   0 0 0 
..
2 2 2   1 1 3   0 0 2 
0 3 1   2 0 1   0 0 0 
2 2 3   2 1 2   0 0 2 
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 1 0   0 1 0 
0 1 0   1 0 0   0 0 0 
];
assert_checkequal ( L' , LE );


