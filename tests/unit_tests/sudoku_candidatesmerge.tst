// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->




X = [
0 2 0   6 3 0   0 4 0 
6 0 0   0 0 0   0 0 3 
3 0 4   0 0 0   5 6 0 
..
0 0 0   8 0 6   0 0 0 
8 0 0   0 1 0   0 0 6 
0 6 0   7 0 5   0 0 0 
..
0 8 7   0 0 0   6 0 4 
4 0 0   0 6 7   0 0 8 
0 3 6   0 4 8   0 2 0 
];

[C,L] = sudoku_candidates(X);
// Merge candidates in row 2
carray = sudoku_candidatesmerge ( X , C , L , 1 , 2 , [] , [0 9] );
assert_checkequal ( carray , [1 2 4 5 7 8 9] );
// Merge candidates in column 1
carray = sudoku_candidatesmerge ( X , C , L , 2 , [] , 1 , [0 9] );
assert_checkequal ( carray , [1 2 5 7 9] );
// Merge candidates in column 1, consider only cells with length <= 3
carray = sudoku_candidatesmerge ( X , C , L , 2 , [] , 1 , [0 3] );
assert_checkequal ( carray , [1 2 5 9] );
// Merge candidates in block 7,7
carray = sudoku_candidatesmerge ( X , C , L , 3 , 7 , 7 , [0 9] );
assert_checkequal ( carray , [1 3 5 7 9] );
// Merge candidates in block 7,7
carray = sudoku_candidatesmerge ( X , C , L , 3 , 7 , 7 , [0 9] );
assert_checkequal ( carray , [1 3 5 7 9] );
// Merge candidates in block 7,7, select cells of length 9
carray = sudoku_candidatesmerge ( X , C , L , 3 , 7 , 7 , [9 9] );
assert_checkequal ( carray , [] );

