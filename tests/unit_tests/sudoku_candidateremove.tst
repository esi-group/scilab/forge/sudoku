// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->



function candidates = candcell ( C , i , j )
  candidates = find(C(i,j,:))
endfunction

X = [
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   5 3 0 
9 0 0   0 3 7   0 0 0 
..
0 0 2   3 0 1   0 9 6 
0 4 7   6 9 0   1 8 0 
0 0 6   7 8 5   0 0 0 
..
0 5 0   0 2 0   9 0 3 
0 3 0   9 0 0   6 0 8 
8 0 0   0 1 0   4 7 0 
];

// Remove v = 2 from row 1
[C,L] = sudoku_candidates ( X );
[C,L] = sudoku_candidateremove ( C , L , 1 , 1 , [] , 2 );
assert_checkequal ( candcell ( C , 1,1) , [1 3 4 5 6 7] ) ;
assert_checkequal ( candcell ( C , 1,2) , [1 6 7 8] ) ;
assert_checkequal ( candcell ( C , 1,3) , [1 3 4 5 8] ) ;
assert_checkequal ( candcell ( C , 1,4) , [1 4 5 8] ) ;
assert_checkequal ( candcell ( C , 1,5) , [4 5 6] ) ;
assert_checkequal ( candcell ( C , 1,6) , [4 6 8 9] ) ;
assert_checkequal ( candcell ( C , 1,7) , [7 8] ) ;
assert_checkequal ( candcell ( C , 1,8) , [1 4 6] ) ;
assert_checkequal ( candcell ( C , 1,9) , [1 4 7 9] ) ;
assert_checkequal ( candcell ( C , 2,1) , [1 2 4 6 7] ) ;
assert_checkequal ( candcell ( C , 2,2) , [1 2 6 7 8] ) ;
assert_checkequal ( candcell ( C , 2,3) , [1 4 8] ) ;
assert_checkequal ( candcell ( C , 2,4) , [1 2 4 8] ) ;
assert_checkequal ( candcell ( C , 2,5) , [4 6] ) ;
assert_checkequal ( candcell ( C , 2,6) , [2 4 6 8 9] ) ;
assert_checkequal ( candcell ( C , 2,9) , [1 2 4 7 9] ) ;
assert_checkequal ( candcell ( C , 3,2) , [1 2 6 8] ) ;
assert_checkequal ( candcell ( C , 3,3) , [1 4 5 8] ) ;
assert_checkequal ( candcell ( C , 3,4) , [1 2 4 5 8] ) ;
assert_checkequal ( candcell ( C , 3,7) , [2 8] ) ;
assert_checkequal ( candcell ( C , 3,8) , [1 2 4 6] ) ;
assert_checkequal ( candcell ( C , 3,9) , [1 2 4] ) ;
assert_checkequal ( candcell ( C , 4,1) , [5] ) ;
assert_checkequal ( candcell ( C , 4,2) , [8] ) ;
assert_checkequal ( candcell ( C , 4,5) , [4] ) ;
assert_checkequal ( candcell ( C , 4,7) , [7] ) ;
assert_checkequal ( candcell ( C , 5,1) , [3 5] ) ;
assert_checkequal ( candcell ( C , 5,6) , [2] ) ;
assert_checkequal ( candcell ( C , 5,9) , [2 5] ) ;
assert_checkequal ( candcell ( C , 6,1) , [1 3] ) ;
assert_checkequal ( candcell ( C , 6,2) , [1 9] ) ;
assert_checkequal ( candcell ( C , 6,7) , [2 3] ) ;
assert_checkequal ( candcell ( C , 6,8) , [2 4] ) ;
assert_checkequal ( candcell ( C , 6,9) , [2 4] ) ;
assert_checkequal ( candcell ( C , 7,1) , [1 4 6 7] ) ;
assert_checkequal ( candcell ( C , 7,3) , [1 4] ) ;
assert_checkequal ( candcell ( C , 7,4) , [4 8] ) ;
assert_checkequal ( candcell ( C , 7,6) , [4 6 8] ) ;
assert_checkequal ( candcell ( C , 7,8) , [1] ) ;
assert_checkequal ( candcell ( C , 8,1) , [1 2 4 7] ) ;
assert_checkequal ( candcell ( C , 8,3) , [1 4] ) ;
assert_checkequal ( candcell ( C , 8,5) , [4 5 7] ) ;
assert_checkequal ( candcell ( C , 8,6) , [4] ) ;
assert_checkequal ( candcell ( C , 8,8) , [1 2 5] ) ;
assert_checkequal ( candcell ( C , 9,2) , [2 6 9] ) ;
assert_checkequal ( candcell ( C , 9,3) , [9] ) ;
assert_checkequal ( candcell ( C , 9,4) , [5] ) ;
assert_checkequal ( candcell ( C , 9,6) , [3 6] ) ;
assert_checkequal ( candcell ( C , 9,9) , [2 5] ) ;
LE = [
6 4 5   4 3 4   2 3 4 
5 5 3   4 2 5   0 0 5 
0 4 4   5 0 0   2 4 3 
..
1 1 0   0 1 0   1 0 0 
2 0 0   0 0 1   0 0 2 
2 2 0   0 0 0   2 2 2 
..
4 0 2   2 0 3   0 1 0 
4 0 2   0 3 1   0 3 0 
0 3 1   1 0 2   0 0 2 
];
assert_checkequal ( L , LE );


// Remove v = 2 from row 1
[C,L] = sudoku_candidates ( X );
[C,L] = sudoku_candidateremove ( C , L , 2 , [] , 1 , 5 );
assert_checkequal ( candcell ( C , 1,1) , [1 2 3 4 6 7] ) ;
assert_checkequal ( candcell ( C , 1,2) , [1 2 6 7 8] ) ;
assert_checkequal ( candcell ( C , 1,3) , [1 3 4 5 8] ) ;
assert_checkequal ( candcell ( C , 1,4) , [1 2 4 5 8] ) ;
assert_checkequal ( candcell ( C , 1,5) , [4 5 6] ) ;
assert_checkequal ( candcell ( C , 1,6) , [2 4 6 8 9] ) ;
assert_checkequal ( candcell ( C , 1,7) , [2 7 8] ) ;
assert_checkequal ( candcell ( C , 1,8) , [1 2 4 6] ) ;
assert_checkequal ( candcell ( C , 1,9) , [1 2 4 7 9] ) ;
assert_checkequal ( candcell ( C , 2,1) , [1 2 4 6 7] ) ;
assert_checkequal ( candcell ( C , 2,2) , [1 2 6 7 8] ) ;
assert_checkequal ( candcell ( C , 2,3) , [1 4 8] ) ;
assert_checkequal ( candcell ( C , 2,4) , [1 2 4 8] ) ;
assert_checkequal ( candcell ( C , 2,5) , [4 6] ) ;
assert_checkequal ( candcell ( C , 2,6) , [2 4 6 8 9] ) ;
assert_checkequal ( candcell ( C , 2,9) , [1 2 4 7 9] ) ;
assert_checkequal ( candcell ( C , 3,2) , [1 2 6 8] ) ;
assert_checkequal ( candcell ( C , 3,3) , [1 4 5 8] ) ;
assert_checkequal ( candcell ( C , 3,4) , [1 2 4 5 8] ) ;
assert_checkequal ( candcell ( C , 3,7) , [2 8] ) ;
assert_checkequal ( candcell ( C , 3,8) , [1 2 4 6] ) ;
assert_checkequal ( candcell ( C , 3,9) , [1 2 4] ) ;
assert_checkequal ( candcell ( C , 4,2) , [8] ) ;
assert_checkequal ( candcell ( C , 4,5) , [4] ) ;
assert_checkequal ( candcell ( C , 4,7) , [7] ) ;
assert_checkequal ( candcell ( C , 5,1) , [3] ) ;
assert_checkequal ( candcell ( C , 5,6) , [2] ) ;
assert_checkequal ( candcell ( C , 5,9) , [2 5] ) ;
assert_checkequal ( candcell ( C , 6,1) , [1 3] ) ;
assert_checkequal ( candcell ( C , 6,2) , [1 9] ) ;
assert_checkequal ( candcell ( C , 6,7) , [2 3] ) ;
assert_checkequal ( candcell ( C , 6,8) , [2 4] ) ;
assert_checkequal ( candcell ( C , 6,9) , [2 4] ) ;
assert_checkequal ( candcell ( C , 7,1) , [1 4 6 7] ) ;
assert_checkequal ( candcell ( C , 7,3) , [1 4] ) ;
assert_checkequal ( candcell ( C , 7,4) , [4 8] ) ;
assert_checkequal ( candcell ( C , 7,6) , [4 6 8] ) ;
assert_checkequal ( candcell ( C , 7,8) , [1] ) ;
assert_checkequal ( candcell ( C , 8,1) , [1 2 4 7] ) ;
assert_checkequal ( candcell ( C , 8,3) , [1 4] ) ;
assert_checkequal ( candcell ( C , 8,5) , [4 5 7] ) ;
assert_checkequal ( candcell ( C , 8,6) , [4] ) ;
assert_checkequal ( candcell ( C , 8,8) , [1 2 5] ) ;
assert_checkequal ( candcell ( C , 9,2) , [2 6 9] ) ;
assert_checkequal ( candcell ( C , 9,3) , [9] ) ;
assert_checkequal ( candcell ( C , 9,4) , [5] ) ;
assert_checkequal ( candcell ( C , 9,6) , [3 6] ) ;
assert_checkequal ( candcell ( C , 9,9) , [2 5] ) ;
LE = [
6 5 5   5 3 5   3 4 5 
5 5 3   4 2 5   0 0 5 
0 4 4   5 0 0   2 4 3 
..
0 1 0   0 1 0   1 0 0 
1 0 0   0 0 1   0 0 2 
2 2 0   0 0 0   2 2 2 
..
4 0 2   2 0 3   0 1 0 
4 0 2   0 3 1   0 3 0 
0 3 1   1 0 2   0 0 2 
];
assert_checkequal ( L , LE );

// Remove v = 2 from block 1,1
[C,L] = sudoku_candidates ( X );
[C,L] = sudoku_candidateremove ( C , L , 3 , 1 , 1 , 2 );
assert_checkequal ( candcell ( C , 1,1) , [1 3 4 5 6 7] ) ;
assert_checkequal ( candcell ( C , 1,2) , [1 6 7 8] ) ;
assert_checkequal ( candcell ( C , 1,3) , [1 3 4 5 8] ) ;
assert_checkequal ( candcell ( C , 1,4) , [1 2 4 5 8] ) ;
assert_checkequal ( candcell ( C , 1,5) , [4 5 6] ) ;
assert_checkequal ( candcell ( C , 1,6) , [2 4 6 8 9] ) ;
assert_checkequal ( candcell ( C , 1,7) , [2 7 8] ) ;
assert_checkequal ( candcell ( C , 1,8) , [1 2 4 6] ) ;
assert_checkequal ( candcell ( C , 1,9) , [1 2 4 7 9] ) ;
assert_checkequal ( candcell ( C , 2,1) , [1 4 6 7] ) ;
assert_checkequal ( candcell ( C , 2,2) , [1 6 7 8] ) ;
assert_checkequal ( candcell ( C , 2,3) , [1 4 8] ) ;
assert_checkequal ( candcell ( C , 2,4) , [1 2 4 8] ) ;
assert_checkequal ( candcell ( C , 2,5) , [4 6] ) ;
assert_checkequal ( candcell ( C , 2,6) , [2 4 6 8 9] ) ;
assert_checkequal ( candcell ( C , 2,9) , [1 2 4 7 9] ) ;
assert_checkequal ( candcell ( C , 3,2) , [1 6 8] ) ;
assert_checkequal ( candcell ( C , 3,3) , [1 4 5 8] ) ;
assert_checkequal ( candcell ( C , 3,4) , [1 2 4 5 8] ) ;
assert_checkequal ( candcell ( C , 3,7) , [2 8] ) ;
assert_checkequal ( candcell ( C , 3,8) , [1 2 4 6] ) ;
assert_checkequal ( candcell ( C , 3,9) , [1 2 4] ) ;
assert_checkequal ( candcell ( C , 4,1) , [5] ) ;
assert_checkequal ( candcell ( C , 4,2) , [8] ) ;
assert_checkequal ( candcell ( C , 4,5) , [4] ) ;
assert_checkequal ( candcell ( C , 4,7) , [7] ) ;
assert_checkequal ( candcell ( C , 5,1) , [3 5] ) ;
assert_checkequal ( candcell ( C , 5,6) , [2] ) ;
assert_checkequal ( candcell ( C , 5,9) , [2 5] ) ;
assert_checkequal ( candcell ( C , 6,1) , [1 3] ) ;
assert_checkequal ( candcell ( C , 6,2) , [1 9] ) ;
assert_checkequal ( candcell ( C , 6,7) , [2 3] ) ;
assert_checkequal ( candcell ( C , 6,8) , [2 4] ) ;
assert_checkequal ( candcell ( C , 6,9) , [2 4] ) ;
assert_checkequal ( candcell ( C , 7,1) , [1 4 6 7] ) ;
assert_checkequal ( candcell ( C , 7,3) , [1 4] ) ;
assert_checkequal ( candcell ( C , 7,4) , [4 8] ) ;
assert_checkequal ( candcell ( C , 7,6) , [4 6 8] ) ;
assert_checkequal ( candcell ( C , 7,8) , [1] ) ;
assert_checkequal ( candcell ( C , 8,1) , [1 2 4 7] ) ;
assert_checkequal ( candcell ( C , 8,3) , [1 4] ) ;
assert_checkequal ( candcell ( C , 8,5) , [4 5 7] ) ;
assert_checkequal ( candcell ( C , 8,6) , [4] ) ;
assert_checkequal ( candcell ( C , 8,8) , [1 2 5] ) ;
assert_checkequal ( candcell ( C , 9,2) , [2 6 9] ) ;
assert_checkequal ( candcell ( C , 9,3) , [9] ) ;
assert_checkequal ( candcell ( C , 9,4) , [5] ) ;
assert_checkequal ( candcell ( C , 9,6) , [3 6] ) ;
assert_checkequal ( candcell ( C , 9,9) , [2 5] ) ;
LE = [
6 4 5   5 3 5   3 4 5 
4 4 3   4 2 5   0 0 5 
0 3 4   5 0 0   2 4 3 
..
1 1 0   0 1 0   1 0 0 
2 0 0   0 0 1   0 0 2 
2 2 0   0 0 0   2 2 2 
..
4 0 2   2 0 3   0 1 0 
4 0 2   0 3 1   0 3 0 
0 3 1   1 0 2   0 0 2 
];
assert_checkequal ( L , LE );

// Remove v = 2 from cell 1,1
[C,L] = sudoku_candidates ( X );
[C,L] = sudoku_candidateremove ( C , L , 4 , 1 , 1 , 2 );
assert_checkequal ( candcell ( C , 1,1) , [1 3 4 5 6 7] ) ;
assert_checkequal ( candcell ( C , 1,2) , [1 2 6 7 8] ) ;
assert_checkequal ( candcell ( C , 1,3) , [1 3 4 5 8] ) ;
assert_checkequal ( candcell ( C , 1,4) , [1 2 4 5 8] ) ;
assert_checkequal ( candcell ( C , 1,5) , [4 5 6] ) ;
assert_checkequal ( candcell ( C , 1,6) , [2 4 6 8 9] ) ;
assert_checkequal ( candcell ( C , 1,7) , [2 7 8] ) ;
assert_checkequal ( candcell ( C , 1,8) , [1 2 4 6] ) ;
assert_checkequal ( candcell ( C , 1,9) , [1 2 4 7 9] ) ;
assert_checkequal ( candcell ( C , 2,1) , [1 2 4 6 7] ) ;
assert_checkequal ( candcell ( C , 2,2) , [1 2 6 7 8] ) ;
assert_checkequal ( candcell ( C , 2,3) , [1 4 8] ) ;
assert_checkequal ( candcell ( C , 2,4) , [1 2 4 8] ) ;
assert_checkequal ( candcell ( C , 2,5) , [4 6] ) ;
assert_checkequal ( candcell ( C , 2,6) , [2 4 6 8 9] ) ;
assert_checkequal ( candcell ( C , 2,9) , [1 2 4 7 9] ) ;
assert_checkequal ( candcell ( C , 3,2) , [1 2 6 8] ) ;
assert_checkequal ( candcell ( C , 3,3) , [1 4 5 8] ) ;
assert_checkequal ( candcell ( C , 3,4) , [1 2 4 5 8] ) ;
assert_checkequal ( candcell ( C , 3,7) , [2 8] ) ;
assert_checkequal ( candcell ( C , 3,8) , [1 2 4 6] ) ;
assert_checkequal ( candcell ( C , 3,9) , [1 2 4] ) ;
assert_checkequal ( candcell ( C , 4,1) , [5] ) ;
assert_checkequal ( candcell ( C , 4,2) , [8] ) ;
assert_checkequal ( candcell ( C , 4,5) , [4] ) ;
assert_checkequal ( candcell ( C , 4,7) , [7] ) ;
assert_checkequal ( candcell ( C , 5,1) , [3 5] ) ;
assert_checkequal ( candcell ( C , 5,6) , [2] ) ;
assert_checkequal ( candcell ( C , 5,9) , [2 5] ) ;
assert_checkequal ( candcell ( C , 6,1) , [1 3] ) ;
assert_checkequal ( candcell ( C , 6,2) , [1 9] ) ;
assert_checkequal ( candcell ( C , 6,7) , [2 3] ) ;
assert_checkequal ( candcell ( C , 6,8) , [2 4] ) ;
assert_checkequal ( candcell ( C , 6,9) , [2 4] ) ;
assert_checkequal ( candcell ( C , 7,1) , [1 4 6 7] ) ;
assert_checkequal ( candcell ( C , 7,3) , [1 4] ) ;
assert_checkequal ( candcell ( C , 7,4) , [4 8] ) ;
assert_checkequal ( candcell ( C , 7,6) , [4 6 8] ) ;
assert_checkequal ( candcell ( C , 7,8) , [1] ) ;
assert_checkequal ( candcell ( C , 8,1) , [1 2 4 7] ) ;
assert_checkequal ( candcell ( C , 8,3) , [1 4] ) ;
assert_checkequal ( candcell ( C , 8,5) , [4 5 7] ) ;
assert_checkequal ( candcell ( C , 8,6) , [4] ) ;
assert_checkequal ( candcell ( C , 8,8) , [1 2 5] ) ;
assert_checkequal ( candcell ( C , 9,2) , [2 6 9] ) ;
assert_checkequal ( candcell ( C , 9,3) , [9] ) ;
assert_checkequal ( candcell ( C , 9,4) , [5] ) ;
assert_checkequal ( candcell ( C , 9,6) , [3 6] ) ;
assert_checkequal ( candcell ( C , 9,9) , [2 5] ) ;
LE = [
6 5 5   5 3 5   3 4 5 
5 5 3   4 2 5   0 0 5 
0 4 4   5 0 0   2 4 3 
..
1 1 0   0 1 0   1 0 0 
2 0 0   0 0 1   0 0 2 
2 2 0   0 0 0   2 2 2 
..
4 0 2   2 0 3   0 1 0 
4 0 2   0 3 1   0 3 0 
0 3 1   1 0 2   0 0 2 
];
assert_checkequal ( L , LE );

