// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction

X  = [
    6    7    3    1    5    8    2    4    9  
    4    1    8    2    6    9    5    3    7  
    9    2    5    4    3    7    8    6    1  
    5    8    2    3    4    1    7    9    6  
    3    4    7    6    9    2    1    8    5  
    1    9    6    7    8    5    3    2    4  
    7    5    4    8    2    6    9    1    3  
    2    3    1    9    7    4    6    5    8  
    8    6    9    5    1    3    4    7    2  
];
Y = sudoku_delrandom ( X , 5 , 2 );
assert_checkequal ( sum(Y)<405 , %t );
// Check that at least one entry has been removed in each subblock
for i = [1 4 7]
for j = [1 4 7]
B = Y(tri(i),tri(j));
assert_checkequal ( sum(B)<45 , %t );
end
end

