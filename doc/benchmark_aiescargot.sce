// Copyright (C) 2010 - Michael Baudin

// A simple test against the AI Escargot
dirtst = get_absolute_file_path("benchmark_aiescargot.sce");

// Test solvebylogic

X = sudoku_readsdk ( dirtst + "../collection/aiescargot.sdk" )
e = sum(X==0);
h = sum(X>0);
timer();
[X,iter,C,L] = sudoku_solvebylogic ( X );
tt = timer();
nrounds = sum(iter);
nstrats = sum(iter>0);
issolved = sudoku_issolved(X);
r = sum(X==0);
mprintf("Hints = %d, Time = %f (s), iter = [%s], nrounds = %d, nstrats = %d, remaining = %d\n", h , tt , strcat(string(iter)," ") , nrounds , nstrats , r );
// Hints = 23, Time = 24.555309 (s), iter = [2 2 1 1 1 1 1 1 1 1 1], nrounds = 13, nstrats = 11, remaining = 57

timer();
[X,iter,maxlevel] = sudoku_solve ( X , %t );
tt = timer();
mprintf("Hints = %d, Time = %f (s), iter = [%s], nrounds = %d, nstrats = %d, remaining = %d\n", h , tt , strcat(string(iter)," ") , nrounds , nstrats , r );

if ( %f ) then
iter  = [    584.    210.    143.    122.    66.    62.    60.    60.    60.    60.    60.    60.  ]
// The solution:
Z  = [
    1.    6.    2.    8.    5.    7.    4.    9.    3.  
    5.    3.    4.    1.    2.    9.    6.    7.    8.  
    7.    8.    9.    6.    4.    3.    5.    2.    1.  
    4.    7.    5.    3.    1.    2.    9.    8.    6.  
    9.    1.    3.    5.    8.    6.    7.    4.    2.  
    6.    2.    8.    7.    9.    4.    1.    3.    5.  
    3.    5.    6.    4.    7.    8.    2.    1.    9.  
    2.    4.    1.    9.    3.    5.    8.    6.    7.  
    8.    9.    7.    2.    6.    1.    3.    5.    4.  
];
// The state at the start.
//
// L  =
// 
//    0.    4.    4.    3.    3.    0.    3.    0.    3.  
//    3.    0.    2.    4.    0.    3.    4.    3.    0.  
//    4.    3.    0.    0.    3.    3.    0.    4.    3.  
//    4.    3.    0.    0.    3.    3.    0.    4.    3.  
//    3.    0.    2.    3.    0.    3.    3.    5.    0.  
//    0.    4.    3.    5.    4.    0.    3.    4.    3.  
//    0.    5.    3.    6.    5.    5.    4.    0.    4.  
//    4.    0.    4.    5.    5.    7.    3.    4.    0.  
//    4.    5.    0.    6.    5.    6.    0.    5.    4.  
// C  =
// 
// 
//!{}         [2;5;6;8]    [2;4;6;8]  [4;5;8]        [3;4;5]      {}               [2;4;6]    {}           [3;4;6]    !
//!                                                                                                                   !
//![4;5;7]    {}           [4;6]      [1;4;5;9]      {}           [1;5;9]          [1;4;6;7]  [4;6;7]      {}         !
//!                                                                                                                   !
//![2;4;7;8]  [2;7;8]      {}         {}             [1;3;4]      [1;3;8]          {}         [2;3;4;7]    [1;3;4]    !
//!                                                                                                                   !
//![2;4;7;8]  [2;7;8]      {}         {}             [1;6;7]      [1;2;6]          {}         [4;6;7;8]    [1;4;6]    !
//!                                                                                                                   !
//![4;7;9]    {}           [3;4]      [5;7;9]        {}           [5;6;9]          [4;6;7]    [3;4;5;6;7]  {}         !
//!                                                                                                                   !
//!{}         [2;7;8;9]    [2;3;8]    [1;2;5;7;9]    [1;5;7;9]    {}               [1;7;8]    [3;5;7;8]    [1;3;5]    !
//!                                                                                                                   !
//!{}         [2;5;6;8;9]  [2;6;8]    [2;4;5;7;8;9]  [4;5;6;7;9]  [2;5;6;8;9]      [2;4;6;8]  {}           [4;5;6;9]  !
//!                                                                                                                   !
//![2;5;8;9]  {}           [1;2;6;8]  [1;2;5;8;9]    [1;3;5;6;9]  [1;2;3;5;6;8;9]  [2;6;8]    [2;5;6;8]    {}         !
//!                                                                                                                   !
//![2;5;8;9]  [2;5;6;8;9]  {}         [1;2;4;5;8;9]  [1;4;5;6;9]  [1;2;5;6;8;9]    {}         [2;4;5;6;8]  [4;5;6;9]  !
//
end

