function [puzzle,solution] = sudoku_generate ( varargin )
  //   Generate a puzzle and its solution.
  //
  // Calling Sequence
  //   [puzzle,solution] = sudoku_generate ( difficulty )
  //   [puzzle,solution] = sudoku_generate ( difficulty , verbose )
  //
  // Parameters
  // difficulty: the level of the puzzle, difficulty=1 is easy, difficulty=2 is medium, difficulty=3 is more difficult, difficulty=4 is extremely difficult
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  // puzzle: the 9x9 puzzle matrix, with zeros for unknown entries
  // solution: the 9x9 solution matrix without any zero
  //
  // Description
  //   Generates a puzzle and its solution based on the difficulty level.
  //  
  //   The algorithm generates a random sudoku with sudoku_fillrandom. 
  //   Then delete some entries according to the
  //   difficulty. Solves the puzzle and the transposed puzzle. If the
  //   two solutions are equal, the solution is probably unique: returns it.
  //   If not, continue to generate puzzles until unicity is achieved.
  //   This algorithm is less slow (typically less than 30 seconds).
  //   
  //   This algorithm is limited in the sense that the difficulty 
  //   is implied by the number of givens.
  //   Easy               : unknowns from 40 to 45 (i.e. from 36 to 41 givens)
  //   Medium             : unknowns from 46 to 49 (i.e. from 32 to 35 givens)
  //   Difficult          : unknowns from 50 to 53 (i.e. from 28 to 31 givens)
  //   Extremely difficult: unknowns from 54 to 58 (i.e. from 27 to 23 givens)
  //   There is no real link between the number of unknowns and the actual
  //   difficulty of a sudoky puzzle. Therefore, this procedure is of limited
  //   usefulness.
  //
  //   The puzzle created by this algorithm is not symetric.
  //
  // Examples
  // [puzzle,solution] = sudoku_generate ( 1 )
  //
  // Authors
  //   Stefan Bleeck, 2005
  //   Michael Baudin, Scilab port, comments, 2010
  //
  // Bibliography
  //   "Enumerating possible Sudoku grids", Bertram Felgenhauer, Frazer Jarvis, June 2005

  [lhs,rhs]=argn();
  if ( rhs < 1 | rhs > 2 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 1 or 2 are expected."), "sudoku_generate", rhs);
    error(errmsg)
  end

  difficulty = varargin(1)
  if ( rhs < 2 ) then
    verbose = %f
  else
    verbose = varargin(2)
  end

  apifun_typereal ( difficulty , "difficulty" , 1 )
  apifun_typeboolean ( verbose , "verbose" , 2 )
  
  //
  // Generate a filled puzzle
  solution = sudoku_fillrandom ( verbose )

  iter = 0
  
  //
  // While the solution of the puzzle is not unique, generate a new 
  // puzzle
  while ( %t )
    iter = iter + 1
    if ( verbose ) then
      mprintf ( "==============================\n")
      mprintf ( "[%d] Searching for a puzzle...\n",iter)
    end
    while ( %t ) then
      select difficulty
      case 1
        puzzle = sudoku_delrandom ( solution , 4.72 , 0.5 )
      case 2
        puzzle = sudoku_delrandom ( solution , 5.27 , 0.5 )
      case 3
        puzzle = sudoku_delrandom ( solution , 5.72 , 0.5 )
      case 4
        puzzle = sudoku_delrandom ( solution , 6.22 , 0.5 )
      else
        errmsg = msprintf(gettext("%s: Unexpected difficulty : %d provided while 1 to 4 is expected."), "sudoku_generate", difficulty);
        error(errmsg)
      end
      if ( verbose ) then
        mprintf ( "Number of givens: %d\n",sum(puzzle>0))
      end
      if ( sum(puzzle>0) > 17 ) then
        break
      end
    end
    
    if ( verbose ) then
      mprintf ( "Solve...\n")
    end
    [SS,iter,maxlevel] = sudoku_solve ( puzzle, verbose, [64 %inf], 0, %t )
    if ( verbose ) then
      mprintf ( "Number of solutions: %d\n",size(SS))
    end
    if ( size(SS) == 1 ) then
      // If unique, this is fine.
      break
    else
      if ( verbose ) then
        mprintf ( "Solution is not unique, try again...\n")
      end
    end
  end
endfunction

//
// Example
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction


// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction

