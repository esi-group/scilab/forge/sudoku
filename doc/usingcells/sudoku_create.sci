function [puzzle,solution] = sudoku_create ( varargin )
  //   Generate a puzzle and its solution.
  //
  // Calling Sequence
  //   [puzzle,solution] = sudoku_create ( )
  //   [puzzle,solution] = sudoku_create ( verbose )
  //   [puzzle,solution] = sudoku_create ( verbose , clues )
  //   [puzzle,solution] = sudoku_create ( verbose , clues , maxiter )
  //   [puzzle,solution] = sudoku_create ( verbose , clues , maxiter , solution )
  //
  // Parameters
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  // clues: the number of givens to put in the puzzle (default = 28)
  // maxiter: the maximum number of loops (default = 100)
  // solution: a solution where to start from (default = automatically generated)
  // puzzle: the 9x9 puzzle matrix, with zeros for unknown entries
  // solution: the 9x9 solution matrix without any zero
  //
  // Description
  //   Creates a puzzle and its solution.
  //  
  //   This algorithm is based on the suggestions provided by Stuart.
  //   The target number of given is computed as a bell-curve around 18.
  //   There is never less than 32 givens.
  //
  //   This algorithm creates sudokus which are almost symetric.
  //
  // Examples
  // [puzzle,solution] = sudoku_generate ( 1 )
  //
  // Authors
  //   Michael Baudin, Scilab port, comments, 2010
  //
  // Bibliography
  //   "Sudoku Creation and Grading", Andrew C. Stuart, 3rd February 2007

  [lhs,rhs]=argn();
  if ( rhs > 4 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while at most 4 are expected."), "sudoku_create", rhs);
    error(errmsg)
  end

  if ( rhs < 1 ) then
    verbose = %f
  else
    verbose = varargin(1)
  end
  if ( rhs < 2 ) then
    clues = 28
  else
    clues = varargin(2)
  end
  if ( rhs < 3 ) then
    maxiter = 100
  else
    maxiter = varargin(3)
  end
  if ( rhs < 4 ) then
    //
    // Generate a filled puzzle
    if ( verbose ) then
      mprintf("Making solution...\n")
    end
    solution = sudoku_fillrandom ( verbose )
  else
    solution = varargin(4)
  end

  apifun_typeboolean ( verbose , "verbose" , 1 )
  apifun_typereal ( clues , "clues" , 2 )
  apifun_typereal ( maxiter , "maxiter" , 3 )
  apifun_typereal ( solution , "solution" , 4 )
  
  if ( verbose ) then
    mprintf("Target number of givens is %d\n", clues)
  end
  //
  // Delete cells until we find a good puzzle
  k = 0
  puzzle = solution
  remove = 8
  // fail is the number of successive attempts during which 
  // the algorithm was unable to find one or more clues to delete.
  fail = 0
  // The number of entries to remove is reduced only
  // when there are more than fmax consecutive steps where
  // there are more than one solution
  fmax = 2
  for k = 1 : maxiter
    if ( verbose ) then
      mprintf("======================\n")
      mprintf("Generate #%d\n",k)
      mprintf("Givens = %d\n",sum(puzzle>0))
      mprintf("Puzzle=""%s""\n",sudoku_writegivens(puzzle))
      mprintf("Fail = %d\n",fail)
    end
    //
    // Our current puzzle attempt is P
    mprintf("Remove = %d\n",remove)
    P = deletecells ( puzzle , remove )
    if ( verbose ) then
      mprintf("P     =""%s""\n",sudoku_writegivens(P))
    end
    //
    // Get all the solutions of the puzzle
    [SS,iter,maxlevel] = sudoku_solve ( P, %f, [64 %inf], 0, 2 )
    if ( verbose ) then
      mprintf("Number of solutions: %d\n", size(SS))
    end
    //
    // If there is only one solution and the number of givens is reached, stop
    if ( size(SS) == 1 & sum(P>0) == clues ) then
      if ( verbose ) then
        mprintf("Number of givens is %d. Finished.\n", sum(P>0) )
      end
      puzzle = P
      break
    end
    //
    // If there is more than one solution, try the next puzzle
    // but do not remove the digits.
    if ( size(SS) > 1 & fail >= fmax ) then
      if ( verbose ) then
        mprintf("There are more than one solution. Failure #%d/%d. Reduce removed entries. Reject puzzle.\n",fail,fmax)
      end
      fail = fail + 1
      remove = max ( [ remove / 2 , 1 ] )
      continue
    end
    if ( size(SS) > 1 ) then
      if ( verbose ) then
        mprintf("There are more than one solution. Failure #%d/%d. Reject puzzle.\n",fail,fmax)
      end
      fail = fail + 1
      continue
    end
    //
    // There is one single solution.
    fail = 0
    // If the number of givens is not attained, remove the digits and go one to the next puzzle.
    if ( sum(P>0) - remove >= clues ) then
      if ( verbose ) then
        mprintf("There are still clues to remove. Accept puzzle.\n")
      end
      puzzle = P
      continue
    end
    // If the next attempt has not enough clues, reduce the number of removes
    if ( sum(puzzle>0) - remove < clues ) then
      if ( verbose ) then
        mprintf("We must reduce the number of clues to remove on next try. Reject puzzle\n")
      end
      remove = max ( [ remove / 2 , 1 ] )
    end
  end
endfunction

//
// Randomly delete k cells in the puzzle, where k=1,2,4,or 8.
// Retains the symetry when k=2,4,8.
function P = deletecells ( puzzle , k )
  P = puzzle
  if ( k == 1 ) 
    while ( %t ) 
      // Pick a random cell in the sudoku
      s1 = ceil ( grand(1,1,"unf",0,1) * 81 )
      [i1,j1] = whatcell ( s1 )
      if ( P(i1,j1) > 0 ) then
        break
      end
    end
    // Delete this cell
    P(i1,j1) = 0
  elseif ( k == 2 ) then
    while ( %t ) 
      // Pick a random cell in the sudoku
      s1 = ceil ( grand(1,1,"unf",0,1) * 81 )
      [i1,j1] = whatcell ( s1 )
      // Get the cell symetric with respect to the center
      i2 = 9 - i1 + 1
      j2 = 9 - j1 + 1
      if ( P(i1,j1) > 0 ) then
        break
      end
    end
    // Delete these two cells
    P(i1,j1) = 0
    P(i2,j2) = 0
  elseif ( k == 4 ) then
    P = deletecells ( P , 2 )
    P = deletecells ( P , 2 )
  elseif ( k == 8 ) then
    P = deletecells ( P , 4 )
    P = deletecells ( P , 4 )
  else
    errmsg = msprintf(gettext("%s: Unexpected number of cells to delete : %d."), "deletecells", k);
    error(errmsg)
  end
endfunction
//
// Compute the level of the puzzle.
// TODO : Use in a separate routine.
function level = computelevel ( L , iter )
    if ( sum(L) == 0 & and(iter(4:$)==0) ) then
      level = 1
    elseif ( sum(L) == 0 & and(iter(10)==0) ) then
      level = 2
    elseif ( sum(L) == 0 & iter(10)> 0 ) then
      level = 3
    else
      // Sudoku is unsolved by logic
      level = 4
    end
endfunction
//
// Example
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction

// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction

