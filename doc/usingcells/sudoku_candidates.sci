function [C,L] = sudoku_candidates ( X ) 
  //   Returns candidates for a sudoku X.
  //
  // Calling Sequence
  //   [C,s,e] = sudoku_candidates(X)
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries.
  // C: a cell array of candidate vectors for each cell.
  // L: the number of candidates for each cell
  //
  // Description
  //   Returns candidates for a sudoku X.
  //
  // Examples
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 0 0 0   0 0 0   5 3 0
  // 9 0 0   0 3 7   0 0 0
  // 0 0 2   3 0 1   0 9 6
  // 0 4 7   6 9 0   1 8 0
  // 0 0 6   7 8 5   0 0 0
  // 0 5 0   0 2 0   9 0 3
  // 0 3 0   9 0 0   6 0 8
  // 8 0 0   0 1 0   4 7 0
  // ]
  // [C,L] = sudoku_candidates(X)
  //
  // See also
  //   sudoku_candidateremove
  //   sudoku_candidates
  //   sudoku_candidatescell
  //   sudoku_confirmcell
  //
  // Authors
  //   Cleve Moler, 2009
  //   Michael Baudin, Scilab port and comments, 2010

  C = cell(9,9)
  L = zeros(9,9)
  for s = find(X==0)
    [i,j] = whatcell ( s )
    d = 1:9
    zone = X(i,:)
    d(zone(find(zone>0))) = 0
    zone = X(:,j)
    d(zone(find(zone>0))) = 0
    zone = X(tri(i),tri(j))
    d(zone(find(zone>0))) = 0
    C(i,j).entries = d(find(d>0))'
    L(i,j) = size(find(d>0),"*")
  end 
endfunction


// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction

// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction

