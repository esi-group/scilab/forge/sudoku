function [ X , C , L ] = sudoku_confirmcell ( varargin )
  //   Confirm the value of a cell.
  //
  // Calling Sequence
  //   [ X , C , L ] = sudoku_confirmcell ( X , C , L , i , j , v )
  //   [ X , C , L ] = sudoku_confirmcell ( X , C , L , i , j , v , verbose )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // i: the row of the value
  // j: the column of the value
  // v: the value to remove
  //
  // Description
  //   Confirms v as a value for X(i,j) and remove this value 
  //   from candidates of the row, column and block.
  //
  // Examples
  // 
  // X = [
  // 0 6 0   0 9 0   0 0 0
  // 0 0 3   1 0 0   0 9 0
  // 1 9 0   0 2 6   3 0 0
  // ..
  // 8 0 0   5 0 0   4 1 0
  // 0 0 0   0 6 0   0 0 0
  // 0 5 1   0 0 3   0 0 9
  // ..
  // 0 0 9   6 3 0   0 2 7
  // 0 2 0   0 0 4   5 0 0
  // 0 0 0   0 1 0   0 4 0
  // ];
  // [C,L] = sudoku_candidates ( X )
  // [ X , C , L ] = sudoku_confirmcell ( X , C , L , 4 , 5 , 7 , %t ) // The cell (4,5) is a naked single.
  //
  // See also
  //   sudoku_candidateremove
  //   sudoku_candidates
  //   sudoku_candidatescell
  //   sudoku_confirmcell
  //
  // Authors
  // Michael Baudin, 2010
  
  [lhs,rhs]=argn();
  if ( rhs < 6 | rhs > 7 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 6 or 7 are expected."), "sudoku_confirmcell", rhs);
    error(errmsg)
  end
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  i = varargin(4)
  j = varargin(5)
  v = varargin(6)
  if ( rhs < 7 ) then
    verbose = %f
  else
    verbose = varargin(7)
  end

  // Confirm v for (i,j)
  X(i,j) = v
  C(i,j).entries = []
  L(i,j) = 0
  if ( verbose ) then
    mprintf("Confirmed (%d,%d) as %d (remains %d unknowns)\n",i,j,v,sum(X==0))
  end
  // Remove v from row i of C
  for c = 1 : 9
    if ( c <> j & find(C(i,c).entries==v) <> [] ) then
      z = C(i,c).entries
      z(find(z==v)) = 0
      C(i,c).entries = nonzeros(z)
      L(i,c) = L(i,c) - 1
      if ( verbose ) then
        mprintf("Remove candidate %d in (%d,%d) (remaining %d candidates)\n",v,i,c,sum(L))
      end
    end
  end
  // Remove v from column j of C
  for r = 1 : 9
    if ( r <> i & find(C(r,j).entries==v) <> [] ) then
      z = C(r,j).entries
      z(find(z==v)) = 0
      C(r,j).entries = nonzeros(z)
      L(r,j) = L(r,j) - 1
      if ( verbose ) then
        mprintf("Remove candidate %d in (%d,%d) (remaining %d candidates)\n",v,r,j,sum(L))
      end
    end
  end
  // Remove v from block (i,j) of C
  for r = tri(i)
    for c = tri(j)
      if ( r <> i & c <> j & find(C(r,c).entries==v) <> [] ) then
        z = C(r,c).entries
        z(find(z==v)) = 0
        C(r,c).entries = nonzeros(z)
        L(r,c) = L(r,c) - 1
        if ( verbose ) then
          mprintf("Remove candidate %d in (%d,%d) (remaining %d candidates)\n",v,r,c,sum(L))
        end
      end
    end
  end

endfunction

//
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction
//
// nonzeros --
// Returns the values in A which are nonzero.
//   nonzeros([0 1 0 2 3 4]) = [1 2 3 4]
//   nonzeros([0 0 0 4]) = 4
function s = nonzeros ( A )
  [i,j,s] = mtlb_find( A )
endfunction


