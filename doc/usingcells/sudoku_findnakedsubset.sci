function [X,C,L] = sudoku_findnakedsubset ( varargin )
  //   Find naked subsets of given length.
  //
  // Calling Sequence
  //   [X,C,L] = sudoku_findnakedsubset ( X , C , L , k )
  //   [X,C,L] = sudoku_findnakedsubset ( X , C , L , k , stopatfirst )
  //   [X,C,L] = sudoku_findnakedsubset ( X , C , L , k , stopatfirst , verbose )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // k: the length of the naked subset
  // stopatfirst: if %t, then stop when one or more candidates have been removed. (default = %t)
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  //
  // Description
  //   Search for naked subsets of length k.
  //   For k=1, finds naked singles, for k=2, finds naked pairs, for k=3, finds
  //   naked triples, for k = 4, finds naked quads.
  //
  //   The algorithm computes a merged list of all candidates in the row, column
  //   or block. This merged list is simplified and only unique candidates are kept.
  //   The list is used to generate all combinations of k elements.
  //   Each combination S is searched in the row, column or block.
  //   The row, column or block contains a naked subset of length k if exactly k  
  //   cells exactly have candidates all contained in S.
  //   If a naked subset of length k is found, these candidates are eliminated from 
  //   the other cells in the row, column or block.
  // 
  //   This generic algorithm can be specialized to find naked pairs, naked triples,
  //   naked quads, naked sextets, naked septets and naked octets.
  //   If k is near 4, the cost is higher. When k is near 1 or n, the cost is smaller.
  //   On the contrary that one may think at first, at worst, the number of naked subsets 
  //   of length 7 is smaller than the number of triplets. Indeed, there are at most 9 possible 
  //   candidates in the current row, column or block. Now, consider the following table
  //   of the binomial number (9,k) with k from 1 to 9: 9  36  84  126  126  84  36  9  1.
  //   Hence it costs less to try all naked subsets of length 7 (36 combinations), than
  //   to try all naked combinations of length 4 (126 combinations).
  //   
  //   Note on interrupting the algorithm
  //
  //   Consider a row, column or block with n unfixed entries. Assume that there are 
  //   p cells with a naked subset with candidates (a1,a2,...,ap). 
  //   We can prove that there is an hidden subset of length n-p (see solvebylogic).
  //   The consequence is that, depending on the searched length k and the number of 
  //   unfixed entries n. The following is the list of all cases, where Nk represents
  //   a Naked subset of length k and Hk represents a Hidden subset of length k.
  //   For each value of n, we give the list of possible combinations of naked and 
  //   hidden subsets which can be found by the algorithm.
  //   n=9: N1+H8, N2+H7, N3+H6, N4+H5, N5+H4, N6+H3, N7+H2, N8+H1
  //   In the case N2+H7, there a Naked pair and a Hidden 7-set.
  //   The hidden 7-set will not be searched, but the Naked pair will be found.
  //   Hence, the list of possible subsets which might be found is: N1, N2, N3, N4, H4, H3, H2, H1.
  //   We conclude that the maximum length of a subset is kmax = 4 if n=9.
  //   
  //   In the following list of cases, we only detail the found subsets.
  //   n=9: N1, N2, N3, N4, H4, H3, H2, H1, kmax = 4
  //   n=8: N1, N2, N3, N4, H4, H3, H2, H1, kmax = 4
  //   n=7: N1, N2, N3, H3, H2, H1,         kmax = 3
  //   n=6: N1, N2, N3, H3, H2, H1,         kmax = 3
  //   n=5: N1, N2, H2, H1,                 kmax = 2
  //   n=4: N1, N2, H2, H1,                 kmax = 2
  //   n=3: N1, H1,                         kmax = 1
  //   n=2: N1, H1,                         kmax = 1
  //
  //   The general rule is that the maximum length a subset in a row, column or block
  //   with n unfixed cells is kmax = ceil(n/2).
  //   If k is larger that kmax, we are sure that there will not be 
  //   any subset of this length is the row, column or block.
  //
  // Examples
  // X = [
  // 4 0 0   3 9 0   0 0 2
  // 2 6 0   0 5 8   3 9 0
  // 5 9 3   6 0 0   1 8 0
  // ..
  // 1 0 0   8 6 0   0 0 9
  // 6 0 5   9 0 0   2 0 0 
  // 0 3 9   2 4 5   0 1 6
  // ..
  // 0 5 6   0 0 9   0 2 0
  // 0 1 4   7 0 0   9 0 5
  // 9 0 0   5 3 0   0 0 0
  // ];
  // [C,L] = sudoku_candidates(X);
  // sudoku_findnakedsubset ( X , C , L , %t ); // Found triple {1,7,8} in row 1 at (1,2), (1,3), (1,6).
  //
  // Authors
  //   Michael Baudin, 2010
  //
  

  [lhs,rhs]=argn();
  if ( rhs < 4 | rhs > 6 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 4 to 6 are expected."), "sudoku_findnakedsubset", rhs);
    error(errmsg)
  end
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  k = varargin(4)
  if ( rhs < 5 ) then
    stopatfirst = %t
  else
    stopatfirst = varargin(5)
  end
  if ( rhs < 6 ) then
    verbose = %f
  else
    verbose = varargin(6)
  end
  
  apifun_typereal ( X , "X" , 1 )
  apifun_typecell ( C , "C" , 2 )
  apifun_typereal ( L , "L" , 3 )
  apifun_typereal ( k , "k" , 4 )
  apifun_typeboolean ( stopatfirst , "stopatfirst" , 5 )
  apifun_typeboolean ( verbose , "verbose" , 6 );
  
  before = sum(L)

  // Search for subsets in rows
  for i = 1 : 9
    // See if there can be a subset of length k in this row
    n = sum(X(i,:)==0)
    kmax = ceil(n/2)
    if ( k > kmax ) then
      // There is no subset of length k, try next.
      continue
    end
    // Search for subsets in row i.
    // Assemble all candidates of the row.
    carray  = sudoku_candidatesmerge ( X , C , L , 1 , i , [] , [1 k] )
    if ( carray == [] ) then
      // There are no possible subset in this row, go on to the next
      continue
    end
    if ( size(carray,"*") < k ) then
      // There are less than k candidates in this row:
      // there cannot exist a subset here, try the next row.
      continue
    end
    // Get all possible combinations of k elements among 
    // these candidates
    cmap = combine ( carray , k )
    nk = size(cmap,"r")
    // Search for each combinations in the row i
    for m = 1 : nk
      S = cmap(m,:)
      icols = []
      for c = 1 : 9
        if ( X(i,c) == 0 ) then
          if ( areAllContained ( C(i,c).entries' , S ) ) then
            // This might be a subset: register it
            icols($+1) = c
            if ( size(icols,"*") > k ) then
              // There are already more than k cells containing the subset.
              break
            end
          end
        end
      end
      if ( size(icols,"*") <> k ) then
        // This subset is not represented in the row:
        // try next subset.
        continue
      end
      if ( k == 1 ) then
        // This is a naked single
        c = icols(1)
        v = C(i,c).entries
        if ( verbose ) then
          mprintf("Confirmed naked single in row (%d,%d) as %d\n",i,c,v)
        end
        [ X , C , L ] = sudoku_confirmcell ( X , C , L , i , c , v , verbose )
        continue
      end
      // This is a subset: remove the candidates 
      // from the other cells in the row i.
      firstverb = %t
      for c = 1 : 9
        if ( X(i,c) == 0 & and(c <> icols) ) then
          // This is a cell which does not belong to the subset
          // remove the subset from the candidates
          for v = S
            if ( find ( C(i,c).entries == v ) <> [] ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , i , c , v )
              if ( verbose ) then
                if ( firstverb ) then
                  mprintf("Found naked %d-set {%s} in row %d at columns (%s)\n" , ..
                    k , strcat(string(S)," "),i,strcat(string(icols)," "))
                  firstverb = %f
                end
                mprintf("Removed subset row candidate %d at (%d,%d) (remaining %d candidates)\n",v,i,c,sum(L))
              end
            end
          end
        end
      end
      if ( stopatfirst & sum(L) <> before ) then
        return
      end
    end
  end
  
  // Search for subsets in columns
  for j = 1 : 9
    // See if there can be a subset of length k in this column
    n = sum(X(:,j)==0)
    kmax = ceil(n/2)
    if ( k > kmax ) then
      // There is no subset of length k, try next.
      continue
    end
    // Search for subsets in column j.
    // Assemble all candidates of the column.
    carray  = sudoku_candidatesmerge ( X , C , L , 2 , [] , j , [1 k] )
    if ( carray == [] ) then
      // There are no possible subset in this column, go on to the next
      continue
    end
    if ( size(carray,"*") < k ) then
      // There are less than k candidates in this column:
      // there cannot exist a subset here, try the next column.
      continue
    end
    // Get all possible combinations of k elements among 
    // these candidates.
    cmap = combine ( carray , k )
    nk = size(cmap,"r")
    // Search for each combinations in the column i
    for m = 1 : nk
      S = cmap(m,:)
      irows = []
      for r = 1 : 9
        if ( X(r,j) == 0 ) then
          if ( areAllContained ( C(r,j).entries' , S ) ) then
            // This might be a subset: register it
            irows($+1) = r
            if ( size(irows,"*") > k ) then
              // There are already more than k cells containing the subset.
              break
            end
          end
        end
      end
      if ( size(irows,"*") <> k ) then
        // This subset is not represented in the column:
        // try next subset.
        continue
      end
      if ( k == 1 ) then
        // This is a naked single
        r = irows(1)
        v = C(r,j).entries
        if ( verbose ) then
          mprintf("Confirmed naked single in column (%d,%d) as %d\n",r,j,v)
        end
        [ X , C , L ] = sudoku_confirmcell ( X , C , L , r , j , v , verbose )
        continue
      end
      // This is a subset: remove the candidates 
      // from the other cells in the column j.
      firstverb = %t
      for r = 1 : 9
        if ( X(r,j) == 0 & and(r <> irows) ) then
          // This is a cell which does not belong to the subset
          // remove the subset from the candidates.
          for v = S
            if ( find ( C(r,j).entries == v ) <> [] ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , j , v )
              if ( verbose ) then
                if ( firstverb ) then
                  mprintf("Found naked %d-set {%s} in column %d at rows (%s)\n" , ..
                    k , strcat(string(S)," "),j,strcat(string(irows)," "))
                  firstverb = %f
                end
                mprintf("Removed subset column candidate %d at (%d,%d) (remaining %d candidates)\n",v,r,j,sum(L))
              end
            end
          end
        end
      end
      if ( stopatfirst & sum(L) <> before ) then
        return
      end
    end
  end
  // Search for subsets in blocks
  for i = [1 4 7]
    for j = [1 4 7]
      // See if there can be a subset of length k in this column
      n = sum(X(tri(i),tri(j))==0)
      kmax = ceil(n/2)
      if ( k > kmax ) then
        // There is no subset of length k, try next.
        continue
      end
      // Search for subsets in block (i,j)
      // Assemble all candidates of the block
      carray  = sudoku_candidatesmerge ( X , C , L , 3 , i , j , [1 k] )
      if ( carray == [] ) then
        // There are no possible subset in this block, go on to the next
        continue
      end
      if ( size(carray,"*") < k ) then
        // There are less than k candidates in this block:
        // there cannot exist a subset here, try the next block.
        continue
      end
      // Get all possible combinations of 3 elements among 
      // these candidates
      cmap = combine ( carray , k )
      nk = size(cmap,"r")
      // Search for each combinations in the block (i,j)
      for m = 1 : nk
        S = cmap(m,:)
        irows = []
        icols = []
        for r = tri(i)
          for c = tri(j)
            if ( X(r,c) == 0 ) then
              if ( areAllContained ( C(r,c).entries' , S ) ) then
                // This might be a subset: register it
                irows($+1) = r
                icols($+1) = c
                if ( size(irows,"*") > k ) then
                  // There are already more than k cells containing the subset.
                  break
                end
              end
            end
          end
          if ( size(irows,"*") > k ) then
            break
          end
        end
        if ( size(irows,"*") <> k ) then
          // This subset is not represented in the block
          // Try next subset
          continue
        end
        if ( k == 1 ) then
          // This is a naked single
          r = irows(1)
          c = icols(1)
          v = C(r,c).entries
          if ( verbose ) then
            mprintf("Confirmed naked single in block (%d,%d) as %d\n",r,c,v)
          end
          [ X , C , L ] = sudoku_confirmcell ( X , C , L , r , c , v , verbose )
          continue
        end
        // This is a subset: remove the candidates 
        // from the other cells in the block
        firstverb = %t
        for r = tri(i)
          for c = tri(j)
            if ( X(r,c) == 0 & and(~(r == irows & c == icols)) ) then
              // This is a cell which does not belong to the subset
              // remove the subset from the candidates
              for v = S
                if ( find ( C(r,c).entries == v ) <> [] ) then
                  [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , v )
                  if ( verbose ) then
                    if ( firstverb ) then
                      mprintf("Found naked %d-set {%s} in block (%d,%d) at rows %s and cols %s \n" , ..
                        k , strcat(string(S)," "),i,j,strcat(string(irows)," "),strcat(string(icols)," "))
                      firstverb = %f
                    end
                    mprintf("Removed subset block candidate %d at (%d,%d) (remaining %d candidates)\n",v,r,c,sum(L))
                  end
                end
              end
            end
          end
        end
        if ( stopatfirst & sum(L) <> before ) then
          return
        end
      end
    end
  end
  
endfunction

// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type cell
function apifun_typecell ( var , varname , ivar )
  if ( typeof ( var ) <> "ce" ) then
    errmsg = msprintf(gettext("%s: Expected cell variable for variable %s at input #%d, but got %s instead."),"apifun_typecell", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction


// Returns the (i,j) index given the global index s in a block
// Note: in Scilab, entries are numbered column bX column.
//  whatcellinblock ( 1 ) = (1,1)
//  whatcellinblock ( 2 ) = (2,1)
//  whatcellinblock ( 3 ) = (3,1)
//  whatcellinblock ( 4 ) = (1,2)
//  ...
//  whatcellinblock ( 9 ) = (9,1)
function [i,j] = whatcellinblock ( s )
  i = modulo(s-1,3) + 1
  j = (s-i)/3 + 1
endfunction

// combine --
// Returns all combinations of k values from the row vector x as a row-by-row array
// with (n,k) rows where (n,k) is the binomial coefficient and n is the 
// number of values in x. 
//
// References
// http://home.att.net/~srschmitt/script_combinations.html
// Kenneth H. Rosen, Discrete Mathematics and Its Applications, 2nd edition (NY: McGraw-Hill, 1991), pp. 284-286.
//
// Example
//   combine ( [17 32 48 53] , 3 ) :
//       17.    32.    48.  
//       17.    32.    53.  
//       17.    48.    53.  
//       32.    48.    53.  
//
function cmap = combine ( x , k )
  n = size(x,"*")
  if ( n < k ) then
    error ( msprintf ( "%s: The number of values in x is n = %d which is lower than k = %d\n", n,k))
  end
  c = combinations ( n , k )
  cmap = zeros(c,k)
  a = 1:k
  cmap(1,:) = x(a)
  for m = 2 : c
    i = k
    while ( a(i) == n - k + i )
      i = i - 1
    end
    a(i) = a(i) + 1
    for j = i+1 : k
      a(j) = a(i) + j - i
    end
    cmap(m,:) = x(a)
  end
endfunction
//
// whatcell --
// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//
// Example
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction

// areAllContained --
// Returns %t if all the items in the row vector x are in the 
// row set S.
//
// Example 
//   areAllContained ( [3 5 7] , 1:2:11 ) is true
//   areAllContained ( [2 4 7] , 2:2:10 ) is false
function bool = areAllContained ( x , S )
  bool = %t
  for v = x
    if ( find(S==v) == [] ) then
      bool = %f
      break
    end
  end
endfunction

// combinations --
//   Returns the number of combinations of j objects chosen from n objects.
//   If the input where integers, returns also an integer.
function c = combinations ( n , j )
  c = exp(gammaln(n+1)-gammaln(j+1)-gammaln(n-j+1));
  if ( and(round(n)==n) & and(round(j)==j) ) then
    c = round ( c )
  end
endfunction

