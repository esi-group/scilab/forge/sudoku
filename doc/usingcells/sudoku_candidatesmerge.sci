function carray = sudoku_candidatesmerge ( X , C , L , kind , i , j , len )
  //   Merge candidates in the row, column or block.
  //
  // Calling Sequence
  //   carray = sudoku_candidatesmerge ( X , C , L , kind , i , j , len )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // kind: set kind=1 for row, kind=2 for column and kind=3 for block
  // i: the row of the value. If kind = 1, i in 1:9. If kind = 3, i in [1 4 7].
  // j: the column of the value. If kind = 2, j in 1:9. If kind = 3, j in [1 4 7].
  // len: an array of two reals
  // len(1): minimum number of candidates in the cell.
  // len(2): maximum number of candidates in the cell.
  //
  // Description
  //   Merge all candidates in a row, column or block and returns them.
  //   Only cells which number of candidates is between lenmin and lenmax are 
  //   taken into account.
  //   Returns only unique candidates: duplicate candidates are removed.
  //   If there is no candidate in this row, column or block, returns an empty matrix.
  //   The merged candidates are returned as a row matrix.
  //
  // Examples
  // X = [
  // 4 0 0   3 9 0   0 0 2
  // 2 6 0   0 5 8   3 9 0
  // 5 9 3   6 0 0   1 8 0
  // ..
  // 1 0 0   8 6 0   0 0 9
  // 6 0 5   9 0 0   2 0 0 
  // 0 3 9   2 4 5   0 1 6
  // ..
  // 0 5 6   0 0 9   0 2 0
  // 0 1 4   7 0 0   9 0 5
  // 9 0 0   5 3 0   0 0 0
  // ];
  // [C,L] = sudoku_candidates(X);
  // sudoku_candidatesmerge ( X , C , L , 1 , 2 , [] ); // Merge all candidates from row 2
  //
  // Authors
  //   Michael Baudin, 2010
  //
  
  
  apifun_typereal ( X , "X" , 1 )
  apifun_typecell ( C , "C" , 2 )
  apifun_typereal ( L , "L" , 3 )
  apifun_typereal ( kind , "kind" , 4 )
  apifun_typereal ( i , "i" , 5 )
  apifun_typereal ( j , "j" , 6 )
  apifun_typereal ( len , "len" , 7 )

  carray = []
  m = 1
  if ( kind == 1 ) then
    // Merge candidates in row i
    if ( i == [] ) then
      error ( mprintf("%s: Empty i is not expected when kind = 1.","sudoku_candidatesmerge"))
    end
    for c = 1 : 9
      if ( X(i,c) == 0 & L(i,c) >= len(1) & L(i,c) <= len(2) ) then
        carray(m:m+L(i,c)-1) = C(i,c).entries
        m = m + L(i,c)
      end
    end
  elseif ( kind == 2 ) then
    // Merge candidates in column j
    if ( j == [] ) then
      error ( mprintf("%s: Empty j is not expected when kind = 2.","sudoku_candidatesmerge"))
    end
    for r = 1 : 9
      if ( X(r,j) == 0 & L(r,j) >= len(1) & L(r,j) <= len(2) ) then
        carray(m:m+L(r,j)-1) = C(r,j).entries
        m = m + L(r,j)
      end
    end
  elseif ( kind == 3 ) then
    // Merge candidates in block (i,j)
    if ( i == [] ) then
      error ( mprintf("%s: Empty i is not expected when kind = 3.","sudoku_candidatesmerge"))
    end
    if ( j == [] ) then
      error ( mprintf("%s: Empty j is not expected when kind = 3.","sudoku_candidatesmerge"))
    end
    for r = tri(i)
      for c = tri(j)
        if ( X(r,c) == 0 & L(r,c) >= len(1) & L(r,c) <= len(2) ) then
          carray(m:m+L(r,c)-1) = C(r,c).entries
          m = m + L(r,c)
        end
      end
    end
  else
    errmsg = msprintf(gettext("%s: Unexpected value of kind = %d. Only kind = 1, 2 and 3 are managed."), "sudoku_candidatesmerge", kind);
    error(errmsg)
  end
  // Keep only unique candidates
  carray = unique(carray)
  carray = carray(:)'
endfunction

// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type cell
function apifun_typecell ( var , varname , ivar )
  if ( typeof ( var ) <> "ce" ) then
    errmsg = msprintf(gettext("%s: Expected cell variable for variable %s at input #%d, but got %s instead."),"apifun_typecell", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
//
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction

