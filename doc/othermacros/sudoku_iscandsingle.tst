// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction


////////////////////////////////////////

X = [
0 0 0   0 0 0   0 0 0
0 0 0   0 0 0   5 3 0
9 0 0   0 3 7   0 0 0
0 0 2   3 0 1   0 9 6
0 4 7   6 9 0   1 8 0
0 0 6   7 8 5   0 0 0
0 5 0   0 2 0   9 0 3
0 3 0   9 0 0   6 0 8
8 0 0   0 1 0   4 7 0
];
[C,L] = sudoku_candidates(X);

////////////////////////////////////////


 X  = [ 
    1.    2.    0.    0.    3.    0.    0.    4.    0.  
    3.    5.    0.    0.    0.    0.    0.    0.    0.  
    4.    6.    0.    0.    0.    0.    0.    0.    0.  
    2.    1.    0.    0.    0.    0.    0.    0.    0.  
    5.    3.    0.    0.    0.    0.    0.    0.    0.  
    6.    0.    0.    0.    0.    0.    0.    0.    0.  
    7.    0.    0.    0.    0.    0.    0.    0.    0.  
    8.    0.    0.    0.    0.    0.    0.    0.    0.  
    9.    0.    0.    0.    0.    0.    0.    0.    0.  
];
[C,L] = sudoku_candidates(X);
b = sudoku_iscandsingle ( C , 7 , 2 );
assert_equal ( b , %f );
b = sudoku_iscandsingle ( C , 8 , 2 );
assert_equal ( b , %f );
b = sudoku_iscandsingle ( C , 9 , 2 );
assert_equal ( b , %f );

