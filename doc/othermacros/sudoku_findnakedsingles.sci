function [X,C,L] = sudoku_findnakedsingles ( varargin )
  //   Fills "singletons" (i.e. naked singles).
  //
  // Calling Sequence
  //   [X,C,L] = sudoku_findnakedsingles ( X , C , L )
  //   [X,C,L] = sudoku_findnakedsingles ( X , C , L , verbose )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  //
  // Description
  //   Search for entries where there is only one possible candidate and fill them.
  //   Superseded by findnakedsubset.
  //
  //   Instead of computing all candidates, we search for unique 
  //   candidates until there is no more singleton.
  //   This is significantly faster (3 times in average) than searching for all
  //   candidates and using only a part of the matrix.
  //   This method is presented as "Column, Row and Minigrid Elimination"
  //   in "Programming Sudoku", Chapter 3.
  //   Singletons are often called "naked singles".
  //   The rows and columns are randomly permuted during the search,
  //   which, in practice, increases the speed a bit.
  //
  // Examples
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 0 0 0   0 0 0   5 3 0
  // 9 0 0   0 3 7   0 0 0
  // 0 0 2   3 0 1   0 9 6
  // 0 4 7   6 9 0   1 8 0
  // 0 0 6   7 8 5   0 0 0
  // 0 5 0   0 2 0   9 0 3
  // 0 3 0   9 0 0   6 0 8
  // 8 0 0   0 1 0   4 7 0
  // ]
  // [C,L] = sudoku_candidates(X);
  // [X,C,L] = sudoku_findnakedsingles ( X , C , L , %t )
  //
  // Authors
  //   Cleve Moler, 2009
  //   Michael Baudin, Scilab port, comments, refactoring, improved algorithm and speed, 2010
  //
  // Bibliography
  //   Programming Sudoku, Wei-Meng Lee, 2006
  
  [lhs,rhs]=argn();
  if ( rhs < 3 | rhs > 4 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 3 or 4 are expected."), "sudoku_findnakedsingles", rhs);
    error(errmsg)
  end
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  if ( rhs < 4 ) then
    verbose = %f
  else
    verbose = varargin(4)
  end

  apifun_typereal ( X , "X" , 1 )
  apifun_typecell ( C , "C" , 2 )
  apifun_typereal ( L , "L" , 3 )
  apifun_typeboolean ( verbose , "verbose" , 4 );
  
  singles = find(X == 0 & L == 1)
  if ( singles == [] ) then
    // There is no naked single.
    return
  end
  for c = grand(1,"prm",singles')'
    [i,j] = whatcell ( c )
    v = C(i,j).entries(1)
    if ( v == [] ) then
      // A naked single was present in several cells:
      // the sudoku is inconsistent.
      return
    end
    if ( verbose ) then
      mprintf("Confirmed singleton (%d,%d) as %d\n",i,j,v)
    end
    [ X , C , L ] = sudoku_confirmcell ( X , C , L , i , j , v , verbose )
  end
endfunction

// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type cell
function apifun_typecell ( var , varname , ivar )
  if ( typeof ( var ) <> "ce" ) then
    errmsg = msprintf(gettext("%s: Expected cell variable for variable %s at input #%d, but got %s instead."),"apifun_typecell", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction


// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction



// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction
// nonzeros --
// Returns the values in A which are nonzero.
//   nonzeros([0 1 0 2 3 4]) = [1 2 3 4]
//   nonzeros([0 0 0 4]) = 4
function s = nonzeros ( A )
  [i,j,s] = mtlb_find( A )
endfunction


