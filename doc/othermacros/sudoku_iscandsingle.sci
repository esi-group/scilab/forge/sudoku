function single = sudoku_iscandsingle ( C , ix , jx )
// See if a candidate is single.
//
// Calling Sequence
//   single = sudoku_iscandsingle ( C , ix , jx )
//
// Parameters
// C: a matrix of candidates
// ix: the row index of a particular candidate
// jx: the column index of a particular candidate
// single: true if the cell C(ix,jx) is unique in its row, column and block.
//
// Description
//   This algorithm is useful when we want to insert a particular 
//   candidate in a sudoku. If the candidate is unique in its row, column
//   or subblock, the insertion is fine. But if the sudoku is inconsistent,
//   it may happen that the candidate appears several times in the matrix 
//   of candidates. If we did not check, we could insert it several times.
//
// Examples
// X = [
// 0 0 0   0 0 0   0 0 0
// 0 0 0   0 0 0   5 3 0
// 9 0 0   0 3 7   0 0 0
// 0 0 2   3 0 1   0 9 6
// 0 4 7   6 9 0   1 8 0
// 0 0 6   7 8 5   0 0 0
// 0 5 0   0 2 0   9 0 3
// 0 3 0   9 0 0   6 0 8
// 8 0 0   0 1 0   4 7 0
// ];
// [C,L] = sudoku_candidates(X)
//
//  X  = [ 
//     1.    2.    0.    0.    3.    0.    0.    4.    0.  
//     3.    5.    0.    0.    0.    0.    0.    0.    0.  
//     4.    6.    0.    0.    0.    0.    0.    0.    0.  
//     2.    1.    0.    0.    0.    0.    0.    0.    0.  
//     5.    3.    0.    0.    0.    0.    0.    0.    0.  
//     6.    0.    0.    0.    0.    0.    0.    0.    0.  
//     7.    0.    0.    0.    0.    0.    0.    0.    0.  
//     8.    0.    0.    0.    0.    0.    0.    0.    0.  
//     9.    0.    0.    0.    0.    0.    0.    0.    0.  
// ];
// [C,L] = sudoku_candidates(X);
// b = sudoku_iscandsingle ( C , 7 , 2 );
// b = sudoku_iscandsingle ( C , 8 , 2 );
// b = sudoku_iscandsingle ( C , 9 , 2 )
//
// Authors
//   Michael Baudin, 2010

  x = C(ix,jx).entries
  single = %t
  // Search in the row
  for j = 1 : 9
    if ( j <> jx & and(C(ix,j).entries==x) ) then
      single = %f
      return
    end
  end
  // Search in the column
  for i = 1 : 9
    if ( i <> ix & and(C(i,jx).entries==x) ) then
      single = %f
      return
    end
  end
  // Search in the subblock
  for i = tri(ix)
  for j = tri(jx)
    if ( i <> ix & j <> jx & and(C(i,j).entries==x) ) then
      single = %f
      return
    end
  end
  end
endfunction
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction

