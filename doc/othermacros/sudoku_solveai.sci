function [X,iter,C,L] = sudoku_solveai ( varargin )
  //   Solves Sudoku.
  //
  // Calling Sequence
  //   Y = sudoku_solveai ( X )
  //   Y = sudoku_solveai ( X , verbose )
  //   [Y,iter,C,L] = sudoku_solveai ( ... )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  // Y: a 9-by-9 matrix. If Y contains zeros, they stand for unsolved entries. The test and(Y(:) > 0) allows to see if the puzzle is solved.
  // iter: the number of steps required by each algorithm
  // iter(1): number naked singles steps
  // iter(2): number hidden singles steps
  // iter(3): number locked candidates steps
  // iter(4): number naked pairs steps
  // iter(5): number hidden pair steps
  // iter(6): number naked triples steps
  // iter(7): number hidden triples steps
  // iter(8): number naked quad steps 
  // iter(9): number hidden quad steps 
  // C: a cell array of candidate vectors for each cell.
  // L: the number of candidates for each cell
  //
  // Description
  //   Uses a method designed by Arto Inkala:
  //   1. put candidate to its' cell
  //   2. check if any of the conflict appear
  //   3. eliminate all candidates, which cause conflict in the new situation
  //   4. repeat 2 and 3 until conflict appear of eliminations are not possible   
  //
  // Examples
  // X = [
  // 0 0 0   0 0 0   0 0 0
  // 0 0 0   0 0 0   5 3 0
  // 9 0 0   0 3 7   0 0 0
  // 0 0 2   3 0 1   0 9 6
  // 0 4 7   6 9 0   1 8 0
  // 0 0 6   7 8 5   0 0 0
  // 0 5 0   0 2 0   9 0 3
  // 0 3 0   9 0 0   6 0 8
  // 8 0 0   0 1 0   4 7 0
  // ]
  // sudoku_solveai(X)
  // sudoku_solveai(X,%t)
  //
  // Authors
  // Michael Baudin, 2010
  //
  // Bibliography
  //   "AI Escargot - The Most Difficult Sudoku Puzzle", Arto Inkala, 2007
  
  [lhs,rhs]=argn();
  if ( rhs < 1 | rhs > 3 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 1 or 3 are expected."), "sudoku_solveai", rhs);
    error(errmsg)
  end
  X = varargin(1)
  apifun_typereal ( X , "X" , 1 )
  if ( rhs < 2 ) then
    verbose = %f
  else
    verbose = varargin(2)
    apifun_typeboolean ( verbose , "verbose" , 2 )
  end
  
  apifun_typereal ( X , "X" , 1 )
  apifun_typeboolean ( verbose , "verbose" , 2 )
  
  //
  // First, solve by logic only
  [X,iter,C,L] = sudoku_solvebylogic ( X , verbose )
  //
  // Finished puzzle
  if and(X(:) > 0) then
    return
  end
  //
  // This is becoming interesting...
  //
  // Then try all candidates of all cells and see if there are
  // conflicts. Try cells with fewer candidates first
  findconflict = %f
  for d = 2 : 9
    for s = find(L==d)
      [i,j] = whatcell ( s )
      for k = 1 : L(i,j)
        // Insert this candidate and try to solve by logic
        Y = X
        v = find(C(i,j,:))
        if ( verbose ) then
          mprintf("Trying candidate %d (of %d candidates) of cell (%d,%d).\n",v,d,i,j)
        end
        Y(i,j) = v
        [Y,iterY,CY,LY,conflict] = sudoku_solvebylogic ( Y , verbose )
        if ( conflict ) then
          findconflict = %t
          if ( verbose ) then
            mprintf("At cell (%d,%d), candidate %d creates a conflict.\n",i,j,v)
          end
          break
        else
          if ( verbose ) then
            mprintf("Unable to find a conflict with candidate %d of cell (%d,%d).\n",i,j,v)
          end
        end
      end
      if ( findconflict ) then
        break
      end
    end
    if ( findconflict ) then
      break
    end
  end
endfunction

// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction

// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction

