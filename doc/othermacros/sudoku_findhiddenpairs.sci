function [X,C,L] = sudoku_findhiddenpairs ( varargin )
  //   Find triples.
  //
  // Calling Sequence
  //   [X,C,L] = sudoku_findhiddenpairs ( X , C , L )
  //   [X,C,L] = sudoku_findhiddenpairs ( X , C , L , stopatfirst )
  //   [X,C,L] = sudoku_findhiddenpairs ( X , C , L , stopatfirst , verbose )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // stopatfirst: if %t, then stop after one or more candidates have been removed. (default = %t)
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  //
  // Description
  //   Search for hidden pairs.
  //   The algorithm also finds hidden singles, if any.
  //   This algorithm is superseded by findhiddensubset.
  //
  // Examples
  // X = [
  // 4 0 0   3 9 0   0 0 2
  // 2 6 0   0 5 8   3 9 0
  // 5 9 3   6 0 0   1 8 0
  // ..
  // 1 0 0   8 6 0   0 0 9
  // 6 0 5   9 0 0   2 0 0 
  // 0 3 9   2 4 5   0 1 6
  // ..
  // 0 5 6   0 0 9   0 2 0
  // 0 1 4   7 0 0   9 0 5
  // 9 0 0   5 3 0   0 0 0
  // ];
  // [C,L] = sudoku_candidates(X);
  // sudoku_findhiddenpairs ( X , C , L , %f , %t );
  //
  // Authors
  //   Michael Baudin, 2010
  //
  // Bibliography
  //   http://www.palmsudoku.com/pages/techniques-7.php
  //   http://www.sadmansoftware.com/sudoku/hiddensubset.htm
  
  [lhs,rhs]=argn();
  if ( rhs < 3 | rhs > 5 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 3 to 5 are expected."), "sudoku_findhiddensingles", rhs);
    error(errmsg)
  end
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  if ( rhs < 4 ) then
    stopatfirst = %t
  else
    stopatfirst = varargin(4)
  end
  if ( rhs < 5 ) then
    verbose = %f
  else
    verbose = varargin(5)
  end
  
  apifun_typereal ( X , "X" , 1 )
  apifun_typecell ( C , "C" , 2 )
  apifun_typereal ( L , "L" , 3 )
  apifun_typeboolean ( stopatfirst , "stopatfirst" , 4 );
  apifun_typeboolean ( verbose , "verbose" , 5 );
  
  before = sum(L)

  // Search for hidden pairs in rows
  for i = 1 : 9
    // Search for hidden pairs in row i.
    // Assemble all candidates of the row.
    carray  = sudoku_candidatesmerge ( X , C , L , 1 , i , [] , [2 9] )
    if ( carray == [] ) then
      // There are no possible hidden pair in this row, go on to the next
      continue
    end
    // Keep only unique candidates
    n = size(carray,"*")
    if ( n <= 2 ) then
      // There are only 1 or 2 unique candidates in this row:
      // there cannot exist a hidden pair here, try the next row.
      continue
    end
    // Get all possible combinations of 2 elements among 
    // these candidates
    cmap = combine ( carray , 2 )
    nk = size(cmap,"r")
    // Search for each combinations in the row i
    for m = 1 : nk
      S = cmap(m,:)
      icols = []
      for c = 1 : 9
        if ( X(i,c) == 0 ) then
          if ( isAnyContained ( S , C(i,c).entries' ) ) then
            // This might be a hidden pair: register it
            icols($+1) = c
            if ( size(icols,"*") > 2 ) then
              // This pair is represented more than twice in the row.
              break
            end
          end
        end
      end
      if ( size(icols,"*") <> 2 ) then
        // This hidden pair is not represented in the row:
        // try next hidden pair.
        continue
      end
      // This is a hidden pair: remove the other candidates 
      // from these two cells.
      firstverb = %t
      for k = 1 : 2
        // Simplify the cell #k
        c = icols(k)
        // Remove each candidate which is different from the pair
        // We cannot set directly the pair as a candidate for this cell,
        // since it may contain only one of the two (locked candidate may not have
        // been removed).
        for v = C(i,c).entries'
          if ( and(S<>v) ) then
            [ C , L ] = sudoku_candidateremove ( C , L , 4 , i , c , v )
            if ( verbose ) then
              if ( firstverb ) then
                mprintf("Found hidden pair {%s} in row %d at (%d,%d), (%d,%d)\n" , ..
                  strcat(string(S),","),i,i,icols(1),i,icols(2))
                firstverb = %f
              end
              mprintf("Removed other candidates %d in hidden pair at (%d,%d) (remaining %d candidates)\n",v,i,c,sum(L))
            end
          end
        end
      end
      if ( stopatfirst & sum(L) <> before ) then
        return
      end
    end
  end
  
  // Search for hidden pairs in columns
  for j = 1 : 9
    // Search for hidden pairs in column j.
    // Assemble all candidates of the column.
    carray  = sudoku_candidatesmerge ( X , C , L , 2 , [] , j , [2 9] )
    if ( carray == [] ) then
      // There are no possible hidden pair in this column, go on to the next
      continue
    end
    n = size(carray,"*")
    if ( n <= 2 ) then
      // There are only 1 or 2 unique candidates in this column:
      // there cannot exist a hidden pair here, try the next column.
      continue
    end
    // Get all possible combinations of 2 elements among 
    // these candidates.
    cmap = combine ( carray , 2 )
    nk = size(cmap,"r")
    // Search for each combinations in the column i
    for m = 1 : nk
      S = cmap(m,:)
      irows = []
      for r = 1 : 9
        if ( X(r,j) == 0 ) then
          if ( isAnyContained ( S , C(r,j).entries' ) ) then
            // This might be a hidden pair: register it
            irows($+1) = r
            if ( size(irows,"*") > 2 ) then
              // This pair is represented more than twice in the column.
              break
            end
          end
        end
      end
      if ( size(irows,"*") <> 2 ) then
        // This hidden pair is not represented in the column:
        // try next hidden pair.
        continue
      end
      // This is a hidden pair: remove the other candidates 
      // from these two cells.
      firstverb = %t
      for k = 1 : 2
        // Simplify the cell #k
        r = irows(k)
        // Remove each candidate which is different from the pair
        // We cannot set directly the pair as a candidate for this cell,
        // since it may contain only one of the two (locked candidate may not have
        // been removed).
        for v = C(r,j).entries'
          if ( and(S<>v) ) then
            [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , j , v )
            if ( verbose ) then
              if ( firstverb ) then
                mprintf("Found hidden pair {%s} in column %d at (%d,%d), (%d,%d)\n" , ..
                  strcat(string(S),","),j,irows(1),j,irows(2),j)
                firstverb = %f
              end
              mprintf("Removed other candidates %d in hidden pair at (%d,%d) (remaining %d candidates)\n",v,r,j,sum(L))
            end
          end
        end
      end
      if ( stopatfirst & sum(L) <> before ) then
        return
      end
    end
  end
  // Search for hidden pairs in blocks
  for i = [1 4 7]
    for j = [1 4 7]
      // Search for hidden pairs in block (i,j)
      // Assemble all candidates of the block
      carray  = sudoku_candidatesmerge ( X , C , L , 3 , i , j , [2 9] )
      if ( carray == [] ) then
        // There are no possible hidden pair in this block, go on to the next
        continue
      end
      // Keep only unique candidates
      n = size(carray,"*")
      if ( n <= 2 ) then
        // There are only 1 or 2 unique candidates in this block:
        // there cannot exist a hidden pair here, try the next block.
        continue
      end
      // Get all possible combinations of 2 elements among 
      // these candidates
      cmap = combine ( carray , 2 )
      nk = size(cmap,"r")
      // Search for each combinations in the block (i,j)
      for m = 1 : nk
        S = cmap(m,:)
        irows = []
        icols = []
        for r = tri(i)
          for c = tri(j)
            if ( X(r,c) == 0 ) then
              if ( isAnyContained ( S , C(r,c).entries' ) ) then
                // This might be a hidden pair: register it
                irows($+1) = r
                icols($+1) = c
                if ( size(irows,"*") > 2 ) then
                  // This pair is represented more than twice in the block.
                  break
                end
              end
            end
          end
          if ( size(irows,"*") > 2 ) then
            // This pair is represented more than twice in the block.
            break
          end
        end
        if ( size(irows,"*") <> 2 ) then
          // This pair is not represented in the block
          // Try next pair
          continue
        end
        // This is a hidden pair: remove the other candidates 
        // from these two cells.
        firstverb = %t
        for k = 1 : 2
          // Simplify the cell #k
          r = irows(k)
          c = icols(k)
          // Remove each candidate which is different from the pair
          // We cannot set directly the pair as a candidate for this cell,
          // since it may contain only one of the two (locked candidate may not have
          // been removed).
          for v = C(r,c).entries'
            if ( and(S<>v) ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , v )
              if ( verbose ) then
                if ( firstverb ) then
                  mprintf("Found hidden pair {%s} in block (%d,%d) at (%d,%d), (%d,%d)\n" , ..
                    strcat(string(S),","),i,j,irows(1),icols(1),irows(2),icols(2))
                end
                mprintf("Removed other candidates %d in hidden pair at (%d,%d) (remaining %d candidates)\n",v,r,c,sum(L))
              end
            end
          end
        end
        if ( stopatfirst & sum(L) <> before ) then
          return
        end
      end
    end
  end
  
endfunction

// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type cell
function apifun_typecell ( var , varname , ivar )
  if ( typeof ( var ) <> "ce" ) then
    errmsg = msprintf(gettext("%s: Expected cell variable for variable %s at input #%d, but got %s instead."),"apifun_typecell", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction


// Returns the (i,j) index given the global index s in a block
// Note: in Scilab, entries are numbered column bX column.
//  whatcellinblock ( 1 ) = (1,1)
//  whatcellinblock ( 2 ) = (2,1)
//  whatcellinblock ( 3 ) = (3,1)
//  whatcellinblock ( 4 ) = (1,2)
//  ...
//  whatcellinblock ( 9 ) = (9,1)
function [i,j] = whatcellinblock ( s )
  i = modulo(s-1,3) + 1
  j = (s-i)/3 + 1
endfunction

// combine --
// Returns all combinations of k values from the row vector x as a row-by-row array
// with (n,k) rows where (n,k) is the binomial coefficient and n is the 
// number of values in x. 
//
// References
// http://home.att.net/~srschmitt/script_combinations.html
// Kenneth H. Rosen, Discrete Mathematics and Its Applications, 2nd edition (NY: McGraw-Hill, 1991), pp. 284-286.
//
// Example
//   combine ( [17 32 48 53] , 3 ) :
//       17.    32.    48.  
//       17.    32.    53.  
//       17.    48.    53.  
//       32.    48.    53.  
//
function cmap = combine ( x , k )
  n = size(x,"*")
  if ( n < k ) then
    error ( msprintf ( "%s: The number of values in x is n = %d which is lower than k = %d\n", n,k))
  end
  c = combinations ( n , k )
  cmap = zeros(c,k)
  a = 1:k
  cmap(1,:) = x(a)
  for m = 2 : c
    i = k
    while ( a(i) == n - k + i )
      i = i - 1
    end
    a(i) = a(i) + 1
    for j = i+1 : k
      a(j) = a(i) + j - i
    end
    cmap(m,:) = x(a)
  end
endfunction
//
// whatcell --
// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//
// Example
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction

// isAnyContained --
// Returns %t if any item in the row vector x is in the 
// row set S.
//
// Example 
//   isAnyContained ( [3 50 51] , 1:2:11 ) is true
//   isAnyContained ( [49 50 51] , 1:2:11 ) is false
function bool = isAnyContained ( x , S )
  bool = %f
  for v = x
    if ( find(S==v) <> [] ) then
      bool = %t
      break
    end
  end
endfunction

// areAllContained --
// Returns %t if all the items in the row vector x are in the 
// row set S.
//
// Example 
//   areAllContained ( [3 5 7] , 1:2:11 ) is true
//   areAllContained ( [2 4 7] , 2:2:10 ) is false
function bool = areAllContained ( x , S )
  bool = %t
  for v = x
    if ( find(S==v) == [] ) then
      bool = %f
      break
    end
  end
endfunction

// combinations --
//   Returns the number of combinations of j objects chosen from n objects.
//   If the input where integers, returns also an integer.
function c = combinations ( n , j )
  c = exp(gammaln(n+1)-gammaln(j+1)-gammaln(n-j+1));
  if ( and(round(n)==n) & and(round(j)==j) ) then
    c = round ( c )
  end
endfunction
function [X,C,L] = sudoku_findhiddenpairs2 ( varargin )


  [lhs,rhs]=argn();
  if ( rhs < 3 | rhs > 4 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 3 or 4 are expected."), "sudoku_findhiddensingles", rhs);
    error(errmsg)
  end
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  if ( rhs < 4 ) then
    verbose = %f
  else
    verbose = varargin(4)
  end
  
  apifun_typereal ( X , "X" , 1 )
  apifun_typecell ( C , "C" , 2 )
  apifun_typereal ( L , "L" , 3 )
  apifun_typeboolean ( verbose , "verbose" , 4 );
  
  // Search for hidden pairs in rows
  for i = 1 : 9
    // Search for hidden pairs in row i.
    // Assemble all candidates of the row.
    carray  = sudoku_candidatesmerge ( X , C , L , 1 , i , [] , [2 9] )
    if ( carray == [] ) then
      // There are no possible hidden pair in this row, go on to the next
      continue
    end
    // Keep only unique candidates
    n = size(carray,"*")
    if ( n <= 2 ) then
      // There are only 1 or 2 unique candidates in this row:
      // there cannot exist a hidden pair here, try the next row.
      continue
    end
    // Get all possible combinations of 2 elements among 
    // these candidates
    cmap = combine ( carray , 2 )
    nk = size(cmap,"r")
    // Search for each combinations in the row i
    for m = 1 : nk
      S = cmap(m,:)
      icols = []
      for c = 1 : 9
        if ( X(i,c) == 0 & L(i,c) >= 2 ) then
          if ( areAllContained ( S , C(i,c).entries' ) ) then
            // This might be a hidden pair: register it
            icols($+1) = c
            if ( size(icols,"*") > 2 ) then
              // This pair is represented more than twice in the row:
              // try next hidden pair.
              break
            end
          end
        end
      end
      if ( size(icols,"*") <> 2 ) then
        // This hidden pair is not represented in the row:
        // try next hidden pair.
        continue
      end
      // There is a hidden pair in two cells:
      // check that these two candidates are not in the other cells.
      notpair = %f
      for c = 1 : 9
        if ( X(i,c) == 0 & and(icols<>c) & isAnyContained(S,C(i,c).entries') ) then
          notpair = %t
          break
        end
      end
      if ( notpair ) then
        // The pair S is hidden in two cells, but other cells in the row
        // are also containing one of these two candidates. This is not a 
        // hidden pair: try next.
        continue
      end
      if ( verbose ) then
        mprintf("Found hidden pair {%s} in row %d at (%d,%d), (%d,%d)\n" , ..
          strcat(string(S),","),i,i,icols(1),i,icols(2))
      end
      // This is a hidden pair: remove the other candidates 
      // from these two cells.
      for k = 1 : 2
        // Simplify the cell #k
        c = icols(k)
        // Remove each candidate which is different from the pair
        // We cannot set directly the pair as a candidate for this cell,
        // since it may contain only one of the two (locked candidate may not have
        // been removed).
        for v = C(i,c).entries'
          if ( and(S<>v) ) then
            [ C , L ] = sudoku_candidateremove ( C , L , 4 , i , c , v )
            if ( verbose ) then
              mprintf("Removed other candidates %d in hidden pair at (%d,%d) (remaining %d candidates)\n",v,i,c,sum(L))
            end
          end
        end
      end
    end
  end
  
  // Search for hidden pairs in columns
  for j = 1 : 9
    // Search for hidden pairs in column j.
    // Assemble all candidates of the column.
    carray  = sudoku_candidatesmerge ( X , C , L , 2 , [] , j , [2 9] )
    if ( carray == [] ) then
      // There are no possible hidden pair in this column, go on to the next
      continue
    end
    n = size(carray,"*")
    if ( n <= 2 ) then
      // There are only 1 or 2 unique candidates in this column:
      // there cannot exist a hidden pair here, try the next column.
      continue
    end
    // Get all possible combinations of 2 elements among 
    // these candidates.
    cmap = combine ( carray , 2 )
    nk = size(cmap,"r")
    // Search for each combinations in the column i
    for m = 1 : nk
      S = cmap(m,:)
      irows = []
      for r = 1 : 9
        if ( X(r,j) == 0 & L(r,j) >= 2 ) then
          if ( areAllContained ( S , C(r,j).entries' ) ) then
            // This might be a hidden pair: register it
            irows($+1) = r
          end
        end
      end
      if ( size(irows,"*") <> 2 ) then
        // This hidden pair is not represented in the column:
        // try next hidden pair.
        continue
      end
      // There is a hidden pair in two cells:
      // check that these two candidates are not in the other cells.
      notpair = %f
      for r = 1 : 9
        if ( X(r,j) == 0 & and(irows<>r) & isAnyContained(S,C(r,j).entries') ) then
          notpair = %t
          break
        end
      end
      if ( notpair ) then
        // The pair S is hidden in two cells, but other cells in the column 
        // are also containing one of these two candidates. This is not a 
        // hidden pair: try next.
        continue
      end
      if ( verbose ) then
        mprintf("Found hidden pair {%s} in column %d at (%d,%d), (%d,%d)\n" , ..
          strcat(string(S),","),j,irows(1),j,irows(2),j)
      end
      // This is a hidden pair: remove the other candidates 
      // from these two cells.
      for k = 1 : 2
        // Simplify the cell #k
        r = irows(k)
        // Remove each candidate which is different from the pair
        // We cannot set directly the pair as a candidate for this cell,
        // since it may contain only one of the two (locked candidate may not have
        // been removed).
        for v = C(r,j).entries'
          if ( and(S<>v) ) then
            [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , j , v )
            if ( verbose ) then
              mprintf("Removed other candidates %d in hidden pair at (%d,%d) (remaining %d candidates)\n",v,r,j,sum(L))
            end
          end
        end
      end
    end
  end
  // Search for hidden pairs in blocks
  for i = [1 4 7]
    for j = [1 4 7]
      // Search for hidden pairs in block (i,j)
      // Assemble all candidates of the block
      carray  = sudoku_candidatesmerge ( X , C , L , 3 , i , j , [2 9] )
      if ( carray == [] ) then
        // There are no possible hidden pair in this block, go on to the next
        continue
      end
      // Keep only unique candidates
      n = size(carray,"*")
      if ( n <= 2 ) then
        // There are only 1 or 2 unique candidates in this block:
        // there cannot exist a hidden pair here, try the next block.
        continue
      end
      // Get all possible combinations of 2 elements among 
      // these candidates
      cmap = combine ( carray , 2 )
      nk = size(cmap,"r")
      // Search for each combinations in the block (i,j)
      for m = 1 : nk
        S = cmap(m,:)
        irows = []
        icols = []
        for r = tri(i)
          for c = tri(j)
            if ( X(r,c) == 0 & L(r,c) >= 2 ) then
              if ( areAllContained ( S , C(r,c).entries' ) ) then
                // This might be a hidden pair: register it
                irows($+1) = r
                icols($+1) = c
              end
            end
          end
        end
        if ( size(irows,"*") <> 2 ) then
          // This pair is not represented in the block
          // Try next pair
          continue
        end
        // There is a hidden pair in two cells:
        // check that these two candidates are not in the other cells.
        notpair = %f
        for r = tri(i)
          for c = tri(j)
            if ( X(r,c) == 0 & and(~(r == irows & c == icols)) & isAnyContained(S,C(r,j).entries') ) then
              notpair = %t
              break
            end
          end
        end
        if ( notpair ) then
          // The pair S is hidden in two cells, but other cells in the column 
          // are also containing one of these two candidates. This is not a 
          // hidden pair: try next.
          continue
        end
        if ( verbose ) then
          mprintf("Found hidden pair {%s} in block (%d,%d) at (%d,%d), (%d,%d)\n" , ..
            strcat(string(S),","),i,j,irows(1),icols(1),irows(2),icols(2))
        end
        // This is a hidden pair: remove the other candidates 
        // from these two cells.
        //
        for k = 1 : 2
          // Simplify the cell #k
          r = irows(k)
          c = icols(k)
          // Remove each candidate which is different from the pair
          // We cannot set directly the pair as a candidate for this cell,
          // since it may contain only one of the two (locked candidate may not have
          // been removed).
          for v = C(r,c).entries'
            if ( and(S<>v) ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , c , v )
              if ( verbose ) then
                mprintf("Removed other candidates %d in hidden pair at (%d,%d) (remaining %d candidates)\n",v,r,c,sum(L))
              end
            end
          end
        end
      end
    end
  end
  
endfunction

