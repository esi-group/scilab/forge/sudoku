function [ X , C , L ] = sudoku_findnakedpairs ( varargin )
  //   Search for pairs.
  //
  // Calling Sequence
  //   [ X , C , L ] = sudoku_findnakedpairs ( X , C , L )
  //   [ X , C , L ] = sudoku_findnakedpairs ( X , C , L , stopatfirst )
  //   [ X , C , L ] = sudoku_findnakedpairs ( X , C , L , stopatfirst , verbose )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // C: a 9 x 9 cell of candidates
  // L: the 9 x 9 matrix of number candidates
  // stopatfirst: if %t, then stop when one or more candidates have been removed. (default = %t)
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  //
  // Description
  //   Search for naked pais of candidates. 
  //   Superseded by findnakedsubset.
  //
  //   If one naked pair is identified on one row,
  //   column or block, remove them from other candidates in the row, column or block.
  //   This method is in "Advanced Techniques" 
  //   in "Programming Sudoku", Chapter 5.
  //   Pairs are often called twins.
  //
  //   I modified the algorithm of Lee so that each, when we search
  //   for pairs in blocks, each pair is visited
  //   only once. For example, in the original algorithm, 
  //   the pair (6,9) and (6,8) was visited once (leading to improvement),
  //   and second, the pair (6,8) and (6,9) is visited a second 
  //   time (which is useless).
  //   This saving done with the additionnal condition
  //   location(r,c) < location(rr,cc) which is true when the 
  //   location of the cell (r,c) in greater to the location of the 
  //   cell (rr,cc), relatively to the block.
  //   In this case, location(6,8)=8 and location(6,9)=9, so that the 
  //   only considered pair is (6,8) - (6,9)
  //
  // Examples
  // 
  // X = [
  // 0 2 0   0 3 0   0 4 0 
  // 6 0 0   0 0 0   0 0 3 
  // 0 0 4   0 0 0   5 0 0 
  // ..
  // 0 0 0   8 0 6   0 0 0 
  // 8 0 0   0 1 0   0 0 6 
  // 0 0 0   7 0 5   0 0 0 
  // ..
  // 0 0 7   0 0 0   6 0 0 
  // 4 0 0   0 0 0   0 0 8 
  // 0 3 0   0 4 0   0 2 0 
  // ];
  // [C,L] = sudoku_candidates(X);
  // X = sudoku_findnakedpairs ( X , C , L , %f , %t );
  //
  // Authors
  //   Michael Baudin, 2010
  //
  // Bibliography
  //   Programming Sudoku, Wei-Meng Lee, 2006
  
  [lhs,rhs]=argn();
  if ( rhs < 3 | rhs > 5 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 3 to 5 are expected."), "sudoku_findnakedpairs", rhs);
    error(errmsg)
  end
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  if ( rhs < 4 ) then
    stopatfirst = %t
  else
    stopatfirst = varargin(4)
  end
  if ( rhs < 5 ) then
    verbose = %f
  else
    verbose = varargin(5)
  end
  
  apifun_typereal ( X , "X" , 1 )
  apifun_typecell ( C , "C" , 2 )
  apifun_typereal ( L , "L" , 3 )
  apifun_typeboolean ( stopatfirst , "stopatfirst" , 4 )
  apifun_typeboolean ( verbose , "verbose" , 5 );
  
  before = sum(L)

  // Look for naked Pairs in rows
  // For each row, check each column in the row
  for s = find ( X == 0 & L == 2 )
    [r,c] = whatcell ( s )
    // Scan the columns in this row
    for cc = c+1:9
      if ( and(C(r,c).entries==C(r,cc).entries) ) then
        // pair found 
        // Remove twins from all the other possible values in the column
        first = %t
        for ccc = 1:9
          if ( X(r,ccc)==0 & ccc <> c & ccc <> cc ) then
            // Remove first twin from candidates
            v = C(r,c).entries(1)
            if ( find(C(r,ccc).entries==v) <> [] ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , ccc , v )
              if ( verbose ) then
                if ( first ) then
                  mprintf("Found naked pair in row at (%d,%d) and (%d,%d)\n",r,c,r,cc)
                  first = %f
                end
                mprintf("Removed row candidate %d from (%d,%d) (remaining %d candidates)\n",v,r,ccc,sum(L))
              end
            end
            // Remove second twin from candidates
            v = C(r,c).entries(2)
            if ( find(C(r,ccc).entries==v) <> [] ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , ccc , v )
              if ( verbose ) then
                if ( first ) then
                  mprintf("Found naked pair in row at (%d,%d) and (%d,%d)\n",r,c,r,cc)
                  first = %f
                end
                mprintf("Removed row candidate %d from (%d,%d) (remaining %d candidates)\n",v,r,ccc,sum(L))
              end
            end
          end
        end
        if ( stopatfirst & sum(L) <> before ) then
          return
        end
      end
    end
  end
  
  // Look for Pairs in columns
  // For each column, check each row in the column
  for s = find ( X == 0 & L == 2 )
    [r,c] = whatcell ( s )
    // Scan the rows in this column
    for rr = r+1:9
      if ( and(C(r,c).entries==C(rr,c).entries) ) then
        // pair found 
        // Remove twins from all the other possible values in the row
        first = %t
        for rrr = 1:9
          if ( X(rrr,c)==0 & rrr <> r & rrr <> rr ) then
            // Remove first twin from candidates
            v = C(r,c).entries(1)
            if ( find(C(rrr,c).entries==v) <> [] ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , rrr , c , v )
              if ( verbose ) then
                if ( first ) then
                  mprintf("Found naked pair in column at (%d,%d) and (%d,%d)\n",r,c,rr,c)
                  first = %f
                end
                mprintf("Removed column candidate %d from (%d,%d) (remaining %d candidates)\n",v,rrr,c,sum(L))
              end
            end
            // Remove second twin from candidates
            v = C(r,c).entries(2)
            if ( find(C(rrr,c).entries==v) <> [] ) then
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , rrr , c , v )
              if ( verbose ) then
                if ( first ) then
                  mprintf("Found naked pair in column at (%d,%d) and (%d,%d)\n",r,c,rr,c)
                  first = %f
                end
                mprintf("Removed column candidate %d from (%d,%d) (remaining %d candidates)\n",v,rrr,c,sum(L))
              end
            end
          end
        end
        if ( stopatfirst & sum(L) <> before ) then
          return
        end
      end
    end
  end

  // Look for Pairs in Blocks
  for s = find ( X == 0 & L == 2 )
    [r,c] = whatcell ( s )
    // Scan the block that the current cell is in
    startR = tri(r)
    startC = tri(c)
    for rr = startR
      for cc = startC
        if ( ~(cc == c & rr == r) & location(r,c) < location(rr,cc) & and(C(r,c).entries==C(rr,cc).entries) ) then
          // pair found : remove the pair from all the other possible
          // values in the block
          first = %t
          for rrr = startR
            for ccc = startC
              if ( X(rrr,ccc)==0 & or(C(rrr,ccc).entries <> C(r,c).entries) ) then
                // Remove first twin from candidates
                v = C(r,c).entries(1)
                if ( find(C(rrr,ccc).entries==v) <> [] ) then
                  [ C , L ] = sudoku_candidateremove ( C , L , 4 , rrr , ccc , v )
                  if ( verbose ) then
                    if ( first ) then
                      mprintf("Found naked pair in block at (%d,%d) and (%d,%d)\n",r,c,rr,cc)
                      first = %f
                    end
                    mprintf("Removed block candidate %d from (%d,%d) (remaining %d candidates)\n",v,rrr,ccc,sum(L))
                  end
                end
                // Remove second twin from candidates
                v = C(r,c).entries(2)
                if ( find(C(rrr,ccc).entries==v) <> [] ) then
                  [ C , L ] = sudoku_candidateremove ( C , L , 4 , rrr , ccc , v )
                  if ( verbose ) then
                    if ( first ) then
                      mprintf("Found naked pair in block at (%d,%d) and (%d,%d)\n",r,c,rr,cc)
                      first = %f
                    end
                    mprintf("Removed block candidate %d from (%d,%d) (remaining %d candidates)\n",v,rrr,ccc,sum(L))
                  end
                end
              end
            end
          end
          if ( stopatfirst & sum(L) <> before ) then
            return
          end
        end
      end
    end
  end
  
  
endfunction

// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type cell
function apifun_typecell ( var , varname , ivar )
  if ( typeof ( var ) <> "ce" ) then
    errmsg = msprintf(gettext("%s: Expected cell variable for variable %s at input #%d, but got %s instead."),"apifun_typecell", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction

//
// tri --
//   tri(1) = [1 2 3], tri(2) = [1 2 3], tri(3) = [1 2 3]
//   tri(4) = [4 5 6], tri(5) = [4 5 6], tri(6) = [4 5 6]
//   tri(7) = [7 8 9], tri(8) = [7 8 9], tri(9) = [7 8 9]
function y = tri ( k )
  y = 3*ceil(k/3-1) + (1:3)
endfunction
// Returns the location of a cell inside a block.
// The location is row first, column second
// location(7,7) = 1, location(7,8) = 2, location(7,9) = 3
// location(8,7) = 4, location(8,8) = 5, location(8,9) = 6
// location(9,7) = 7, location(9,8) = 8, location(9,9) = 9
function l = location ( i , j )
  l = pmodulo(i-1,3)*3 +1 + pmodulo(j-1,3)
endfunction
// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction

function [ X , C , L ] = sudoku_findnakedpairs2 ( varargin )

  [lhs,rhs]=argn();
  if ( rhs < 3 | rhs > 4 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 3 or 4 are expected."), "sudoku_findnakedpairs", rhs);
    error(errmsg)
  end
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  if ( rhs < 4 ) then
    verbose = %f
  else
    verbose = varargin(4)
  end

  changes = %f
  // Look for Pairs in Blocks
  for r = 1 : 9
  for c = 1 : 9
    if ( X(r,c) == 0 & L(r,c) == 2) then
      // Scan the block that the current cell is in
      startR = tri(r)
      startC = tri(c)
      for rr = startR
      for cc = startC
        if ( ~(cc == c & rr == r) & location(r,c) < location(rr,cc) & and(C(r,c).entries==C(rr,cc).entries) ) then
          // pair found : remove the pair from all the other possible
          // values in the block
          if ( verbose ) then
            mprintf("Found pair in block at (%d,%d) and (%d,%d)\n",r,c,rr,cc)
          end
          for rrr = startR
          for ccc = startC
            if ( X(rrr,ccc)==0 & or(C(rrr,ccc).entries <> C(r,c).entries) ) then
              // Save a copy of the original candidates
              orig = C(rrr,ccc).entries
              // Remove first twin from candidates
              v = C(r,c).entries(1)
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , rrr , ccc , v )
              // Remove second twin from candidates
              v = C(r,c).entries(2)
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , rrr , ccc , v )
              if ( or(orig <> C(rrr,ccc).entries) ) then
                changes = %t
              end
              // If candidates reduces to empty, this is an unconsistent sudoku
              if ( C(rrr,ccc).entries == [] ) then
                return
              end
              // If left with 1 candidate, confirm it
              if ( L(rrr,ccc) == 1 ) then
                X(rrr,ccc) = C(rrr,ccc).entries(1)
                C(rrr,ccc).entries = []
                L(rrr,ccc) = 0
                if ( verbose ) then
                  mprintf("Confirmed (%d,%d) as %d\n",rrr,ccc,X(rrr,ccc))
                end
              end
            end
          end
          end
        end
      end
      end
    end
  end
  end

  // Look for Pairs in rows
  // For each row, check each column in the row
  for r = 1 : 9
  for c = 1 : 9
    if ( X(r,c) == 0 & L(r,c) == 2) then
      // Scan the columns in this row
      for cc = c+1:9
        if ( and(C(r,c).entries==C(r,cc).entries) ) then
          // pair found 
          if ( verbose ) then
            mprintf("Found pair in row at (%d,%d) and (%d,%d)\n",r,c,r,cc)
          end
          // Remove twins from all the other possible values in the column
          for ccc = 1:9
            if ( X(r,ccc)==0 & ccc <> c & ccc <> cc ) then
              // Save a copy of the original candidates
              orig = C(r,ccc).entries
              // Remove first twin from candidates
              v = C(r,c).entries(1)
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , ccc , v )
              // Remove second twin from candidates
              v = C(r,c).entries(2)
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , r , ccc , v )
              if ( or(orig <> C(r,ccc).entries) ) then
                changes = %t
              end
              // If candidates reduces to empty, this is an unconsistent sudoku
              if ( C(r,ccc).entries == [] ) then
                return
              end
              // If left with 1 candidate, confirm it
              if ( L(r,ccc) == 1 ) then
                X(r,ccc) = C(r,ccc).entries(1)
                C(r,ccc).entries = []
                L(r,ccc) = 0
                if ( verbose ) then
                  mprintf("Confirmed (%d,%d) as %d\n",r,ccc,X(r,ccc))
                end
              end
            end
          end
        end
      end
    end
  end
  end

  // Look for Pairs in columns
  // For each column, check each row in the column
  for c = 1 : 9
  for r = 1 : 9
    if ( X(r,c) == 0 & L(r,c) == 2) then
      // Scan the rows in this column
      for rr = r+1:9
        if ( and(C(r,c).entries==C(rr,c).entries) ) then
          // pair found 
          if ( verbose ) then
            mprintf("Found pair in column at (%d,%d) and (%d,%d)\n",r,c,rr,c)
          end
          // Remove twins from all the other possible values in the row
          for rrr = 1:9
            if ( X(rrr,c)==0 & rrr <> r & rrr <> rr ) then
              // Save a copy of the original candidates
              orig = C(rrr,c).entries
              // Remove first twin from candidates
              v = C(r,c).entries(1)
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , rrr , c , v )
              // Remove second twin from candidates
              v = C(r,c).entries(2)
              [ C , L ] = sudoku_candidateremove ( C , L , 4 , rrr , c , v )
              if ( or(orig <> C(rrr,c).entries) ) then
                changes = %t
              end
              // If candidates reduces to empty, this is an unconsistent sudoku
              if ( C(rrr,c).entries == [] ) then
                return
              end
              // If left with 1 candidate, confirm it
              if ( L(rrr,c) == 1 ) then
                X(rrr,c) = C(rrr,c).entries(1)
                C(rrr,c).entries = []
                L(rrr,c) = 0
                if ( verbose ) then
                  mprintf("Confirmed (%d,%d) as %d\n",rrr,c,X(rrr,c))
                end
              end
            end
          end
        end
      end
    end
  end
  end

endfunction

