function [puzzle , solution] = sudoku_generate ( )
  // Generates a sudoku 9x9 puzzle.
  //
  // Calling Sequence
  //   [puzzle , solution] = sudoku_generate ( s )
  //
  // Parameters
  // puzzle: the 9x9 puzzle matrix, with zeros for unknown entries
  // solution: the 9x9 solution matrix without any zero
  // 
  // Description
  //   Generates a random sudoku. This algorithm is very slow (typically 120 seconds).
  //
  // Examples
  // [puzzle , solution] = sudoku_generate ()
  //
  // Authors
  // John R. Birge, March 14, 2006
  // Michael Baudin, Scilab port and comments, 2010
  
  puzzle=zeros(9,9)
  solution=zeros(9,9)
  //poss will contain the possible value for each element
  poss=ones(9,9,9)
  // count is used to prevent getting stuck by early choices
  count=1 
  last=[0 0 0]
  // last and ps2 are used to backtrack if infeasibility found
  ps2=[] 
  while ( min(solution)<1 & count<100 )
    
    // the first part finds all values that are implied by the solution so
    // far
    // if change is "1" then some element was updated 
    change=1 
    while change==1
      change=0
      for il=1:9
        for kl=1:9
          if ( mtlb_sum(poss(il,:,kl))==1 ) then
            [ml jl]=max(matrix(poss(il,:,kl),9,1))
            if solution(il,jl)==0  then
              solution(il,jl)=kl
              change=1
              for ii=1:9
                if ii~=il
                  poss(ii,jl,kl)=0
                end
              end
              for kk=1:9
                if kk~=kl
                  poss(il,jl,kk)=0
                end
              end
              top=3*floor((il-1)/3)+1
              left=3*floor((jl-1)/3)+1
              for ii=top:top+2
                for jj=left:left+2
                  if ( ii~=il | jj~=jl ) then
                    poss(ii,jj,kl)=0
                  end
                end
              end
            end
          end
        end
        for jl=1:9
          if ( mtlb_sum(poss(il,jl,1:9))==1 ) then
            [ml kl]=max(matrix(poss(il,jl,1:9),9,1))
            if ( solution(il,jl)==0 ) then
              solution(il,jl)=kl
              change=1
              for ii=1:9
                if ( ii~=il ) then
                  poss(ii,jl,kl)=0
                end
              end
              for jj=1:9
                if ( jj~=jl ) then
                  poss(il,jj,kl)=0
                end
              end
              top=3*floor((il-1)/3)+1
              left=3*floor((jl-1)/3)+1
              for ii=top:top+2
                for jj=left:left+2
                  if ( (ii~=il)|(jj~=jl) ) then
                    poss(ii,jj,kl)=0
                  end
                end
              end
            end
          end
          for kl=1:9
            if ( mtlb_sum(poss(:,jl,kl))==1 )  then
              [ml il]=max(matrix(poss(:,jl,kl),9,1))
              if ( solution(il,jl)==0 ) then
                solution(il,jl)=kl
                change=1
                for jj=1:9
                  if ( jj~=jl ) then
                    poss(il,jj,kl)=0
                  end
                end
                for kk=1:9
                  if ( kk~=kl ) then
                    poss(il,jl,kk)=0
                  end
                end
                top=3*floor((il-1)/3)+1
                left=3*floor((jl-1)/3)+1
                for ii=top:top+2
                  for jj=left:left+2
                    if ( ii~=il | jj~=jl ) then
                      poss(ii,jj,kl)=0
                    end
                  end
                end
              end
            end
          end
        end
      end
      for kl=1:9
        for tp=1:3
          for lt=1:3
            it=3*(tp-1)+1
            jt=3*(lt-1)+1
            if ( mtlb_sum(mtlb_sum(poss(it:it+2,jt:jt+2,kl)))==1 )  then
              for il=it:it+2
                for jl=jt:jt+2
                  if ( poss(il,jl,kl)==1 & solution(il,jl)==0 )  then
                    change=1
                    solution(il,jl)=kl
                    ils=il
                    jls=jl
                    for ii=1:9
                      if ( ii~=ils )  then
                        poss(ii,jls,kl)=0
                      end
                    end
                    for jj=1:9
                      if (jj~=jls) then
                        poss(ils,jj,kl)=0
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
    for kl=1:9
      for il=1:9
        for jl=1:9
          if ( mtlb_sum(poss(:,jl,kl))< 1 | mtlb_sum(poss(il,:,kl))<1 | mtlb_sum(poss(il,jl,:))<1 )  then
            // This means that an infeasibility has been found
            //   In this case, the method backtracks one or two
            //   steps
            poss=pslast
            puzzle=mglast
            solution=mslast
            if ( length(ps2)>0 )  then
              poss=ps2
              puzzle=mg2
              solution=ms2
            end
            // if the process has lasted 50 iterations, then 
            //  the randomization and backtracking two levels
            //  do not appear successful; so, the process starts 
            //  with a new seed.
            if ( count>50 )  then
              poss=ones(9,9,9)
              count=1
              puzzle=zeros(9,9)
              solution=zeros(9,9)
              last=[0 0 0] 
              ps2=[]
            end
          end
        end
      end
    end
    // this part looks for a new value in the puzzle
    // at first, the choices is random and then 
    // this method tries to minimize puzzle entries by
    // picking numbers that have the most alternatives
    //  after 20 values, the choice is again random to avoid 
    //    repeating potential dead ends.
    if ( count==1 | count > 20 )  then
      r = grand(1,1,"unf",0,1)
      kk=ceil(r*9)
    else
      [maxno kk]=mtlb_max(mtlb_sum(mtlb_sum(poss(1:9,1:9,:))))
    end
    if ( sum(poss(:,:,kk)) > 9 )  then
      ipos=[]
      icount=[]
      // this looks for rows that have the greatest number of open
      // locations
      for ii=1:9
        if mtlb_sum(poss(ii,:,kk))>1 
          ipos=[ipos ii]
          icount=[icount mtlb_sum(poss(ii,:,kk))]
        end
      end
      ilen=length(ipos)
      if ( ilen >= 1 )  then
        // randomization is used at the beginning and end
        // with maximum row counts used in the middle iterations
        // to keep the number of entries low
        if ( count<=9 | count > 20 )  then
          r = grand(1,1,"unf",0,1)
          ichoice=ceil(r*length(ipos))
          ii=ipos(ichoice)
        else
          [maxct ichoice]=mtlb_max(icount)
          ii=ipos(ichoice)
        end
        jpos=[]
        jcount=[]
        maxct=0
        for jj=1:9
          if ( poss(ii,jj,kk)>0 & mtlb_sum(poss(ii,jj,:))>1 & mtlb_sum(poss(:,jj,kk))>1 )  then
            jpos=[jpos jj]
            jcount=[jcount mtlb_sum(poss(:,jj,kk))]
          end
        end
        // randomization is also used in the column choice 
        //  at the beginning and end
        if ( count<= 9 | count > 20 )  then
          r = grand(1,1,"unf",0,1)
          jchoice=ceil(r*length(jpos))
          jj=jpos(jchoice)
          maxct=jcount(jchoice)
        else
          [maxct jchoice]=mtlb_max(jcount)
          jj=jpos(jchoice)
        end
        if ( count<=9 | (maxct>1 & mtlb_sum([ii jj kk]~=last)>0) )  then
          top=3*floor((ii-1)/3)+1
          left=3*floor((jj-1)/3)+1
          if ( mtlb_sum(mtlb_sum(poss(top:top+2,left:left+2,kk)))>1 )  then
            // here the method recordes previous values in case
            // the current or last solution produce an infeasibility
            // after completing all checks
            if ( last(1)>0 )  then
              mg2=mglast
              ms2=mslast
              ps2=pslast
            end
            last=[ii jj kk]
            mglast=puzzle
            mslast=solution
            pslast=poss
            puzzle(ii,jj)=kk
            solution(ii,jj)=kk
            for il=1:9
              if ( il~=ii )  then
                poss(il,jj,kk)=0
              end
            end
            for jl=1:9
              if (jl~=jj) then
                poss(ii,jl,kk)=0
              end
            end
            for kl=1:9
              if ( kl~=kk ) then
                poss(ii,jj,kl)=0
              end
            end
            for il=top:top+2
              for jl=left:left+2
                if ( (il~=ii)|(jl~=jj) )  then
                  poss(il,jl,kk)=0
                end
              end
            end
          end
        end
      end
    end
    count=count+1
  end
endfunction

