function [nsrows,nscols] = sudoku_locatenakedsubset ( varargin )
  //   Find naked subsets of given length and returns the associated rows and columns.
  //
  // Calling Sequence
  //   [nsrows,nscols] = sudoku_locatenakedsubset ( X , C , L , k )
  //   [nsrows,nscols] = sudoku_locatenakedsubset ( X , C , L , k , stopatfirst )
  //   [nsrows,nscols] = sudoku_locatenakedsubset ( X , C , L , k , stopatfirst , verbose )
  //
  // Parameters
  // X: a 9-by-9 matrix, with 0 for unknown entries
  // k: the length of the naked subset
  // stopatfirst: if %t, then stop when one or more candidates have been removed. (default = %t)
  // verbose: a boolean. Set to %t to display the state of the matrix. (default = %f)
  // nsrows: a ns x k matrix, where ns is the number of naked subsets. nsrows(i,j) is the row index of the j-th cell of the i-th naked subset 
  // nscols: a ns x k matrix, where ns is the number of naked subsets. nscols(i,j) is the column index of the j-th cell of the i-th naked subset 
  //
  // Description
  //   Search for naked subsets of length k and returns the rows and columns indices.
  //
  // Examples
  // X = [
  // 4 0 0   3 9 0   0 0 2
  // 2 6 0   0 5 8   3 9 0
  // 5 9 3   6 0 0   1 8 0
  // ..
  // 1 0 0   8 6 0   0 0 9
  // 6 0 5   9 0 0   2 0 0 
  // 0 3 9   2 4 5   0 1 6
  // ..
  // 0 5 6   0 0 9   0 2 0
  // 0 1 4   7 0 0   9 0 5
  // 9 0 0   5 3 0   0 0 0
  // ];
  // [C,L] = sudoku_candidates(X);
  // sudoku_locatenakedsubset ( X , C , L , %t ); // Found triple {1,7,8} in row 1 at (1,2), (1,3), (1,6).
  //
  // Authors
  //   Michael Baudin, 2010
  //
  

  [lhs,rhs]=argn();
  if ( rhs < 4 | rhs > 6 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 4 to 6 are expected."), "sudoku_locatenakedsubset", rhs);
    error(errmsg)
  end
  X = varargin(1)
  C = varargin(2)
  L = varargin(3)
  k = varargin(4)
  if ( rhs < 5 ) then
    stopatfirst = %t
  else
    stopatfirst = varargin(5)
  end
  if ( rhs < 6 ) then
    verbose = %f
  else
    verbose = varargin(6)
  end
  
  apifun_typereal ( X , "X" , 1 )
  apifun_typecell ( C , "C" , 2 )
  apifun_typereal ( L , "L" , 3 )
  apifun_typereal ( k , "k" , 4 )
  apifun_typeboolean ( stopatfirst , "stopatfirst" , 5 )
  apifun_typeboolean ( verbose , "verbose" , 6 );
  
  before = sum(L)
  nsrows = []
  nscols = []
  ns = 0

  // Search for subsets in rows
  for i = 1 : 9
    // Search for subsets in row i.
    // Assemble all candidates of the row.
    carray  = sudoku_candidatesmerge ( X , C , L , 1 , i , [] , [1 k] )
    if ( carray == [] ) then
      // There are no possible subset in this row, go on to the next
      continue
    end
    if ( size(carray,"*") <= k ) then
      // There are less than k candidates in this row:
      // there cannot exist a subset here, try the next row.
      continue
    end
    // Get all possible combinations of k elements among 
    // these candidates
    cmap = combine ( carray , k )
    nk = size(cmap,"r")
    // Search for each combinations in the row i
    for m = 1 : nk
      S = cmap(m,:)
      icols = []
      for c = 1 : 9
        if ( X(i,c) == 0 ) then
          if ( areAllContained ( C(i,c).entries' , S ) ) then
            // This might be a subset: register it
            icols($+1) = c
            if ( size(icols,"*") > k ) then
              // There are already more than k cells containing the subset.
              break
            end
          end
        end
      end
      if ( size(icols,"*") <> k ) then
        // This subset is not represented in the row:
        // try next subset.
        continue
      end
      // This is a subset: register it
      if ( verbose ) then
        mprintf("Found naked %d-set {%s} in row %d at columns (%s)\n" , ..
          k , strcat(string(S)," "),i,strcat(string(icols)," "))
      end
      ns = ns + 1
      for ik = 1 : k
        nsrows(ns,ik) = i
        nscols(ns,ik) = icols(ik)
      end
      if ( stopatfirst & sum(L) <> before ) then
        return
      end
    end
  end
  
  // Search for subsets in columns
  for j = 1 : 9
    // Search for subsets in column j.
    // Assemble all candidates of the column.
    carray  = sudoku_candidatesmerge ( X , C , L , 2 , [] , j , [1 k] )
    if ( carray == [] ) then
      // There are no possible subset in this column, go on to the next
      continue
    end
    if ( size(carray,"*") <= k ) then
      // There are less than k candidates in this column:
      // there cannot exist a subset here, try the next column.
      continue
    end
    // Get all possible combinations of k elements among 
    // these candidates.
    cmap = combine ( carray , k )
    nk = size(cmap,"r")
    // Search for each combinations in the column i
    for m = 1 : nk
      S = cmap(m,:)
      irows = []
      for r = 1 : 9
        if ( X(r,j) == 0 ) then
          if ( areAllContained ( C(r,j).entries' , S ) ) then
            // This might be a subset: register it
            irows($+1) = r
            if ( size(irows,"*") > k ) then
              // There are already more than k cells containing the subset.
              break
            end
          end
        end
      end
      if ( size(irows,"*") <> k ) then
        // This subset is not represented in the column:
        // try next subset.
        continue
      end
      // This is a subset: register it
      if ( verbose ) then
        mprintf("Found naked %d-set {%s} in column %d at rows (%s)\n" , ..
          k , strcat(string(S)," "),j,strcat(string(irows)," "))
      end
      ns = ns + 1
      for ik = 1 : k
        nsrows(ns,ik) = irows(ik)
        nscols(ns,ik) = j
      end
      if ( stopatfirst & sum(L) <> before ) then
        return
      end
    end
  end
  //
  // Search for subsets in blocks
  for i = [1 4 7]
    for j = [1 4 7]
      // Search for subsets in block (i,j)
      // Assemble all candidates of the block
      carray  = sudoku_candidatesmerge ( X , C , L , 3 , i , j , [1 k] )
      if ( carray == [] ) then
        // There are no possible subset in this block, go on to the next
        continue
      end
      if ( size(carray,"*") <= k ) then
        // There are less than k candidates in this block:
        // there cannot exist a subset here, try the next block.
        continue
      end
      // Get all possible combinations of 3 elements among 
      // these candidates
      cmap = combine ( carray , k )
      nk = size(cmap,"r")
      // Search for each combinations in the block (i,j)
      for m = 1 : nk
        S = cmap(m,:)
        irows = []
        icols = []
        for r = tri(i)
          for c = tri(j)
            if ( X(r,c) == 0 ) then
              if ( areAllContained ( C(r,c).entries' , S ) ) then
                // This might be a subset: register it
                irows($+1) = r
                icols($+1) = c
                if ( size(irows,"*") > k ) then
                  // There are already more than k cells containing the subset.
                  break
                end
              end
            end
          end
          if ( size(irows,"*") > k ) then
            break
          end
        end
        if ( size(irows,"*") <> k ) then
          // This subset is not represented in the block
          // Try next subset
          continue
        end
        // This is a subset: register it
        if ( verbose ) then
          mprintf("Found naked %d-set {%s} in block (%d,%d) at rows %s and cols %s \n" , ..
            k , strcat(string(S)," "),i,j,strcat(string(irows)," "),strcat(string(icols)," "))
        end
        ns = ns + 1
        for ik = 1 : k
          nsrows(ns,ik) = irows(ns,ik)
          nscols(ns,ik) = irows(ns,ik)
        end
        if ( stopatfirst & sum(L) <> before ) then
          return
        end
      end
    end
  end
  
endfunction

// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type cell
function apifun_typecell ( var , varname , ivar )
  if ( typeof ( var ) <> "ce" ) then
    errmsg = msprintf(gettext("%s: Expected cell variable for variable %s at input #%d, but got %s instead."),"apifun_typecell", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction


// Returns the (i,j) index given the global index s in a block
// Note: in Scilab, entries are numbered column bX column.
//  whatcellinblock ( 1 ) = (1,1)
//  whatcellinblock ( 2 ) = (2,1)
//  whatcellinblock ( 3 ) = (3,1)
//  whatcellinblock ( 4 ) = (1,2)
//  ...
//  whatcellinblock ( 9 ) = (9,1)
function [i,j] = whatcellinblock ( s )
  i = modulo(s-1,3) + 1
  j = (s-i)/3 + 1
endfunction

// combine --
// Returns all combinations of k values from the row vector x as a row-by-row array
// with (n,k) rows where (n,k) is the binomial coefficient and n is the 
// number of values in x. 
//
// References
// http://home.att.net/~srschmitt/script_combinations.html
// Kenneth H. Rosen, Discrete Mathematics and Its Applications, 2nd edition (NY: McGraw-Hill, 1991), pp. 284-286.
//
// Example
//   combine ( [17 32 48 53] , 3 ) :
//       17.    32.    48.  
//       17.    32.    53.  
//       17.    48.    53.  
//       32.    48.    53.  
//
function cmap = combine ( x , k )
  n = size(x,"*")
  if ( n < k ) then
    error ( msprintf ( "%s: The number of values in x is n = %d which is lower than k = %d\n", n,k))
  end
  c = combinations ( n , k )
  cmap = zeros(c,k)
  a = 1:k
  cmap(1,:) = x(a)
  for m = 2 : c
    i = k
    while ( a(i) == n - k + i )
      i = i - 1
    end
    a(i) = a(i) + 1
    for j = i+1 : k
      a(j) = a(i) + j - i
    end
    cmap(m,:) = x(a)
  end
endfunction
//
// whatcell --
// Returns the (i,j) index given the global index s
// Note: in Scilab, entries are numbered column by column.
//
// Example
//  whatcell ( 1 ) = (1,1)
//  whatcell ( 2 ) = (2,1)
//  whatcell ( 3 ) = (3,1)
//  ...
//  whatcell ( 9 ) = (9,1)
//  whatcell ( 10 ) = (1,2)
//  whatcell ( 12 ) = (2,2)
function [i,j] = whatcell ( s )
  i = modulo(s-1,9) + 1
  j = (s-i)/9 + 1
endfunction

// areAllContained --
// Returns %t if all the items in the row vector x are in the 
// row set S.
//
// Example 
//   areAllContained ( [3 5 7] , 1:2:11 ) is true
//   areAllContained ( [2 4 7] , 2:2:10 ) is false
function bool = areAllContained ( x , S )
  bool = %t
  for v = x
    if ( find(S==v) == [] ) then
      bool = %f
      break
    end
  end
endfunction

// combinations --
//   Returns the number of combinations of j objects chosen from n objects.
//   If the input where integers, returns also an integer.
function c = combinations ( n , j )
  c = exp(gammaln(n+1)-gammaln(j+1)-gammaln(n-j+1));
  if ( and(round(n)==n) & and(round(j)==j) ) then
    c = round ( c )
  end
endfunction

