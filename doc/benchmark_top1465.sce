// Copyright (C) 2010 - Michael Baudin

// A simple test against the Learning Curve collection
dirtst = get_absolute_file_path("benchmark_top1465.sce");
nb = sudoku_readsdmnb ( dirtst + "../collection/top1465.sdm" )

// Test solvebylogic

for i = 1 : nb
  X = sudoku_readsdm ( dirtst + "../collection/top1465.sdm" , i );
  e = sum(X==0);
  h = sum(X>0);
  timer();
  [X,iter,C,L] = sudoku_solvebylogic ( X );
  nrounds = sum(iter);
  nstrats = sum(iter>0);
  tt = timer();
  issolved = sudoku_issolved(X);
  r = sum(X==0);
  mprintf("Sudoku #%d, Hints = %d, Time = %f (s), iter = [%s], nrounds = %d, nstrats = %d, remaining = %d\n", i , h , tt , strcat(string(iter)," ") , nrounds , nstrats , r );
end

// Test solvebylogic only on tough sudokus
for i = [71 109 336 590 627]
  X = sudoku_readsdm ( dirtst + "../collection/learningcurve.sdm" , i );
  e = sum(X==0);
  h = sum(X>0);
  timer();
  [X,iter,C,L] = sudoku_solvebylogic ( X );
  nrounds = sum(iter);
  nstrats = sum(iter>0);
  tt = timer();
  issolved = sudoku_issolved(X);
  r = sum(X==0);
  mprintf("Sudoku #%d, Hints = %d, Time = %f (s), iter = [%s], nrounds = %d, nstrats = %d, remaining = %d\n", i , h , tt , strcat(string(iter)," ") , nrounds , nstrats , r );
end


// Test solve

for i = 1 : nb
  X = sudoku_readsdm ( dirtst + "../collection/learningcurve.sdm" , i );
  e = sum(X==0);
  timer();
  [X,iter] = sudoku_solve ( X );
  tt = timer();
  issolved = sudoku_issolved(X);
  r = sum(X==0);
  mprintf("Sudoku #%d, Unknowns = %d, Time = %f (s), iter = [%s], solved = %s, remaining = %d\n", i , e , tt , strcat(string(iter)," ") , string(issolved) , r);
end


