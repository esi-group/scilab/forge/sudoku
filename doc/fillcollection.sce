// Copyright (C) 2010 - Michael Baudin

// Create several puzzles based on the same solution
filename = "mycollection.sdm"
nmax = 100;
for i = 1 : nmax
  mprintf ( "==========================\n")
  mprintf ( "Creation #%d/%d\n",i,nmax)
  clues = round(grand(1,1,"nor",28,1));
  clues = min ( clues , 32 );
  solution = sudoku_readgivens("271649853634815972598273614862957431947381265315426798789532146456198327123764589");
  timer();
  [puzzle,solution] = sudoku_create(%f,clues,100,solution);
  t = timer();
  mprintf ( "Puzzle = ""%s""\n",sudoku_writegivens(puzzle))
  mprintf ( "Solution = ""%s""\n",sudoku_writegivens(solution))
  mprintf ( "Time = %d (s)\n",t)
  [fd,err] = mopen(filename ,"a" );
  fprintf(fd,"%s\n",sudoku_writegivens(puzzle));
  err = mclose(fd);
end

