// Copyright (C) 2010 - Michael Baudin

// A simple test against the Learning Curve collection
dirtst = get_absolute_file_path("benchmark_aiescargot.sce");
nb = sudoku_readsdmnb ( dirtst + "../collection/hardest.sdm" )

// Test solvebylogic

for i = 1 : nb
  X = sudoku_readsdm ( dirtst + "../collection/hardest.sdm" , i );
  e = sum(X==0);
  h = sum(X>0);
  timer();
  [X,iter,C,L] = sudoku_solvebylogic ( X );
  nrounds = sum(iter);
  nstrats = sum(iter>0);
  tt = timer();
  issolved = sudoku_issolved(X);
  r = sum(X==0);
  mprintf("Sudoku #%d, Hints = %d, Time = %f (s), iter = [%s], nrounds = %d, nstrats = %d, remaining = %d\n", i , h , tt , strcat(string(iter)," ") , nrounds , nstrats , r );
end

// Test solve

for i = 1 : nb
  X = sudoku_readsdm ( dirtst + "../collection/hardest.sdm" , i );
  e = sum(X==0);
  h = sum(X>0);
  timer();
  [X,iter,maxlevel] = sudoku_solve ( X );
  nrounds = sum(iter);
  nstrats = sum(iter>0);
  tt = timer();
  issolved = sudoku_issolved(X);
  r = sum(X==0);
  mprintf("Sudoku #%d, Hints = %d, Time = %f (s), iter = [%s], nrounds = %d, nstrats = %d, levels = %d, remaining = %d\n", i , h , tt , strcat(string(iter)," ") , nrounds , nstrats , maxlevel , r );
end

