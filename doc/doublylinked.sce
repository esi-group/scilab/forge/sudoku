// Copyright (C) - Michael Baudin - 2010

// Testing doubly linked lists
MyList = tlist ( [ "DLINKED" , "entries" ] )
MyList.entries = list()
iroot = length ( MyList.entries ) + 1
node = tlist ( [ "NODE" , "prev" , "next" ] )
node.prev = iroot
node.next = iroot
MyList(iroot) = node

