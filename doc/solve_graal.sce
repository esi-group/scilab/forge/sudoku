// Copyright (C) - Michael Baudin - 2010


///////////////////////////////////////////////////////////////////////
// The puzzle suggested by Moler in Chapter 18 of "Experiments with Matlab"
X = [
0 2 0   0 3 0   0 4 0 
6 0 0   0 0 0   0 0 3 
0 0 4   0 0 0   5 0 0 
..
0 0 0   8 0 6   0 0 0 
8 0 0   0 1 0   0 0 6 
0 0 0   7 0 5   0 0 0 
..
0 0 7   0 0 0   6 0 0 
4 0 0   0 0 0   0 0 8 
0 3 0   0 4 0   0 2 0 
];
E = [
9 2 5   6 3 1   8 4 7 
6 1 8   5 7 4   2 9 3 
3 7 4   9 8 2   5 6 1 
..
7 4 9   8 2 6   1 3 5 
8 5 2   4 1 3   9 7 6 
1 6 3   7 9 5   4 8 2 
..
2 8 7   3 5 9   6 1 4 
4 9 1   2 6 7   3 5 8
5 3 6   1 4 8   7 2 9
];

function checksudoku ( X , E )
  for i = 1 : 9
  for j = 1 : 9
    if ( X(i,j) <> 0 & X(i,j) <> E(i,j) ) then
      mprintf(" == Entry (%d,%d) is %d instead of %d ==\n", i,j,X(i,j),E(i,j))
    end
  end
  end
endfunction

checksudoku ( X , E )

[C,L] = sudoku_candidates(X);

// Start the search
sum(X==0), sum(L) // There are 60 unknowns and 264 candidates left.
[X,C,L] = sudoku_findnakedsingles ( X , C , L , %t );
[X,C,L] = sudoku_findhiddensingles ( X , C , L , %t );
[X,C,L] = sudoku_findlocked ( X , C , L , %t );
[X,C,L] = sudoku_findnakedpairs ( X , C , L , %t );
sum(X==0), sum(L) // There are 49 unknowns and 171 candidates left.
checksudoku ( X , E );

///////////////////////////////////////
X = [
0 2 0   6 3 0   0 4 0 
6 0 0   0 0 0   0 0 3 
3 0 4   0 0 0   5 6 0 
..
0 0 0   8 0 6   0 0 0 
8 0 0   0 1 0   0 0 6 
0 6 0   7 0 5   0 0 0 
..
0 8 7   0 5 0   6 0 4 
4 0 0   0 6 7   0 0 8 
0 3 6   0 4 8   0 2 0 
];


sum(X==0), sum(L) // There are 49 unknowns and 171 candidates left.
[X,C,L] = sudoku_findnakedsingles ( X , C , L , %t );
[X,C,L] = sudoku_findlocked ( X , C , L , %t );
[X,C,L] = sudoku_findnakedpairs ( X , C , L , %t );
[X,C,L] = sudoku_findhiddensingles ( X , C , L , %t );
sum(X==0), sum(L) // There are 40 unknowns and 132 candidates left.
checksudoku ( X , E );


///////////////////////////////////////
X = [
0 2 0   6 3 0   0 4 0 
6 0 0   5 7 4   0 0 3 
3 0 4   0 8 0   5 6 0 
..
0 4 0   8 0 6   0 0 0 
8 0 0   4 1 3   0 0 6 
0 6 0   7 0 5   4 8 0 
..
0 8 7   0 5 0   6 0 4 
4 0 0   0 6 7   0 0 8 
0 3 6   0 4 8   0 2 0 
];

sum(X==0), sum(L) // There are 40 unknowns and 132 candidates left.
[X,C,L] = sudoku_findnakedsingles ( X , C , L , %t );
[X,C,L] = sudoku_findhiddensingles ( X , C , L , %t );
[X,C,L] = sudoku_findlocked ( X , C , L , %t );
[X,C,L] = sudoku_findnakedpairs ( X , C , L , %t );
sum(X==0), sum(L) // There are 31 unknowns and 82 candidates left.
checksudoku ( X , E ); 


///////////////////////////////////////

X = [
0 2 0   6 3 0   8 4 0 
6 0 8   5 7 4   2 0 3 
3 0 4   0 8 2   5 6 0 
..
0 4 0   8 0 6   0 0 0 
8 0 2   4 1 3   0 0 6 
0 6 3   7 0 5   4 8 0 
..
2 8 7   3 5 0   6 0 4 
4 0 0   2 6 7   0 0 8 
0 3 6   0 4 8   0 2 0 
];

sum(X==0), sum(L) // There are 31 unknowns and 82 candidates left.
[X,C,L] = sudoku_findnakedsingles ( X , C , L , %t );
[X,C,L] = sudoku_findhiddensingles ( X , C , L , %t );
[X,C,L] = sudoku_findlocked ( X , C , L , %t );
[X,C,L] = sudoku_findnakedpairs ( X , C , L , %t );
sum(X==0), sum(L) // There are 29 unknowns and 72 candidates left.

///////////////////////////////////////

X = [
0 2 0   6 3 0   8 4 0 
6 0 8   5 7 4   2 0 3 
3 0 4   0 8 2   5 6 0 
..
0 4 0   8 0 6   0 0 0 
8 0 2   4 1 3   9 0 6 
0 6 3   7 0 5   4 8 0 
..
2 8 7   3 5 0   6 0 4 
4 0 0   2 6 7   0 0 8 
0 3 6   0 4 8   7 2 0 
];

sum(X==0), sum(L) // There are 29 unknowns and 72 candidates left.
[X,C,L] = sudoku_findnakedsingles ( X , C , L , %t ); // No effect
[X,C,L] = sudoku_findhiddensingles ( X , C , L , %t ); // No effect
[X,C,L] = sudoku_findlocked ( X , C , L , %t ); // No effect
[X,C,L] = sudoku_findnakedpairs ( X , C , L , %t ); // No effect
sum(X==0), sum(L) // There are 29 unknowns and 72 candidates left.

// and in the end...
X = sudoku_solve(X,%t);



