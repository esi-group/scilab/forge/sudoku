// Copyright (C) 2010 - Michael Baudin

// A simple benchmark
X = [
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
..
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
0 0 0   0 0 0   0 0 0 
];
Y = sudoku_solve(X);

t = []
for i = 1:10
  tic;
  [Y,n] = sudoku_solve(X)
  t=[t toc()]
end
mean(t)
min(t)
max(t)

