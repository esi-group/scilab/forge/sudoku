// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


src_dir = get_absolute_file_path("builder_c.sce");


src_path = "c";
linknames = ["sudoku"];
files = [
  "sudoku_engine.c"
  "simpledriver.c"
  ];
ldflags = "";

if MSDOS then
  cflags = "-DWIN32 -DLIBSUDOKU_EXPORTS";
else
  include1 = src_dir;
  cflags = "-I"""+include1+"""";
end

libs = [];

tbx_build_src(linknames, files, src_path, src_dir, libs, ldflags, cflags);

clear tbx_build_src;

