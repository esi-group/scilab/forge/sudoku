// This file is released under the 3-clause BSD license. See COPYING-BSD.
// Generated by builder.sce : Please, do not edit this file
// ----------------------------------------------------------------------------
//
if win64() then
  warning(_("This module requires a Windows x86 platform."));
  return
end
//
sudoku_path = get_absolute_file_path('loader.sce');
//
// ulink previous function with same name
[bOK, ilib] = c_link('sudoku');
if bOK then
  ulink(ilib);
end
//
link(sudoku_path + 'libsudoku' + getdynlibext(), ['sudoku'],'c');
// remove temp. variables on stack
clear sudoku_path;
clear bOK;
clear ilib;
// ----------------------------------------------------------------------------
