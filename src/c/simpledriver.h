// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#ifndef _SUDDRIVER_H_

#define _SUDDRIVER_H_

#ifdef _MSC_VER
	#if LIBSUDOKU_EXPORTS 
		#define SUDOKU_IMPORTEXPORT __declspec (dllexport)
	#else
		#define SUDOKU_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define SUDOKU_IMPORTEXPORT
#endif

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>

#include "sudoku_engine.h"


// simplifiedsolver
//   Solves one single sudoku
//
// Parameters
//
// Inputs:
// puzzle: a string containing the sudoku puzzle, with dots (i.e. ".") for unknowns
//
// Outputs:
// status: the status of the computation. 
// totalsol: the total number of solutions for this puzzle
// score: the score of the first solution, positive
// depth: the maximum depth of the recursive calls for the first puzzle. depth is greater than 1.
// sol: the first solution
//
// Description
// The following describes the status:
//   status=-1 if the format of the puzzle is inconsistent (e.g. the sudoku contains two "5" in one column)
//   status=0 if the puzzle could not been solved
//   status=1 if the puzzle has been solved
//
// The following describes the score:
// Degree of Difficulty	Score
// Trivial 	80 points or less
// Easy 	81 - 125 points
// Medium 	126 - 225 points
// Hard 	226 - 350 points
// Very Hard 	351 - 760 points
// Diabolical 	761 and up
//
SUDOKU_IMPORTEXPORT void simplifiedsolver(char puzzle[82], int * status, int * totalsol, int * score, int * depth, char sol[82]);

__END_DECLS

#endif
