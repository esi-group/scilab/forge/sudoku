/************************************************************************************/
/* A simplified driver                                     */
/************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>

#include "sudoku_engine.h"

#define VERSION "1.20"

/* Command line options */
#ifdef EXPLAIN
#define OPTIONS "?1acdef:Ggmno:p:r:s"
#else
#define OPTIONS "?1acdf:Ggmno:p:r:s"
#endif

extern char *optarg;
extern int optind, opterr, optopt;

/*******************/
/* Mainline logic. */
/*******************/

int main(int argc, char **argv)
{
	int i, rc, bogus, opt, count, solved, unsolved, solncount, explain, first_soln_only;
    int prt_count, prt_num, prt_score, prt_answer, prt_depth, prt_grid, prt_mask, prt_givens, prt;
    char *myname, *infile, *outfile, *rejectfile, outbuf[128], mbuf[28];
    char inbuf[1024] = //".........8..3.5..2..6...9...4.5.6.8.7.1...4.9...9.1...97..6..35..3...1....4.2.7..";
	//"1....7.9..3..2...8..96..5....53..9...1..8...26....4...3......1..4......7..7...3..";
	".....7.9..3..2...8..96..5....53..9...1..8...26....4...3......1..4......7..7...3..";
	Grid *s, *g, *solved_list;
	FILE *h, *solnfile, *rejects;


	/* Init */
	h = stdin;
	solnfile = stdout;
	rejects = stderr;
	rejectfile = infile = outfile = NULL;
	count = solved = unsolved = 0;
	explain = rc = bogus = prt_mask = prt_grid = prt_score = prt_depth = prt_answer = prt_count = prt_num = prt_givens = 0;
	first_soln_only = 0;
                        	prt_answer = 1;		/* print solution */
                        	prt_count = 1;		/* number solutions */
                        	//prt_grid = 1;
                        	prt_depth = 1;
                        	prt_score = 1;
                        	//prt_givens = 1;

    /* Set prt flag if we're printing anything at all */
	prt = prt_mask | prt_grid | prt_score | prt_depth | prt_answer | prt_num | prt_givens;


    if (first_soln_only && prt_score) {
		fprintf(stderr, "Scoring is meaningless when multi-solution mode is disabled.\n");
    }

	if (rejectfile && !(rejects = fopen(rejectfile, "w"))) {
		fprintf(stderr, "Failed to open reject output file: %s\n", rejectfile);
		exit(1);
	}

    if (outfile && !(solnfile = fopen(outfile, "w"))) {
		fprintf(stderr, "Failed to open solution output file: %s\n", outfile);
		exit(1);
	}

	if (infile && strcmp(infile, "-") && !(h = fopen(infile, "r"))) {
       	fprintf(stderr, "Failed to open input game file: %s\n", infile);
		exit(1);
    }

	init_solve_engine(NULL, solnfile, rejects, first_soln_only, explain);

		fprintf(stderr,"SUDOKU=%s\n",inbuf);
		
	solved_list = solve_sudoku(inbuf);
	if (solved_list == NULL) {
		fprintf(rejects, "%d: %s invalid puzzle format\n", count, inbuf); 
		fflush(rejects);
		exit(1);
	}
	
	fprintf(stderr,"Number of solutions=%d\n",solved_list->solncount);

    if (solved_list->solncount) {
	    for (solncount = 0, g = s = solved_list; s; s = s->next) {
			solncount += 1;
			fprintf(solnfile, "Solution #%d/%d\n", solncount,solved_list->solncount);
			if (prt_num) {
				char nbuf[32];
				if (first_soln_only) 
				{
					sprintf(nbuf, "%d: ", count);
				}
                else 
				{
					sprintf(nbuf, "%d:%d ", count, solncount);
					fprintf(solnfile, "%-s", nbuf);
				}
	        }
            if (solncount > 1 || first_soln_only) g->score = 0;
        	if (prt_score) fprintf(solnfile, "Score: %-7d \n", g->score);
            if (prt_depth) fprintf(solnfile, "Depth: %-3d \n", g->maxlvl);
            if (prt_answer || prt_grid) {
				format_answer(s, outbuf);
			}
	        if (prt_answer) fprintf(solnfile, "Solution : %s\n", outbuf);
            if (prt_mask) fprintf(solnfile, " %s\n", cvt_to_mask(mbuf, inbuf));
            if (prt_givens) fprintf(solnfile, "Givens %d\n", g->givens);
        	if (prt_grid) print_grid(outbuf, solnfile);
            if (prt) fprintf(solnfile, "\n");
        }
        if (solncount > 1) {
			rc |= 1;
		}
    }
	else 
	{
		rc |= 1;
		fprintf(rejects, "%d: %*.*s insoluble\n", count, PUZZLE_CELLS, PUZZLE_CELLS, inbuf); fflush(rejects);
		diagnostic_grid(solved_list, rejects);
#if defined(DEBUG)
		mypause();
#endif
    }

	free_soln_list(solved_list);


	return rc;
}

void simplifiedsolver(char puzzle[81], int * status, int * solncount, int * score, int * depth, char sol[81])
{
	int i, rc, bogus, opt, count, solved, unsolved, solncount, explain, first_soln_only;
    int prt_count, prt_num, prt_score, prt_answer, prt_depth, prt_grid, prt_mask, prt_givens, prt;
    char *myname, *infile, *outfile, *rejectfile, outbuf[128], mbuf[28];
    char inbuf[1024] = //".........8..3.5..2..6...9...4.5.6.8.7.1...4.9...9.1...97..6..35..3...1....4.2.7..";
	//"1....7.9..3..2...8..96..5....53..9...1..8...26....4...3......1..4......7..7...3..";
	".....7.9..3..2...8..96..5....53..9...1..8...26....4...3......1..4......7..7...3..";
	Grid *s, *g, *solved_list;
	FILE *h, *solnfile, *rejects;


	/* Init */
	h = stdin;
	solnfile = stdout;
	rejects = stderr;
	rejectfile = infile = outfile = NULL;
	count = solved = unsolved = 0;
	explain = rc = bogus = prt_mask = prt_grid = prt_score = prt_depth = prt_answer = prt_count = prt_num = prt_givens = 0;
	first_soln_only = 0;
                        	prt_answer = 1;		/* print solution */
                        	prt_count = 1;		/* number solutions */
                        	//prt_grid = 1;
                        	prt_depth = 1;
                        	prt_score = 1;
                        	//prt_givens = 1;

    /* Set prt flag if we're printing anything at all */
	prt = prt_mask | prt_grid | prt_score | prt_depth | prt_answer | prt_num | prt_givens;


    if (first_soln_only && prt_score) {
		fprintf(stderr, "Scoring is meaningless when multi-solution mode is disabled.\n");
    }

	if (rejectfile && !(rejects = fopen(rejectfile, "w"))) {
		fprintf(stderr, "Failed to open reject output file: %s\n", rejectfile);
		exit(1);
	}

    if (outfile && !(solnfile = fopen(outfile, "w"))) {
		fprintf(stderr, "Failed to open solution output file: %s\n", outfile);
		exit(1);
	}

	if (infile && strcmp(infile, "-") && !(h = fopen(infile, "r"))) {
       	fprintf(stderr, "Failed to open input game file: %s\n", infile);
		exit(1);
    }

	init_solve_engine(NULL, solnfile, rejects, first_soln_only, explain);

		fprintf(stderr,"SUDOKU=%s\n",inbuf);
		
	solved_list = solve_sudoku(inbuf);
	if (solved_list == NULL) {
		fprintf(rejects, "%d: %s invalid puzzle format\n", count, inbuf); 
		fflush(rejects);
		exit(1);
	}
	
	fprintf(stderr,"Number of solutions=%d\n",solved_list->solncount);

    if (solved_list->solncount) {
	    for (solncount = 0, g = s = solved_list; s; s = s->next) {
			solncount += 1;
			fprintf(solnfile, "Solution #%d/%d\n", solncount,solved_list->solncount);
			if (prt_num) {
				char nbuf[32];
				if (first_soln_only) 
				{
					sprintf(nbuf, "%d: ", count);
				}
                else 
				{
					sprintf(nbuf, "%d:%d ", count, solncount);
					fprintf(solnfile, "%-s", nbuf);
				}
	        }
            if (solncount > 1 || first_soln_only) g->score = 0;
        	if (prt_score) fprintf(solnfile, "Score: %-7d \n", g->score);
            if (prt_depth) fprintf(solnfile, "Depth: %-3d \n", g->maxlvl);
            if (prt_answer || prt_grid) {
				format_answer(s, outbuf);
			}
	        if (prt_answer) fprintf(solnfile, "Solution : %s\n", outbuf);
            if (prt_mask) fprintf(solnfile, " %s\n", cvt_to_mask(mbuf, inbuf));
            if (prt_givens) fprintf(solnfile, "Givens %d\n", g->givens);
        	if (prt_grid) print_grid(outbuf, solnfile);
            if (prt) fprintf(solnfile, "\n");
        }
        if (solncount > 1) {
			rc |= 1;
		}
    }
	else 
	{
		rc |= 1;
		fprintf(rejects, "%d: %*.*s insoluble\n", count, PUZZLE_CELLS, PUZZLE_CELLS, inbuf); 
		fflush(rejects);
		//diagnostic_grid(solved_list, rejects);
#if defined(DEBUG)
		mypause();
#endif
    }

	free_soln_list(solved_list);

	return rc;
}
