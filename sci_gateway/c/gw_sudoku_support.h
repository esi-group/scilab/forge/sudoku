
// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#ifndef __SCI_LINALG_GWSUPPORT_H__
#define __SCI_LINALG_GWSUPPORT_H__

// Headers from Scilab:
#include "machine.h"
#include "doublecomplex.h" 

/* ==================================================================== */

// Utility functions used in the gateway.
int getRealMatrixOfDoubles( char * fname, int ivar , int *piAddr , double** lrA, int * mA , int * nA );
int getComplexMatrixOfDoubles(  char * fname, int ivar , int *piAddr , double** lrA, double** liA, int * mA , int * nA );


// These are functions from BLAS/LAPACK used in the gateway.

	
extern int C2F(dgemm)(char *transa, char *transb, int *m, int *
	n, int *k, double *alpha, double *a, int *lda, 
	double *b, int *ldb, double *beta, double *c__, 
	int *ldc);

extern int C2F(zgemm)(char *transa, char *transb, int *m, int *
	n, int *k, doublecomplex *alpha, doublecomplex *a, int *lda, 
	doublecomplex *b, int *ldb, doublecomplex *beta, doublecomplex *
	c__, int *ldc);

extern int C2F(zhbev)(char *jobz, char *uplo, int *n, int *kd, 
	doublecomplex *ab, int *ldab, double *w, doublecomplex *z__, 
	int *ldz, doublecomplex *work, double *rwork, int *info);

extern int C2F(dcopy)(int *n, double *dx, int *incx, 
	double *dy, int *incy);
	
extern int C2F(dsyev)(char *jobz, char *uplo, int *n, double *a, 
	int *lda, double *w, double *work, int *lwork, 
	int *info);

extern int C2F(dgesv)(int *n, int *nrhs, double *a, int *lda, 
	int *ipiv, double *b, int *ldb, int *info);

extern int C2F(zgesv)(int *n, int *nrhs, doublecomplex *a, 
	int *lda, int *ipiv, doublecomplex *b, int *ldb, int * info);

	
#endif /* __SCI_LINALG_GWSUPPORT_H__ */
