// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


gateway_path = get_absolute_file_path("builder_gateway_c.sce");

libname = "sudokugateway";
namelist = [
  "_sudoku_solvefast"  "sci_sudoku_solvefast"
];
files = [
  "sci_sudoku_solvefast.c"
  "gw_sudoku_support.c"
  ];


ldflags = "";

if ( MSDOS ) then
  include2 = "..\..\src\c";
  include3 = SCI+"/modules/output_stream/includes";
  cflags = "-DWIN32 -I"""+include2+""" -I"""+include3+"""";
else
  include1 = gateway_path;
  include2 = gateway_path+"../../src/c";
  include3 = SCI+"/../../include/scilab/localization";
  include4 = SCI+"/../../include/scilab/output_stream";
  include5 = SCI+"/../../include/scilab/core";
  cflags = "-I"""+include1+""" -I"""+include2+""" -I"""+include3+...
    """ -I"""+include4+""" -I"""+include5+"""";
end

libs = [
  "../../src/c/libsudoku"
];

tbx_build_gateway(libname, namelist, files, gateway_path, libs, ldflags, cflags);

clear tbx_build_gateway;

