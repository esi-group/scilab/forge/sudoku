// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Headers from Scilab:
#include "Scierror.h"
#include "api_scilab.h"
#include "localization.h"
#include "stack-c.h"

// Headers from the gateway:
#include "gw_sudoku.h"
#include "gw_sudoku_support.h"

#include "simpledriver.h"

// [status, totalsol, score, depth, sol] = sudoku_solvefast(puzzle)
//
// Outputs:
// status: the status of the computation. 
// totalsol: the total number of solutions for this puzzle
// score: the score of the first solution, positive
// depth: the maximum depth of the recursive calls for the first puzzle. depth is greater than 1.
// sol: the first solution
//
// Description
// The following describes the status:
//   status=-1 if the format of the puzzle is inconsistent (e.g. the sudoku contains two "5" in one column)
//   status=0 if the puzzle could not been solved
//   status=1 if the puzzle has been solved
//
// The following describes the score:
// Degree of Difficulty	Score
// Trivial 	80 points or less
// Easy 	81 - 125 points
// Medium 	126 - 225 points
// Hard 	226 - 350 points
// Very Hard 	351 - 760 points
// Diabolical 	761 and up
//

/* -------------------------------------------------------------------------- */
int sci_sudoku_solvefast(char* fname)
{
	int m = 0, n = 0;
	int k = 0;

	int minlhs=5, minrhs=1, maxlhs=5, maxrhs=1;

	int *piAddr = NULL;
	SciErr sciErr;
	int iRet = 0;

	CheckRhs(minrhs,maxrhs) ;
	CheckLhs(minlhs,maxlhs) ;

	// Get puzzle (arg #1)
	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 1;
	}
	if(isStringType(pvApiCtx, piAddr))
	{
		if(isScalar(pvApiCtx, piAddr))
		{
			char * puzzle = NULL;
			int status;
			int totalsol;
			int score;
			int depth;
			char sol[82];
			int iRows;
			int iCols;
			double * pdblStatus = NULL;
			double * pdblTotalsol = NULL;
			double * pdblScore = NULL;
			double * pdblDepth = NULL;
			
			// Get puzzle
			iRet = getAllocatedSingleString(pvApiCtx, piAddr, &puzzle);
			if(iRet)
			{
				freeAllocatedSingleString(puzzle);
				return iRet;
			}
			
			// Call the solver
			simplifiedsolver(puzzle, &status, &totalsol, &score, &depth, sol);
			/*
			if ( status == -1 ) 
			{
				sciprint("Invalid puzzle format\n");
			}
			else if ( status == 0 ) 
			{
				sciprint("Puzzle cannot be solved\n");
			}
			else
			{
				sciprint("Puzzle can be solved\n");
				sciprint("Number of solutions=%d\n",totalsol);
				sciprint("Score of first solution=%d\n",score);
				sciprint("Depth of first solution=%d\n",depth);
				sciprint("First solution=%s\n",sol);
			}
			*/
			// Create Rhs #1: status
			iRows = 1;
			iCols = 1;
			iAllocMatrixOfDouble(Rhs+1, iRows, iCols, &pdblStatus);
			pdblStatus[0] = (double) status;
			
			// Create Rhs #2: totalsol
			iRows = 1;
			iCols = 1;
			iAllocMatrixOfDouble(Rhs+2, iRows, iCols, &pdblTotalsol);
			pdblTotalsol[0] = (double) totalsol;

			// Create Rhs #3: score
			iRows = 1;
			iCols = 1;
			iAllocMatrixOfDouble(Rhs+3, iRows, iCols, &pdblScore);
			pdblScore[0] = (double) score;
			
			// Create Rhs #4: depth
			iRows = 1;
			iCols = 1;
			iAllocMatrixOfDouble(Rhs+4, iRows, iCols, &pdblDepth);
			pdblDepth[0] = (double) depth;
			
			// Create Rhs #5: sol
			iRet = createSingleString(pvApiCtx, Rhs + 5, sol);
			if(iRet)
			{
				freeAllocatedSingleString(puzzle);
				return iRet;
			}
			
			freeAllocatedSingleString(puzzle);
			
			LhsVar(1) = Rhs + 1;
			LhsVar(2) = Rhs + 2;
			LhsVar(3) = Rhs + 3;
			LhsVar(4) = Rhs + 4;
			LhsVar(5) = Rhs + 5;
		}
		else
		{
			Scierror(999,_("%s: Wrong size for input argument #%d: %d-by-%d matrix expected.\n"),fname,1,1,1);
			return 1;
		}
	}
	else
	{
		Scierror(999,_("%s: Wrong type for input argument #%d: string expected.\n"),fname,1);
		return 1;
	}
	return 0;
}
/* -------------------------------------------------------------------------- */
